/**
 * -------------------------------
 * AdminATL 模板自定义功能方法
 * -------------------------------
 */

/* 初始执行脚本 */
$(function () {
    //左边栏焦点
    navMenuFocus();
    // 定时删除alert提示框
    deleteAlert();
    // 关闭提示框动画效果
    $('.alert').children('button').click(function () {
        var alert =  $(this).parent();
        alert.addClass('lightSpeedOut');
        var trigger = setTimeout(function(){
            alert.remove();
            clearTimeout(trigger);
        }, 1000);
        return false;
    });
    // 移除表单和列表的动画加载效果
    $('.content').addClass('animated');
    $('.content').addClass('fadeIn');
    var trigger = setTimeout(function(){
        $('.content').removeClass('animated');
        $('.content').removeClass('fadeIn');
        clearTimeout(trigger);
    }, 1000);
});

/* 左边栏根据URL访问路径设置焦点 */
function navMenuFocus() {
    // 获取URL地址参数
    var pageUrl = window.location.pathname.substr(1).split('/');
    // 获取菜单根节点
    var sidebarMenu = $('#sidebar-menu');
    // 获取菜单链接子节点
    var sidebarMenuLink = sidebarMenu.find('a');
    // 便利菜单链接节点
    sidebarMenuLink.each(function () {
        if ($(this).attr('href') != '#') {
            var pathName = this.pathname.substr(1).split('/');
            var status = true;
            // 比对菜单链接地址和页面地址一致性
            for (var i=0; i<pathName.length; i++) {
                if (pageUrl[i] == undefined) {
                    status = false;
                    continue;
                }
                if (pathName[i] != pageUrl[i]) {
                    status = false;
                    continue;
                }
            }
            if (status == true) {
                // 为链接匹配的菜单节点添加active类
                $(this).parent('li').addClass('active');
                $(this).closest('.treeview').addClass('active');
            }
        }
    })
}

/**
 * 定时删除提示框
 */
function deleteAlert() {
    var alert = $('.alert');
    if (alert.length > 0) {
        alert.each(function () {
            var time = $(this).find('.time').text();
            var interval = setInterval(function () {
                time--;
                alert.find('.time').html(time);
                if (time == 1) {
                    alert.addClass('lightSpeedOut');
                }
                if (time < 1) {
                    clearInterval(interval);
                    alert.remove();
                }
                //console.log(time);
            }, 1000);
        });
    }
}