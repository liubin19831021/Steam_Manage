/*
 *
 *   用户中心主方法
 *   version 1.1
 *
 */
(function () {
    "use strict";

    // 定时删除alert提示框
    deleteAlert();

    //日期选择插件
    $('.form-date').datepicker({format:'yyyy-mm-dd'});

    // 监听多条件搜索表单提交
    $("#search-form").submit(function () {
        searchForm(this);
        return false;
    });

    // 显示条件搜索值
    if ($.cookie('search')) {
        var search = JSON.parse($.cookie('search'));
        //console.log(search);
        $(search).each(function () {
            //console.log(this);
            $("#search-form").find("#" + this.name).val(this.value);
        });
    }

    // 清除搜索条件
    $("#clear-search-button").click(function () {
        var path = window.location.pathname;
        $.cookie('search', '', {path: path, expires: -1});
        window.location.reload();
    });

    // 收起展开节点元素
    $('.switch-button').click(function () {
        switchButton(this);
    });


    // 确认订单，并开始备货
    $(".order-confirm-button").click(function () {
        locationPage(this, "确认已经收到购买的商品了吗？（请确认商品已经到手，避免人财两空）？");
        return false;
    });

    // 确认取消订单
    $(".order-cancel-button").click(function () {
        locationPage(this, "确定要取消该订单？");
        return false;
    });

    // 我的订单去付款
    $(".pay-button").click(function () {
        locationPage(this, "确认要对订单进行付款吗？");
        return false;
    });
    
    // 模态窗口
    $(".modal-button").click(function () {
        var url = $(this).attr("href");
        var modalForm = $("#modal-form");
        modalForm.find("form").attr("action", url);
        modalForm.modal();
        return false;
    });

    //选择本地图片并裁切插件脚本
    $('.cropper-image').click(function () {
        cropperImage(this, '#cropper-modal');
        return false;
    });

    // 分类级联选择
    $('.relation-select').each(function () {
        relationSelect(this);
    });

})();