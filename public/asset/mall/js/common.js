/*
 *
 *   商城公共方法文件
 *   version 1.1
 *
 */
// API接口地址
var apiUrl = '/emall/public/test/';
// 静态资源地址
var assetUrl = '/emall/public/asset/mall/';

(function () {
    "use strict";

    // 回到顶部
    toTop('top', false);

    contentFluid('header-nav');
    contentFluid('main-content');

    // 窗口变化监听
    $(window).resize(function () {
        var width = $(window).width();

        contentFluid('header-nav', width);
        contentFluid('main-content', width);
    });

    // 图片预加载
    preloadImage();

    // 响应式图片
    responseImage()
})();

// 获取本地防CSRF攻击的token值
function CSRFToken() {
    var token = null;
    if ($.cookie('XSRF-TOKEN')) {
        token = $.cookie('XSRF-TOKEN');
    }
    return token;
}

// 具有CSRF攻击的ajax提交方法
function ajaxToken(param) {
    var ajaxParam = {};
    if (!param.url) {
        console.log('ajax not url!');
        return false;
    }
    ajaxParam.url = param.url;
    ajaxParam.type = (param.type == undefined) ? 'GET' : param.type;
    ajaxParam.dataType = (param.dataType == undefined) ? 'json' : param.dataType;
    ajaxParam.data = (param.data == undefined) ? {} : param.data;
    if (param.processData != undefined) ajaxParam.processData = false;
    var token = CSRFToken();
    if (token == null && ajaxParam.type != 'GET') {
        console.log('not token!');
        window.location.reload();
        return false;
    } else {
        ajaxParam.headers = {
            'X-XSRF-TOKEN': token,
        };
    }
    if (param.success != undefined) {
        ajaxParam.success = function (result) {
            param.success(result);
        }
    }
    if (param.error != undefined) {
        ajaxParam.error = function (xhr, status, thrown) {
             // xhr 包括XMLHttpRequest对象和其他更多的详细属性和信息
             // 主要有4个属性
             // readyState :当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成
             // status  ：返回的HTTP状态码，比如常见的404,500等错误代码
             // statusText ：对应状态码的错误信息，比如404错误信息是not found,500是Internal Server Error
             // responseText ：服务器响应返回的文本信息
             //
             // status 返回字符串类型，表示返回的状态
             // 根据服务器不同的错误可能返回下面这些信息："timeout"（超时）, "error"（错误）, "abort"(中止), "parsererror"（解析错误），还有可能返回空值
             //
             // thrown 返回字符串类型,表示服务器抛出返回的错误信息
             // 如果产生的是HTTP错误，那么返回的信息就是HTTP状态码对应的错误信息，比如404的Not Found,500错误的Internal Server Error
            param.error(xhr, status, thrown);
        }
    } else {
        ajaxParam.error = function (xhr, status, thrown) {
            console.log(thrown);
        }
    }
    $.ajax(ajaxParam);
    return false;
}

// 回到顶部
function toTop(id,show){
    var oTop = document.getElementById(id);
    if (!oTop) return;
    var bShow = show;
    if(!bShow){
        oTop.style.display = 'none';
        setTimeout(btnShow,50);
    }
    oTop.onclick = scrollToTop;
    function scrollToTop(){
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        var iSpeed = Math.floor(-scrollTop/2);
        if(scrollTop <= 0){
            if(!bShow){
                oTop.style.display = 'none';
            }
            return;
        }
        document.documentElement.scrollTop = document.body.scrollTop = scrollTop + iSpeed;
        setTimeout(arguments.callee,50);
    }
    function btnShow(){
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        if(scrollTop <= 0 ){
            oTop.style.display = 'none';
        }else{
            oTop.style.display = 'block';
        }
        setTimeout(arguments.callee,50);
    }
}

// 中小屏幕尺寸时导航满屏宽度
function contentFluid(name, width) {
    if (width == undefined) width = $(window).width();
    var headNav = $('#' + name);
    if (headNav) {
        var className = (width > 992) ? 'container' : 'container-fluid';
        headNav.attr('class', className);
    }
}

// 响应式图片
function responseImage() {
    $(".response-image").each(function () {
        var img = $(this).children("img");
        var preload = img.attr('preload');
        if (preload) {
            // 处理预加载图片
        }
        img.hide();
        $(this).css("background-image", "url("+assetUrl + img.attr("src") + ")");
    });
}

// 图片预加载
function preloadImage() {
    $(".preload-image").each(function () {
    });
}

// 根据canvas参数生成图片数据,(canvas, base64, blob)
function convertToData(url, canvasdata, cropdata, callback) {
    var cropw = cropdata.width; // 剪切的宽
    var croph = cropdata.height; // 剪切的宽
    var imgw = canvasdata.width; // 图片缩放或则放大后的高
    var imgh = canvasdata.height; // 图片缩放或则放大后的高
    var poleft = canvasdata.left - cropdata.left; // canvas定位图片的左边位置
    var potop = canvasdata.top - cropdata.top; // canvas定位图片的上边位置
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext('2d');
    canvas.width = cropw;
    canvas.height = croph;
    var img = new Image();
    img.src = url;
    img.onload = function() {
        this.width = imgw;
        this.height = imgh;
        // 这里主要是懂得canvas与图片的裁剪之间的关系位置
        ctx.drawImage(this, poleft, potop, this.width, this.height);
        var data = {};
        data.base64 = canvas.toDataURL('image/jpg', 1);  // 这里的“1”是指的是处理图片的清晰度（0-1）之间，当然越小图片越模糊，处理后的图片大小也就越小
        data.canvas = canvas;
        callback && callback(data)      // 回调base64字符串
    }
}

function dataURLtoBlob(dataURl) {
    var arr = dataURl.split(',');
    var mime = arr[0].match(/:(.*?);/)[1];
    var str = atob(arr[1]);
    var n = str.length;
    var u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = str.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}


function locationPage(obj, msg) {
    var url = $(obj).attr("href");
    if (confirm(msg)) {
        window.location.href = url;
    }
}

function switchButton(button) {
    var button = $(button);
    var collapseBox = button.attr('data-collapse');
    var collapse = button.attr('collapse');
    if (collapse == undefined) collapse = "true";
    if (collapse == "true") {
        $(collapseBox).hide();
        button.attr('collapse', "false");
        button.children("i").attr("class", "fa fa-sort-desc");
        button.children(".name").text("展开");
    } else {
        $(collapseBox).show();
        button.attr('collapse', "true");
        button.children("i").attr("class", "fa fa-sort-asc");
        button.children(".name").text("收起");
    }
}

function searchForm(form) {
    var data = $(form).serializeArray();
    data = JSON.stringify(data);
    var path = window.location.pathname;
    $.cookie('search', data, {path: path});
    location.reload();
}

function navTab() {
    var url = window.location.href;
    $('.nav-tabs li').each(function () {
        var tab = $(this).children('a');
        if (tab.attr('href') == url) {
            $(this).attr('class', 'active');
        }
    });
}

/**
 * 定时删除提示框
 */
function deleteAlert() {
    var alert = $('.alert');
    if (alert.length > 0) {
        alert.each(function () {
            var time = $(this).find('.time').text();
            var interval = setInterval(function () {
                time--;
                alert.find('.time').html(time);
                if (time == 1) {
                    alert.addClass('lightSpeedOut');
                }
                if (time < 1) {
                    clearInterval(interval);
                    alert.remove();
                }
                //console.log(time);
            }, 1000);
        });
    }
}

function cropperImage(obj, modal) {
    var thumb = $(obj).attr("thumb");
    var thumb = $(thumb);
    var name = $(obj).attr('data-name');
    var input = $(obj).parent().children("input[name=" + name + "]");
    if (input.length == 0) {
        input = $('<input type="text" name="' + name + '">');
        $(obj).parent().append(input);
        input.hide();
    }
    var fileSrc = $(obj).attr("src");
    var fileRatio = $(obj).attr("ratio");
    var modal = $(modal);
    var image = new Image();
    var fileInput = modal.find('#upload-file-input');
    var previewEdit =  modal.find('#image-container');
    previewEdit.empty().append(image);
    if (fileSrc) {
        image.src = fileSrc;
    }
    modal.modal('show');

    // 图片编辑预览
    var cropBoxData;
    var canvasData;
    var options = {};
    if (fileRatio) {
        options.aspectRatio = fileRatio;
    }
    options.preview = '.img-preview';
    options.built = function () {
        $(image).cropper('setCanvasData', canvasData);
        $(image).cropper('setCropBoxData', cropBoxData);
    };
    modal.on('shown.bs.modal', function () {
        $(image).cropper(options);
    }).on('hidden.bs.modal', function () {
        $(image).cropper('destroy');
    });

    // 选择本地文件
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        fileInput.change(function() {
            var files = this.files;
            if (files && files.length) {
                var file = files[0];
                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $(image).cropper('replace', blobURL);
                } else {
                    console.log('选择的文件不是图片');
                }
            }
        });
    }

    // 保存裁切的图片
    modal.find('.save').click(function () {
        var src = $(image).attr('src');
        cropBoxData = $(image).cropper('getCropBoxData');
        canvasData = $(image).cropper('getCanvasData');
        convertToData(src, canvasData, cropBoxData, function(data) {
            // 回调后的函数处理
            thumb.attr("src", data.base64);
            input.val(data.base64);
            //console.log(data.base64);
            modal.modal('hide');
        });
    });
}

function relationSelect(obj) {
    var input = $(obj);
    var inline = $('<div class="form-inline">');
    var group = $('<div class="form-group">');
    var name = input.attr('name');
    var value = input.attr('data');
    value = value ? value.split(',') : null;
    //console.log(value);
    input.parent().append(inline)
    inline.append(group);
    group.append(input);
    var id = input.attr('id');
    var url = input.attr('url');
    var option = {
        select: '#' + id,
        url: url,
        defVal: value,
        head: '请选择',
        selClass: 'form-control',
        level: 10,
        dataReader: {name: 'name', cell: 'child'}
    };
    var sel = new LinkageSel(option);
    sel.onChange(function () {
        var select = input.parent().find('select').get();
        for (var key in select) {
            $(select[key]).removeAttr('name');
        }
        var count = select.length - 1;
        $(select[count]).attr('name', name);
    });
}






//---------------分割线------------------


//类别导航
$(".slide-tab-box .tab .column").mouseover(function() {
    $(this).addClass('on').siblings().removeClass('on');
    var index = $(this).index();
    $('.slide-tab-box .content').show();
    $('.slide-tab-box .content .box').hide();
    $('.slide-tab-box .content .box:eq(' + index + ')').show();
    //triangle
    $(this).find('.triangle-left').show();
    $(this).siblings().find('.triangle-left').hide();
});
$(".slide-tab-box").mouseleave(function() {
    $('.slide-tab-box .content .box').hide();
    $('.slide-tab-box .content').hide();
    $('.triangle-left').hide();
});

//销量选项卡
$(".index-show-tab .tab .tab-i").mouseover(function() {
    $(this).addClass('on').siblings().removeClass('on');
    var index = $(this).index();
    $(this).parent().parent().find('.show-box').removeClass('show').eq(index).addClass('show');
});
//商品详情页面选项卡
$(".details-tab .tab .tab-i").click(function() {
    $(this).addClass('on').siblings().removeClass('on');
    var index = $(this).index();
    $(this).parent().parent().find('.details-box').removeClass('show').eq(index).addClass('show');
});
//评价选项卡
$(".evaluate .eva-tab .eva-tab-i").click(function() {
    $(this).addClass('on').siblings().removeClass('on');
    var index = $(this).index();
    $(this).parent().parent().find('.eva-box').removeClass('show').eq(index).addClass('show');
});

//公共头部搜索页面
$(".search .btn").click(function() {
    var words = $("input[name='search']").attr("placeholder");
    var search = $("input[name='search']").val();
    if(search == "" && words==""){
        alert("请输入关键字");
    }
    if(search == "" && words!=""){
        $("input[name='search']").val(words);
        $(".search").attr("action","/mall/search");
    }
    else{
        $(".search").attr("action","/mall/search");
    }

});
//progress_bar
function progressBar(){
    $('.probar').css('display','block');
    $('.barline').each(function(){
        var a=$(this).width();
        var b=$(this).children('.line').attr("w");
        $(this).children('.line').animate({
            width: b
        },2000);
       // $(this).children('.line').prev().text(b);
    });
}
//小提示
function tips(msg){
    var ulHtml = '<div class="middleMsg animated bounceIn"><span>'+msg+'</span></div>';
    $('body').prepend(ulHtml);
    $('.middleMsg span').fadeIn(500);
    setTimeout("$('.middleMsg').remove();",1000);
}
//双选按钮弹框 取消和确认
$(".popup-right").click(function(){
    $("#popup").fadeOut(50);
});
$(".popup-left").click(function(){
    var tel = $("#input-tel").val();
    var pwd = $("#input-password").val();
    $("#popup").fadeOut(50);
    if(tel==""){
        //鼠标聚焦
        $(this).parents("#popup").siblings(".login-top").find("#input-tel").focus();return;
    }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(tel))){
        //自动全选
        $(this).parents("#popup").siblings(".login-top").find("#input-tel").select();return;
    }
    if(pwd==""){
        $(this).parents("#popup").siblings(".login-top").find("#input-password").focus();return;
    }else if(pwd<=6||pwd>20){
        $(this).parents("#popup").siblings(".login-top").find("#input-password").select();return;
    }
});
/*单选按钮*/
$(".know").click(function(){
    $("#popup-single").fadeOut(50);
    var tel = $("#input-tel").val();
    var pwd = $("#input-password").val();
    if(tel==""){
        //鼠标聚焦
        $(this).parents("#popup-single").siblings(".login-top").find("#input-tel").focus();return;
    }else if(!(/^1[3|4|5|7|8]\d{9}$/.test(tel))){
        //自动全选
        $(this).parents("#popup-single").siblings(".login-top").find("#input-tel").select();return;
    }
    if(pwd==""){
        $(this).parents("#popup-single").siblings(".login-top").find("#input-password").focus();return;
    }else if(pwd<=6||pwd>20){
        $(this).parents("#popup-single").siblings(".login-top").find("#input-password").select();return;
    }
})