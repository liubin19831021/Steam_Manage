/*
 *
 *   表单验证
 *   version 1.1
 *
 */
/*
(function () {
    "use strict";

    $("[check]").each(function () {
        console.log('asdf');
    });
})()
*/

//显示错误信息
function errorMsg(input, msg) {
    var msg = '<span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span>' + msg;
    var parent = input.parent();
    if (parent.children('.input-group-addon').length > 0) {
        parent = parent.parent();
    }
    var tips = parent.children('.tips');
    if (tips.length > 0) {
        tips.html(msg);
    } else {
        parent.append('<div class="tips">' + msg + '</div>');
    }
}

function clearMsg(input) {
    var parent = input.parent();
    if (parent.children('.input-group-addon').length > 0) {
        parent = parent.parent();
    }
    var tips = parent.children('.tips');
    tips.remove();
}

//手机号
$("#phone").blur(function() {
    var reg = RegExp(/^1[3|4|5|7|8][0-9]\d{4,8}$/i); //验证手机正则(输入前7位至11位)
    var input = $(this);
    if (input.val() == "") {
        errorMsg(input, '手机号不能为空！');
        return false;
    }
    if (input.val().length < 11 || !reg.test(input.val())) {
        errorMsg(input, '手机号不正确！');
        return false;
    }
    clearMsg(input);
    return true;
});

//短信验证码
$("#code").blur(function() {
    var input = $(this);
    if (input.val() == "") {
        errorMsg(input, '验证码不能为空！');
        return false;
    }
    clearMsg(input);
    return true;
});

//密码

$("#password").blur(function() {
    var reg = RegExp(/^(?=.{6,12}$)(?![0-9]+$)(?!.*(.).*\1)[0-9a-zA-Z]+$/);
    var input = $(this);
    if (input.val() == "") {
        errorMsg(input, '密码不能为空');
        return false;
    }
    if(!reg.test(input.val())){
        errorMsg(input, '密码必须为6-12位字母和数字组合！');
        return false;
    }
    clearMsg(input);
    return true;
});

//确认密码
$("#pass-confirm").blur(function() {
    var input = $(this);
    var password = $("#password").val();
    if (input.val() != password) {
        errorMsg(input, '两次密码输入不一致！');
        return false;
    }
    clearMsg(input);
    return true;
});

//图片验证码
$("#captcha").blur(function() {
    var input = $(this);
    if (input.val() == "") {
        errorMsg(input, '图形验证码不能为空！');
        return false;
    }
    clearMsg(input);
    return true;
});












