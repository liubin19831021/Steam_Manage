$(function () {
    "use strict";

    // 全选取消全选
    $(".checkbox-tree .checkbox-input").click(function () {
        var checked = $(this).prop("checked");
        var child = $(this).parent().parent().find(".checkbox-input");
        if (child.length > 0) {
            console.log(checked);
            child.prop("checked", checked);
        }
        if (checked != true) {
            // 检查当前层级是否全部取消选中
            var parent = $(this).closest('ul');
            if (parent.find("li .checkbox-input:checked").length < 1) {
                parent.parent().children('label').children(".checkbox-input").removeAttr("checked");
            }
        }
    });
    // 展开关闭子列表
    $(".collapse-list").click(function () {
        var expanded = $(this).attr('aria-expanded');
        if (expanded == undefined) expanded = 'false';
        console.log(expanded);
        if (expanded == 'true') {
            console.log('plus');
            $(this).children('i').attr('class', 'fa fa fa-plus-square-o');
        } else {
            console.log('minus');
            $(this).children('i').attr('class', 'fa fa fa-minus-square-o');
        }
    });
});