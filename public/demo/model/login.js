/*----------------------------------------*/
/*  Login Model
/*----------------------------------------*/

new Vue({
	el: '#login',
	data: {
		// 语言文件
		lang: {},
	},
	ready: function () {
		var self = this;
		// 获取语言文件
		this.getLang();
		// 监听登录提交操作
		$('#login').submit(function () {
            var phone = $('#phone').val();
            if (!regularCheck('phone', phone)) {
                alert('手机号输入不正确');
                return false;
            }
            var password = $('#password').val();
            if (!regularCheck('password', password)) {
                alert('密码输入不正确');
                return false;
            }
            self.login(phone, password);
			return false;
		});
	},
	methods: {
		getLang: function () {
			var self = this;
			lang(function(result){
				self.lang = result;
			});
		},
		login: function (phone, password) {
            var url = ApiRoute('account_login');
            postAjax(url, {
                phone: phone,
                password: password
            }, function (result) {
                console.log(result);
            });
		}
	}
});

/*

var loginUrl = 'http://yiyuan.9ebo.com/api/account/login';
//按钮变色
$("#input-tel,#input-password").keyup(function(){
    var tel = $("#input-tel").val();
	var pwd = $("#input-password").val();
    if(tel !=''&& (/^1[3|4|5|7|8]\d{9}$/.test(tel)) &&pwd !=''&&(/^\d{6,20}$/.test(pwd))){
        $('#verify').removeClass("aui-login").addClass("add-login-class");
    }else{
        $('#verify').removeClass("add-login-class").addClass("aui-login");
    }
});
  //执行登陆
function logSubmit() {
	var tel = $.trim($("#input-tel").val());
	var pwd = $.trim($("#input-password").val());
	if (tel == null || tel == '' || pwd == null || pwd == '') {
		$("#popup-single").fadeIn(50);
		$(".popup-content").text("账户或密码不能为空！");
		$('.know').text("确认");
		return false;
	} else if (!(/^1[34578][0-9]{9}$/.test(tel))) {
		$("#popup-single").fadeIn(50);
		$(".popup-content").text("手机号格式不正确！");
		$('.know').text("确认");
		return false;
	}else if(!(/^\d{6,20}$/.test(pwd))){
		$("#popup-single").fadeIn(50);
		$(".popup-content").text("密码必须为6到20位数字！");
		$('.know').text("确认");
		return false;
	} else {
		$.ajax({
			type: 'post',
			dataType: "json",
			cache: false,
			data: {
				phone: tel,
				password: pwd
			},
			async: false,
			url: loginUrl,
			success: function (res) {
				if (res.error == '0') {
					alert(JSON.stringify(res));
						//先获取登录信息之后将登录信息存入cookie
					var id=res.data.id;
					var token=res.data.token;
					setCookie("id", id);
					setCookie("token", token);
					setCookie("uname", res.data.username);
					tips('登录成功');
					/!*setTimeout(function () {
						window.location.href = '';
					}, 1000);*!/
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				alert("网络异常");
			}
		});
	}
}

$.ajax({
  type: 'GET',
  url: apiUrl+'login.json',
  async: false,
  cache: false,
  dataType:'json',
  success:function(result) {
    var data={};
    data.messageImg = assetUrl+result.todos.messageImg;
    new Vue({
      el:'#app',
      data : data
    })
  }
});
*/
