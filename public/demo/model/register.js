/*----------------------------------------*/
/*  Register Model
/*----------------------------------------*/
new Vue({
	el: '#register',
	data: {
		lang: {}, // 语言文件
	},
	ready: function () {
		var self = this;
		// 获取语言文件
        lang(function(result){
            self.lang = result;
                // 页面触发操作事件监听
                // 国籍与区号显示与隐藏
            $('.region-btn').click(function(){
                var changeArrow = $(this).attr("changeArrow");
                if(changeArrow=="unChoose"){
                    $(this).addClass("active");
                    $(this).siblings("#region-list").fadeIn(50);
                    $(this).attr("changeArrow","choose");
                }else{
                    $(this).addClass("active");
                    $(this).siblings("#region-list").fadeIn(50);
                    $(this).attr("changeArrow","unChoose");
                }
            });
            $('.region-item').click(function(){
                var changeArrow = $(this).attr("changeArrow");
                var text = $(this).text();
                $(this).parent().siblings('.region-btn').find('span.china').text(text);
                var dataCode = $(this).attr('data-code');
                $(this).parent().siblings('.region-code').val(dataCode);
                $(this).parent().siblings('.region-btn').removeClass("active");
                $(this).parent("#region-list").fadeOut(50);
                if(changeArrow=="unChoose"){
                    $(this).attr("changeArrow","choose");
                }else{
                    $(this).attr("changeArrow","unChoose");
                }
            });
                // 返回首页
            $('#button-home').click(function () {
                window.location.href="../html/index.html";
            });
                // 打开登录页
            $('#login').click(function () {
                window.location.href="../html/login.html";
            });
                // 发送短信验证码
            $('#send-code').click(function () {
                var phone = $('#phone').val();
                if (!regularCheck('phone', phone)) {
                    tips('手机号不能为空或格式不正确');
                    return false;
                }
                self.sendCode(phone);
               });
                // 监听注册提交操作
            $('#register').submit(function () {
                var phone = $('#phone').val();
                if (!regularCheck('phone', phone)) {
                    tips('手机号不能为空或格式不正确！');
                    return false;
                }
                var code = $('#code').val();
                if (!code) {
                    tips('短信验证码不能为空！');
                    return false;
                }
                var password = $('#password').val();
                if (!regularCheck('password', password)) {
                    tips('密码不能为空或格式不正取！');
                    return false;
                }
                var passwordConfirm = $('#password-confirm').val();
                if (passwordConfirm != password) {
                    tips('确认密码与密码输入不一致！');
                    return false;
                }
                self.register(phone, code, password, passwordConfirm);
                return false;
            });
        });
	},
	methods: {
		register: function (phone, code, password, confirm) {
			var url = ApiRoute('account_register');
			postAjax(url, {
				phone: phone,
                code: code,
				password: password,
                password_confirm: confirm
			}, function (result) {
				if (result.error == '0') {
					/*api.setPrefs({
						key: 'user',
						value: JSON.stringify(result.data)
					});*/
                   window.location.href="../html/user.html";
				} else {
					tips(result.msg);
				}
			});
		},
		sendCode: function (phone) {
            var url = ApiRoute('account_register_code');
            postAjax(url, {
                phone: phone,
            }, function (result) {
                if (result.error == '0') {
                    tips('发送成功！');
                } else {
                    tips(result.msg);
                }
            });
		}
	}
});


