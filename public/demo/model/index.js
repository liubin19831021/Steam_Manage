/*----------------------------------------*/
/*  Index Model
/*----------------------------------------*/

new Vue({
    el: '#app',
    data: {
        // 语言文件
        lang: {},
        focus: [], // 焦点广告
        focus_path: '', // 焦点广告路径
        product: [], // 商品列表
        product_path: '' // 商品预览图路径
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取首页焦点广告
        this.getFocus();
        // 获取首页商品列表
        this.getProduct();
    },
    methods: {
        getLang: function () {
            var self = this;
            lang(function(result){
                //console.log(result);
                self.lang = result;
            });
        },
        getFocus: function () {
            var url = ApiRoute('home_ad');
            var self = this;
            getAjax(url, function (result) {
                //console.log(result);
                self.focus = result.data;
                self.focus_path = result.path;
                self.$nextTick(function() {
                    new Swiper('#swiper', {
                        pagination: '.swiper-pagination',
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev',
                        paginationClickable: true,
                        spaceBetween: 30,
                        centeredSlides: true,
                        autoplay: 2500,
                        autoplayDisableOnInteraction: false
                    })
                });
            });
        },
        getProduct: function () {
            var url = ApiRoute('home_product');
            var self = this;
            getAjax(url, function (result) {
                //console.log(result);
                self.product_path = result.path;
                for (var key in result.data) {
                    var val = result.data[key];
                    var data = {};
                    data.id = val['id'];
                    data.title = val['title']; // 商品名称
                    data.thumb = result.path + val['thumb']; // 商品预览图
                    data.end_time = val['end_time']; // 商品开奖时间
                    data.sale_number = val['sale_total'] + '/' + val['total']; // 商品已售数量和总数量比
                    data.sale_percent = percentage(val['sale_total'], val['total']); // 商品已售比例
                    self.product.push(data);
                }
                self.$nextTick(function () {
                    progressBar();
                    // 倒记时
                    timer();
                });
            });
        }
    }
});