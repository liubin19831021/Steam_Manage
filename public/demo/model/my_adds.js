/*----------------------------------------*/
/*  Index Model
 /*----------------------------------------*/

new Vue({
    el: '#app',
    data: {
        // 语言文件
        lang: {},
        address: [], // 地址列表
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取首页商品列表
        this.getAddr();
    },
    methods: {
        getLang: function () {
            var self = this;
            lang(function(result){
                //console.log(result);
                self.lang = result;
            });
        },
        getAddr: function () {
            var url = ApiRoute('adds_index');
            var self = this;
            postAjax(url, {
                member_id: 1
            }, function (result) {
                //console.log(result);
                self.product_path = result.path;
                for (var key in result.data) {
                    var val = result.data[key];
                    var data = {};
                    data.id = val['id'];
                    data.name = val['name']; // 收货人姓名
                    data.phone = val['phone']; // 手机号
                    data.zipcode = val['zipcode']; // 邮政编码
                    data.province = val['province']; // 省份
                    data.city = val['city']; // 城市
                    data.district = val['district']; // 地区
                    data.address = val['address']; // 地址
                    data.is_default = val['is_default']; // 是否为默认地址
                    self.address.push(data);
                }
            });
        }
    }
});