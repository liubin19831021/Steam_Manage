/*----------------------------------------*/
/*  Goods Model
/*----------------------------------------*/

new Vue({
    el: '#app',
    data: {
        // 语言文件
        lang: {},
        product_thumb: [],
        product_thumb_path: '',
        product_info: []
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取商品相册
        this.getThumb();
        // 获取商品详细信息
        this.getInfo();
    },
    methods: {
        buyNumber: 1, // 默认购买数量
        getLang: function () {
            var self = this;
            lang(function(result){
                console.log(result);
                self.lang = result;
            });
        },
        getThumb: function () {
            var url = ApiRoute('product_picture');
            var self = this;
            var id = 14;
            postAjax(url, {
                id: id
            }, function (result) {
                //console.log(result);
                self.product_thumb = result.data;
                self.product_thumb_path = result.path;
                self.$nextTick(function() {
                    new Swiper('#swiper', {
                        pagination: '.swiper-pagination',
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev',
                        paginationClickable: true,
                        spaceBetween: 30,
                        centeredSlides: true,
                        autoplay: 2500,
                        autoplayDisableOnInteraction: false
                    })
                });
            });
        },
        getInfo: function () {
            var url = ApiRoute('product_detail');
            var self = this;
            var id = 14;
            postAjax(url, {
                id: id
            }, function (result) {
                self.product_info = result.data;
                self.product_info.sale_percent = percentage(result.data.sale_total, result.data.total);
                // 详细信息加载完成
                self.$nextTick(function() {
                    // 监听滑动选择购买数量
                    /*var range = new auiRange({
                        element:document.getElementById("number")
                    },function(ret){
                        console.log(ret);
                        self.buyNumber = ret.value;
                    })*/
                    // 购买进度条
                    progressBar();
                    // 立即购买操作
                    this.buy(id);
                    // 查看商品详细信息
                    $('#open-product-conetnt')
                });
            });
        },
        buy: function (id) {
            var self = this;
            $('#button-buy').click(function () {
                var url = ApiRoute('product_buy');
                var member_id = '3';
                var token = '123';
                var number = 1;
                postAjax(url, {
                    product_id: id,
                    order_num: number,
                    member_id: member_id,
                    token: token
                }, function (result) {
                    if (result.error == '0') {
                        alert("购买成功");
                    } else {
                        alert(result.msg);
                    }
                });
            });
        }
    }
});
