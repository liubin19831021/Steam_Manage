new Vue({
    el: '#my-info',
    data: {
        // 语言文件
        lang: {},
        member_info: {},
        member_avatar_path: ''
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取用户信息
        this.getMember();
    },
    methods: {
        getLang: function () {
            var self = this;
            lang(function(result){
                //console.log(result);
                self.lang = result;
            });
        },
        getMember: function () {
            var url = ApiRoute('member_info');
            var self = this;
            postAjax(url, {
                member_id: 1
            }, function (result) {
                console.log(result);
                self.member_info = result.data;
                self.member_info.avatar = result.path + self.member_info.avatar;
                console.log(self.member_info);
                self.$nextTick(function () {
                });
            });
        }
    }
});
