/*----------------------------------------*/
/*  User Model
/*----------------------------------------*/

new Vue({
    el: '#user',
    data: {
        // 语言文件
        lang: {},
        member_info: {},
        member_avatar_path: ''
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取用户信息
        this.getMember();
    },
    methods: {
        getLang: function () {
            var self = this;
            lang(function(result){
                //console.log(result);
                self.lang = result;
            });
        },
        getMember: function () {
            var url = ApiRoute('member_index');
            var self = this;
            postAjax(url, {
                member_id: 1
            }, function (result) {
                console.log(result);
                self.member_info = result.data;
                self.member_info.avatar = result.path + self.member_info.avatar;
                console.log(self.member_info);
                self.$nextTick(function () {
                });
            });
        }
    }
});

/*$.ajax({
  type: 'GET',
  url: apiUrl+'user.json',
  async: false,
  cache: false,
  dataType:'json',
  success:function(result) {
    var data={};
    data.headImg = assetUrl+result.todos.headImg;
    data.headerId = result.todos.headerId;
    data.headerName = result.todos.headerName;
    data.balance = result.todos.balance;
    new Vue({
      el:'#app',
      data : data
    })
  }
});*/
