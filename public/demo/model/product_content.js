/*----------------------------------------*/
/*  Goods Model
/*----------------------------------------*/

new Vue({
    el: '#app',
    data: {
        // 语言文件
        lang: {},
        product_content: []
    },
    ready: function () {
        // 获取语言文件
        this.getConetnt();
    },
    methods: {
        buyNumber: 1, // 默认购买数量
        getLang: function () {
            var self = this;
            lang(function(result){
                console.log(result);
                self.lang = result;
            });
        },
        getConetnt: function () {
            var url = ApiRoute('product_content');
            var self = this;
            var id = 15;
            postAjax(url, {
                id: id
            }, function (result) {
                // console.log(result.content);
                self.product_content = result.data.content;
            });
        },
    }
});
