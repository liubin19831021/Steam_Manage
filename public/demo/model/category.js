new Vue({
    el: '#category',
    data: {
        // 语言文件
        lang: {},
        member_info: {}
    },
    ready: function () {
        // 获取语言文件
        this.getLang();
        // 获取分类信息
        this.getCategory();
    },
    methods: {
        getLang: function () {
            var self = this;
            lang(function(result){
                //console.log(result);
                self.lang = result;
            });
        },
        getCategory: function () {
            var url = ApiRoute('category_url');
            var self = this;
            getAjax(url,function (result) {
                //console.log(result);
                console.log(result.data);
                if (result.error == '0') {
                    //tips('查询成功！');
                    self.member_info = result.data;
                    //console.log(self.member_info);
                } else {
                    tips(result.msg);
                }
            });
        }
    }
});
