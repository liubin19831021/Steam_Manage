/*
 *
 *   公共方法
 *   version 1.1
 *
 */

(function () {
    "use strict";

})();

/**
 * 获取路由URL地址
 */
function ApiRoute(name) {
    var api_url = Config.api_url;
    var route = ROUTES[name];
    return api_url + route;
}

/**
 * 获取语言文件
 */
function lang(func) {
    var file = Config.lang_url + Config.lang_type + '.json';
    $.getJSON(file, function(result) {
        func(result);
    });
}

function getAjax(url, func) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success:function(result) {
            func(result);
        },
        error: function (xhr, status, thrown) {
            console.log(thrown);
        }
    });
}

function postAjax(url, data, func) {
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        success:function(result) {
            func(result);
        },
        error: function (xhr, status, thrown) {
            console.log(thrown);
        }
    });
}


function regularCheck(name, value) {
    var regular = [];
    regular['phone'] = /^1[3|4|5|7|8]\d{9}$/;
    regular['password'] = /^(\w){4,20}$/;
    if (regular[name] != undefined || !value) {
        if (regular[name].test(value)) return true;
    }
    return false;
}

function timer(){
    $('.timer').each(function(){
        var endtime = $(this).attr('data-end-time');
        var ts = (new Date(endtime)) - (new Date());
        var dd = parseInt(ts / 1000 / 60 / 60 / 24, 10);
        var hh = parseInt(ts / 1000 / 60 / 60 % 24, 10);
        var mm = parseInt(ts / 1000 / 60 % 60, 10);
        var ss = parseInt(ts / 1000 % 60, 10);
        if(ts < 0) {
            $(this).find('span').html("夺宝已结束");
        }
        else if(dd > 0) {
            $(this).find('span').html('开奖揭晓倒计时' + dd + "天");
        }
        else if(dd == 0) {
            mm = (mm < 10) ? "0" + mm : mm;
            ss = (ss < 10) ? "0" + ss : ss;
            $(this).find('span').html('开奖揭晓倒计时' + hh + "小时" + mm + "分" + ss + "秒");
        }
        setInterval("timer()",1000);
    });
}


/**
 * 计算百分比
 * @param number1
 * @param number2
 * @returns {string}
 * @constructor
 */
function percentage(number1, number2) {
    return (Math.round(number1 / number2 * 10000) / 100 + "%");
}

/**
 * 进度条
 */
function progressBar(){
    $('.probar').css('display', 'block');
    $('.barline').each(function(){
        var percent = $(this).children('.line').attr("percent");
        var number = $(this).children('.line').attr("number");
        $(this).children('.line').animate({
            width: percent
        },2000);
        $(this).prev().find('span').text(number);
    });
}

//加1
function addSub() {
    var v1 = parseInt($(".goods-num").val());
    v1++;
    $(".goods-num").val(v1);
}

// 减一
function minusSub() {
    var v1 = parseInt($(".goods-num").val());
    if (v1 > 1) {
        v1--;
        $(".goods-num").val(v1);
        if ($(".minus").hasClass("disable")) {
            $(".minus").removeClass("disable");
        }
    } else {
        $(".minus").addClass("disable");
        alert('数量不能少于1');
    }
}


/*function timer(){
    $('.timer').each(function(){
        var endtime = $(this).attr('data-end-time');
        var ts = (new Date(endtime)) - (new Date());
        var dd = parseInt(ts / 1000 / 60 / 60 / 24, 10);
        var hh = parseInt(ts / 1000 / 60 / 60 % 24, 10);
        var mm = parseInt(ts / 1000 / 60 % 60, 10);
        var ss = parseInt(ts / 1000 % 60, 10);
        hh = checkTime(hh);
        mm = checkTime(mm);
        ss = checkTime(ss);
        $(this).find('span').html(dd + "天 " + hh + ":" + mm + ":" + ss);
    });
}
setInterval("timer()",1000);
function checkTime(i){
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}*/

/*function format(timestamp) {
    //timestamp，否则要parseInt转换
    var time = new Date(timestamp);
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + m + '-' + d + ' ' + h + ':' + mm + ':' + s;
}*/
//小提示
function tips(msg){
    var ulHtml = '<div class="middleMsg animated bounceIn"><span>'+msg+'</span></div>';
    $('body').prepend(ulHtml);
    $('.middleMsg span').fadeIn(500);
    setTimeout("$('.middleMsg').remove();",1000);
}
