/*----------------------------------------*/
/*  Config
 /*----------------------------------------*/

var Config = {

    // API接口地址
    api_url: 'http://yiyuan.9ebo.com/api/',

    // 语言文件路径
    lang_url: '/demo/lang/',

    // 语言类型
    lang_type: 'zh',

};

/*----------------------------------------*/
/*  Routes
 /*----------------------------------------*/

var ROUTES = {

    // 首页焦点广告轮播
    home_ad: 'lottery/home/hot',

    // 首页推荐商品列表
    home_product: 'lottery/home/product',

    // 商品分类列表
    //product_list: 'lottery/home/product',

    // 商品详细信息
    product_detail: 'lottery/product/info',

    // 商品图册
    product_picture: "lottery/product/picture",

    // 商品详情
    product_content: "lottery/product/content",

    // 商品购买
    product_buy: "lottery/product/buy",

    // 登录接口
    account_login: "account/login",

    //注册接口
    account_register: "account/register",

    // 发送注册短信验证码
    account_register_code: "account/register/code",

    //找回密码
    account_forgot: "account/forgot/password",

    // 发送找回密码短信验证码
    account_forgot_code: "account/forgot/code",

    // 会员主页信息
    member_index: "member/index",

    // 会员详细信息
    member_info: "member/info",

    // 地址列表页
    adds_index: "member/adds/index",
   
   //我的物流
    member_logistic: "member/logistic",

    //我的晒单
    member_order: "member/sun",

    //我的记录
    member_history: "member/order/index",

    //分类
    category_url: "lottery/category"
};