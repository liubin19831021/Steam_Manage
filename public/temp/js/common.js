$(function () {
    // ajax 表单提交测试
    $('.form').submit(function(){
        var form = $(this);
        var type = form.attr('method');
        var url = $(this).attr('action');
        console.log(form.find('.msg'));
        $.ajax({
            url: url,
            type: type,
            data: $(this).serialize(),
            success: function(result) {
                console.log(result);
                form.find('.msg').html(JSON.stringify(result));
            }
        });
        return false;
    });
});