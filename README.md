# 项目安装与部署

# 服务器环境
php5.6或更高版本
mysql5.6或更高版本
redis2.8或更高版本
ImageMagick6.7或更高版本
php模块 gd mycrpt openssl mbstring imagick fileinfo pdo redis

# 安装依赖插件
composer install
composer update
# 文件自动依赖
composer dump-autoload
# 生成配置文件
cp .env.example .env
# 生成KEY
php artisan key:generate

# 目录权限
chmod -R 777 storage public

# 迁移
# php artisan migrate
# 填充
# php artisan db:seed --class=ManageTableSeeder

# 目录结构
app/Http/Controllers/ 控制器目录
app/Http/Controllers/API 数据接口
app/Http/Controllers/Manage 管理控制台
app/Http/Controllers/Mall 商城
app/Http/Controllers/User 商城-会员中心
app/Http/Controllers/Store 商城-商户中心
app/Http/Controllers/GuestController.php 商城-来宾登录注册

app/Http/Model 数据模型目录

resources/views/ 视图目录
resources/views/manage/ 管理控制台模型
resources/views/manage/mall/ 商城主要页面
resources/views/manage/mall/com/ 商城-公共视图部分
resources/views/manage/mall/guest/ 商城-来宾页面
resources/views/manage/mall/store/ 商城-用户中心
resources/views/manage/mall/user/ 商城-商户中心

# 数据库权限
HOST 42.62.98.59:3306
DATABASE=art_shop
USERNAME=work
PASSWORD=work
PREFIX=art_shop_

# 重新服务器索引命令
cd xunsearch/sdk/php/util/
# 重建商品索引
php Indexer.php --clean goods
php Indexer.php --source=mysql://root:root@localhost/art_shop --sql="SELECT id,goods_name,price,goods_thumb,keywords,goods_description FROM art_shop_goods" --project=goods 

# 自动执行脚本命令 顺序执行
sudo crontab -l   查看
sudo rm -rf /var/run/crond.pid
* * * * * php /mnt/hgfs/G/emall/artisan schedule:run >> /dev/null 2>&1        目录路径改成自己的
sudo crontab -e
sudo cron start

