-- ----------------------------
-- 订单提现状态定时器
-- ----------------------------
CREATE DEFINER=`root`@`%` EVENT `order_status`
ON SCHEDULE EVERY 1 DAY STARTS '2016-07-21 14:30:25'
ON COMPLETION PRESERVE ENABLE DO
UPDATE `art_shop_order` SET `status` = 8 WHERE `status` = 4 AND FROM_UNIXTIME(`update_time`, '%Y-%m-%d') <= DATE_SUB(CURDATE(), INTERVAL 15 DAY)
