<?php

return [

    'welcome' => '您好，欢迎进入steam游戏管理平台!',
    'title' => '微信游戏',
    'keyword' => '微信,游戏',
    'description' => '微信游戏',
    'agreement' => '微信游戏用户服务协议',
    'domain' => 'www.***.com',
    'qq' => '123456789',
];
