<!-- resources/views/manage/index.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>{{ $manage_title }}</title>
    @include('manage.lib.meta')
    @include('manage.lib.link')
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('asset/AdminLTE/plugins/morris/morris.css')}}">

    <link rel="stylesheet" href="{{asset('asset/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">

</head>
<body class="hold-transition skin-{{ $skin }} sidebar-mini">
<div class="wrapper">
    @include('manage.lib.header')
    @include('manage.lib.navigation')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>控制台<small>报表统计</small></h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{$userCount}}</h3>

                            <p>会员总数</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                {{--<div class="col-lg-3 col-xs-6">--}}
                    {{--<!-- small box -->--}}
                    {{--<div class="small-box bg-green">--}}
                        {{--<div class="inner">--}}
                            {{--<h3>{{$storeCount}}</h3>--}}

                            {{--<p>商户总数</p>--}}
                        {{--</div>--}}
                        {{--<div class="icon">--}}
                            {{--<i class="ion ion-stats-bars"></i>--}}
                        {{--</div>--}}
                        {{--<a href="#" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{$goodsCount}}</h3>

                            <p>商品总数</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$orderCount}}</h3>

                            <p>订单总数</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <!-- Custom tabs (Charts with tabs)-->
                    {{--<div class="nav-tabs-custom">--}}
                        {{--<!-- Tabs within a box -->--}}
                        {{--<ul class="nav nav-tabs pull-right">--}}
                            {{--<li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>--}}
                            {{--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>--}}
                            {{--<li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>--}}
                        {{--</ul>--}}
                        {{--<div class="tab-content no-padding">--}}
                            {{--<!-- Morris chart - Sales -->--}}
                            {{--<div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>--}}
                            {{--<div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.nav-tabs-custom -->--}}

                    <!-- solid sales graph -->
                    <div class="box box-solid bg-teal-gradient">
                        <div class="box-header">
                            <i class="fa fa-th"></i>

                            <h3 class="box-title">{{$year}}年订单数据分布图</h3>

                            {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>--}}
                            {{--</button>--}}
                            {{--</div>--}}
                        </div>
                        <div class="box-body border-radius-none">
                            <div class="chart" id="line-chart" style="height: 250px;"></div>
                        </div>
                        <!-- /.box-body -->
                        {{--<div class="box-footer no-border">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">--}}
                                    {{--<input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">--}}

                                    {{--<div class="knob-label">Mail-Orders</div>--}}
                                {{--</div>--}}
                                {{--<!-- ./col -->--}}
                                {{--<div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">--}}
                                    {{--<input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">--}}

                                    {{--<div class="knob-label">Online</div>--}}
                                {{--</div>--}}
                                {{--<!-- ./col -->--}}
                                {{--<div class="col-xs-4 text-center">--}}
                                    {{--<input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">--}}

                                    {{--<div class="knob-label">In-Store</div>--}}
                                {{--</div>--}}
                                {{--<!-- ./col -->--}}
                            {{--</div>--}}
                            {{--<!-- /.row -->--}}
                        {{--</div>--}}
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </section>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('manage.lib.version')
    @include('manage.lib.sidebar')
</div>
@include('manage.lib.script')
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="{{asset('asset/AdminLTE/plugins/morris/morris.min.js')}}"></script>

<script>
    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    });

    var data ='{!! $data !!}';
    data = $.parseJSON(data);
    var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
        data: data,
        xkey: 'date',
        ykeys: ['num'],
        labels: ['订单数:'],
        lineColors: ['#efefef'],
        lineWidth: 2,
        hideHover: 'auto',
        gridTextColor: "#fff",
        gridStrokeWidth: 0.4,
        pointSize: 4,
        pointStrokeColors: ["#efefef"],
        gridLineColor: "#efefef",
        gridTextFamily: "Open Sans",
        gridTextSize: 10
    });


</script>
</body>
</html>