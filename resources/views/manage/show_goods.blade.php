<!DOCTYPE html>
<html>
<head>
    <title>{{ $manage_title }}</title>
    @include('manage.lib.meta')
    @include('manage.lib.link')

    <style type="text/css">
        .checkbox{
            border-style: outset;
        }
    </style>
</head>

<body class="hold-transition skin-{{ $skin }} sidebar-mini">
<div class="wrapper" id="wrapper">
    @include('manage.lib.header')
    @include('manage.lib.navigation')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(isset($crumbs))
                <ol class="breadcrumb">
                    @foreach ($crumbs as $item)
                        @if($item['active']==1)
                            <li class="active"><a href="#">{{$item['value']}}</a></li>
                        @else
                            <li><a href={{$item['url']}}>{{$item['value']}}</a></li>
                        @endif
                    @endforeach
                </ol>
            @endif
        </section>
        <!-- Main content -->
        <section class="content">
            @include('manage.lib.alert')
            <div class="box box-default">
                <div class="box-title">
                    <h3>商品信息</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="box-title">
                                <h3>商品图片</h3>
                            </div>
                            @foreach($picture as $key => $value)
                                <div class="col col-md-4">
                                    <img src="{{ $value }}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label>商品名称 ：</label>
                                <input type="text" name="title" class="form-control" value="{{ $data['title'] }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>商品分类 ：</label>
                                <input type="text" name="category" class="form-control" value="{{ $category }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>市场售价 ：</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-cny"></span></span>
                                    <input type="text" name="price" class="form-control" value="{{ $data['price'] }}" readonly disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>实际价格 ：</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-cny"></span></span>
                                    <input type="text" name="pay_price" class="form-control" value="{{ $data['pay_price'] }}" readonly disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>配送费用 ：</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-cny"></span></span>
                                    <input type="text" name="shipping_price" class="form-control" value="{{ $data['shipping_price'] }}" readonly disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>是否配送 ：</label>
                                <input type="text" name="is_shipping" class="form-control" value="@if($data['is_shipping'] == 1) 是 @else 否 @endif" readonly disabled>
                            </div>
                        </div>
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label>商品的简短描述 ：</label>
                                <input type="text" name="subtitle" class="form-control" value="{{ $data['subtitle'] }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label>商品关键字 ：</label>
                                <input type="text" name="keywords" class="form-control" value="{{ $data['keywords'] }}" readonly disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label>商品缩略图 ：</label><br/>
                                <img src="{{ $goods_thumb }}">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-title">
                    <h3>商品详情</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! $data['content'] !!}
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-body">
                    <div class="row">
                        <div class="col col-md-12">
                            <button class="btn btn-white" type="button"  onclick="window.location.href='{{ route('manage.lottery.product.resource.index') }}'">返回商品列表</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    @include('manage.lib.version')
    @include('manage.lib.sidebar')
</div>
@include('manage.lib.script')
@if (!empty($form['ext']))
    @foreach($form['ext'] as $val)
        @include($val)
    @endforeach
@endif
</body>
</html>