<!-- resources/views/manage/table.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>{{ $manage_title }}</title>
    @include('manage.lib.meta')
    @include('manage.lib.link')
</head>

<body class="hold-transition skin-{{ $skin }} sidebar-mini">
<div class="wrapper">
    @include('manage.lib.header')
    @include('manage.lib.navigation')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div><strong>{{ $title }}</strong><small></small></div>
            @if(!empty($crumbs))
                <ol class="breadcrumb">
                    @foreach ($crumbs as $val)
                        <li {{ $val['active'] == 1 ? 'class="active"' : '' }}><a href={{ $val['url'] }}>{{ $val['value'] }}</a></li>
                    @endforeach
                </ol>
            @endif
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('manage.lib.alert')
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @if(!empty($nav) || !empty($option_title))
                            <div class="box-header">
                                @if(isset($nav['name']))
                                    @include($nav['name'])
                                @endif
                                @if(!empty($option_title))
                                    <div class="option-title">
                                        @foreach($option_title as $key => $val)
                                            <a class="option-title-tab option-button btn btn-primary" href="{{ isset($val['route']) ? $val['route'] : '' }}" modal="{{ isset($option_modal[$key]) ? 'true' : '' }}" modal-key="{{ $key }}" modal-load="{{ isset($val['load']) ? $val['load'] : '' }}">
                                                <i class="fa fa-{{ $key }}"></i>&nbsp;{{ $val['title'] }}
                                            </a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        @endif
                        <div class="box-body">
                            <form class="form-inline pull-left">
                                <select id="limit" name="nubmer" class="form-control">
                                    <option value="10">10条</option>
                                    <option value="20">20条</option>
                                    <option value="50">50条</option>
                                    <option value="100">100条</option>
                                </select>
                            </form>
                            <form class="form-inline pull-right" id="search-form">
                                <select id="field" name="field" class="form-control">
                                    <option value="0">请选择</option>
                                    @foreach ($field as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                                <select id="condition" name="condition" class="form-control">
                                    <option value="0">请选择</option>
                                    <option value="1">等于</option>
                                    <option value="2">小于</option>
                                    <option value="3">大于</option>
                                    <option value="4">小于等于</option>
                                    <option value="5">大于等于</option>
                                    <option value="6">不等于</option>
                                </select>
                                <input type="text" id="keyword" name="keyword" class="form-control">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <div class="box-body">
                            <table name="data-table" class="data-table table table-bordered table-striped">
                                <thead>
                                <tr>
                                    @if (!empty($option_title))
                                        <th>
                                            {{ Form::checkbox('select-all') }}
                                        </th>
                                    @endif
                                    @foreach ($field as $key => $val)
                                        <th class="sorting" sort="asc" data-field="{{$key}}">{{ $val }}</th>
                                    @endforeach
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($list as $row)
                                    <tr>
                                        @if (!empty($option_title))
                                            <td>
                                                {{ Form::checkbox('select-id', $row->id, ['class'=>'form-control']) }}
                                            </td>
                                        @endif
                                        @foreach ($field as $key => $val)
                                            @if(isset($range[$key][$row->$key]))
                                                <td>{{ $range[$key][$row->$key] }}</td>
                                            @else
                                                <td>{{ $row->$key }}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            @inject('metrics', 'App\Ext\MetricsService')
                                            @foreach ($metrics->optionRow($option_row, $row) as $key => $val)
                                                <a class="option-title-tab option-button btn btn-default btn-sm" href="{{ isset($val['route']) ? $val['route'] : '' }}" modal="{{ isset($option_modal[$key]) ? 'true' : '' }}" modal-key="{{ $key }}" modal-load="{{ isset($val['load']) ? $val['load'] : '' }}">
                                                    <i class="fa fa-{{ $key }}"></i>&nbsp;{{ $val['title'] }}
                                                </a>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="page-info text-center">总计&nbsp;{!! $list->total() !!}&nbsp;条记录,每页显示&nbsp;{!! $list->perPage() !!}&nbsp;条,分为&nbsp;{!! $list->lastPage() !!}&nbsp;页,当前显示第&nbsp;{!! $list->currentPage() !!}&nbsp;页</div>
                        </div>
                        <div class="box-footer">
                            {{--<div class="pull-right">{!! $list->render() !!}</div>--}}
                            <div class="pull-right">{!! $list->render !!}</div>
                            @if (isset($form_button))
                                @foreach ($form_button as $key => $value)
                                    <button class="btn btn-white" type="{{ $value['type'] }}" {{ isset($value['route']) ? 'onclick=window.location.href="' .$value['route'] . '"' : '' }}>{{ $value['name'] }}</button>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    @include('manage.lib.version')
    @include('manage.lib.sidebar')
</div>
<!-- 模态框（Modal） -->
@foreach ($option_modal as $key => $val)
    <div class="modal fade" id="modal-{{ $key }}" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="modal-form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="{{ $val['method'] }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">{{ $val['title'] }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{ isset($val['msg']) ? $val['msg'] : ''}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endforeach
<div class="modal fade" id="modal-load-form" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<!-- 模态框 -->
@include('manage.lib.script')
<script type="text/javascript">
    var path = window.location.pathname;
    // 模态对话框操作
    $('.option-button').click(function () {
        var key = $(this).attr('modal-key');
        if (key) {
            if ($(this).attr('modal') == 'true') {
                var url = $(this).attr('href');
                var modal = $('#modal-' + key);
                var form = modal.find('.modal-form');
                form.attr('action', url);
                modal.modal();
                return false;
            } else {
                var load = $(this).attr('modal-load');
                if (load) {
                    // 加载异步表单页
                    var modal = $('#modal-load-form');
                    $.ajax({
                        url: load,
                        type: 'GET',
                        dataType: 'html',
                        success: function (result) {
                            modal.find('.modal-content').html(result)
                        }
                    });
                    modal.modal();
                    return false;
                }
            }
        }
    });
    // 提交条件搜索并刷新页面
    $('#search-form').submit(function () {
        var field = $(this.field).val();
        var condition = $(this.condition).val();
        var keyword = $(this.keyword).val();
        if (field && keyword) {
            if (!condition) condition = 1;
            var data =  {
                'field': field,
                'condition': condition,
                'keyword': keyword
            }
            data = JSON.stringify(data);
            $.cookie('search', data, {path: path});
            location.reload();
            return false;
        }
        $.cookie('search', '', {path: path, expires: -1});
        location.reload();
        return false;
    });
    // 显示条件搜索值
    if ($.cookie('search')) {
        var search = JSON.parse($.cookie('search'));
        console.log(search);
        var searchForm = $('#search-form');
        searchForm.find('#field').val(search.field);
        searchForm.find('#condition').val(search.condition);
        searchForm.find('#keyword').val(search.keyword);
    }
    // 设置每页显示条数
    $('#limit').change(function () {
        var number = $(this).val();
        $.cookie('limit', number, {path: path});
        location.reload();
    });
    // 显示每页显示条数值
    if ($.cookie('limit')) {
        console.log($.cookie('limit'));
        $('#limit').val($.cookie('limit'));
    }
    // 单列排序
    $('.data-table thead th[sort]').click(function () {
        var sortClass = $(this).attr('class');
        var sortButton = $('.data-table thead th[sort]');
        sortButton.attr('class', 'sorting');
        sortButton.attr('sort', null);
        var sort = null;
        switch (sortClass) {
            case 'sorting':
                sort = 'desc';
                break;
            case 'sorting_desc':
                sort = 'asc';
                break;
            case 'sorting_asc':
                sort = null;
                break;
        }
        if (sort != null) {
            $(this).attr('class', 'sorting_' + sort);
            $(this).attr('sort', sort);
            var data =  {
                'field': $(this).attr('data-field'),
                'value': sort,
            }
            $.cookie('sort', JSON.stringify(data), {path: path});
        } else {
            $.cookie('sort', '', {path: path, expires: -1});
        }
        location.reload();
    });
    // 显示单列排序
    if ($.cookie('sort')) {
        var sort = JSON.parse($.cookie('sort'));
        console.log(sort);
        var obj = $('.data-table thead th[data-field=' + sort.field + ']');
        obj.attr('class', 'sorting_' + sort.value);
        obj.attr('sort', sort.value);
    }
</script>
</body>
</html>
