<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('asset/AdminLTE/img/avatar.svg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $username }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" id="sidebar-menu">
            <li class="header">
                <i class="fa fa-dashboard"></i>
                <span>控制台</span>
            </li>
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-sitemap"></i>--}}
                    {{--<span>系统管理</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.user.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>系统管理员</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="{!! route('manage.role.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>管理员角色</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.role.action.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>管理员权限</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.region.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>地区管理</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>用户</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{!! route('manage.member.resource.index') !!}">
                            <i class="fa fa-circle-o"></i>
                            <span>会员管理</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span>订单</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.mall.order.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>全部订单</span>--}}
                            {{--<span class="label pull-right bg-red">4</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--@if($is_wechat == 'YES')--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-folder"></i>--}}
                        {{--<span>公众号管理</span>--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li>--}}
                            {{--<a href="{!! route('manage.wechat.mp.resource.index') !!}">--}}
                                {{--<i class="fa fa-circle-o"></i>--}}
                                {{--<span>公众号列表</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="{!! route('manage.wechat.mp.redpack.resource.index') !!}">--}}
                                {{--<i class="fa fa-circle-o"></i>--}}
                                {{--<span>红包管理</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-folder"></i>--}}
                        {{--<span>游戏管理</span>--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li>--}}
                            {{--<a href="{!! route('manage.game.resource.index') !!}">--}}
                                {{--<i class="fa fa-circle-o"></i>--}}
                                {{--<span>全部游戏</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--@endif--}}
            @if($is_steam == 'YES')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>steam管理</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{!! route('manage.steam.game.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>steam游戏</span>
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('manage.steam.game.category.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>游戏分类</span>
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('manage.steam.game.tag.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>游戏标签</span>
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('manage.steam.game.news.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>游戏新闻</span>
                            </a>
                        </li>
                    </ul>
                </li>
                {{--<li class="treeview">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-folder"></i>--}}
                        {{--<span>游戏管理</span>--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li>--}}
                            {{--<a href="{!! route('manage.game.resource.index') !!}">--}}
                                {{--<i class="fa fa-circle-o"></i>--}}
                                {{--<span>全部游戏</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            @endif
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-folder"></i>--}}
                    {{--<span>商品管理</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.mall.product.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>全部商品</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{!! route('manage.mall.category.resource.index') !!}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>商品分类</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            @if($is_movie == 'YES')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>影视管理</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{!! route('manage.movie.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>全部影视</span>
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('manage.movie.category.resource.index') !!}">
                                <i class="fa fa-circle-o"></i>
                                <span>影视分类</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i>
                    <span>广告管理</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{!! route('manage.ad.resource.index') !!}">
                            <i class="fa fa-circle-o"></i>
                            <span>广告管理</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
</aside>