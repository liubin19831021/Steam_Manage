<footer class="main-footer">
    <div class="pull-right hidden-xs"><b>Version</b>&nbsp;1.1</div>
    <strong>Copyright &copy; 2015-<?= date('Y', time()) ?> <a href="{{ $domain }}">{{ $manage_title }}</a>.</strong> All rights reserved.
</footer>
