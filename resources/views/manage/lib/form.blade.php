<form class="modal-form" method="POST" action="{{ $form_url }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="{{ $form_method }}">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">{{ $form_title }}</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            @foreach ($form['element'] as $key => $value)
                <div class="col col-md-{{ $value['col'] }}">
                    <div class="form-group">
                        {{ Form::label($value['name']) }}
                        @if($value['type'] == 'checkbox-group')
                            @foreach ($value['element'] as $group)
                                <div class="form-group">
                                    @foreach ($group as $k => $val)
                                        @if($k == '0')
                                            <div><label>{{ $val['input'] }} {{ $val['name'] }}</label></div>
                                        @else
                                            <label>{{ $val['input'] }} {{ $val['name'] }}</label>
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        @elseif ($value['type'] == 'checkbox' && is_array($value['element']))
                            <div class="form-group">
                                @foreach ($value['element'] as $k => $val)
                                    <label>{{ $val['input'] }} {{ $val['name'] }}</label>
                                @endforeach
                            </div>
                        @else
                            {{ $value['element'] }}
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="submit" class="btn btn-primary">确定</button>
    </div>
</form>