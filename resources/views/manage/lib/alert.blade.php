@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible animated lightSpeedIn" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ $error }}&nbsp;&nbsp;<span>(<strong class="time">10</strong>秒后自动关闭)</span>
        </div>
    @endforeach
@endif
@if (session('success'))
    <div class="alert alert-success alert-dismissible animated lightSpeedIn" role="alert">
        {{ session('success') }}&nbsp;&nbsp;<span>(<strong class="time">5</strong>秒后自动关闭)</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
@endif
@if (session('msg'))
    <div class="alert alert-info alert-dismissible animated lightSpeedIn" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('msg') }}&nbsp;&nbsp;<span>(<strong class="time">8</strong>秒后自动关闭)</span>
    </div>
@endif
@if (session('warning'))
    <div class="alert alert-warning alert-dismissible animated lightSpeedIn" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('warning') }}&nbsp;&nbsp;<span>(<strong class="time">8</strong>秒后自动关闭)</span>
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger alert-dismissible animated lightSpeedIn" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('error') }}&nbsp;&nbsp;<span>(<strong class="time">10</strong>秒后自动关闭)</span>
    </div>
@endif
