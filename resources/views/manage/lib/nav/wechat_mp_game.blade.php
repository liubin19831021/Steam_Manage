<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.wechat.mp.game.resource.edit', ['id' => $nav['id']]) }}">公众号活动修改</a></li>
    <li class="{{ $nav['active'] == 'game' ? 'active' : '' }}"><a href="{{ route('manage.wechat.mp.game.award.resource.index', ['wechat_mp_game_id' => $nav['id']]) }}">公众号活动游戏列表</a></li>
</ul>