<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.wechat.mp.resource.edit', ['id' => $nav['id']]) }}">微信公众账号修改</a></li>
{{--    <li class="{{ $nav['active'] == 'activity' ? 'active' : '' }}"><a href="{{ route('manage.wechat.activity.resource.index', ['wechat_mp_id' => $nav['id']]) }}">公众账号活动</a></li>--}}
    <li class="{{ $nav['active'] == 'activity' ? 'active' : '' }}"><a href="{{ route('manage.wechat.mp.game.resource.index', ['wechat_mp_id' => $nav['id']]) }}">公众账号游戏</a></li>
</ul>