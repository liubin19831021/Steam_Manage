<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.member.resource.edit', ['id' => $nav['id']]) }}">会员基础信息</a></li>
    <li class="{{ $nav['active'] == 'address' ? 'active' : '' }}"><a href="{{ route('manage.member.adds.resource.index', ['member' =>  $nav['id']]) }}">收货地址管理</a></li>
</ul>