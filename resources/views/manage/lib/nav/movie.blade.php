<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.movie.resource.edit', ['id' => $nav['id']]) }}">影视基本信息</a></li>
    <li class="{{ $nav['active'] == 'resources' ? 'active' : '' }}"><a href="{{ route('manage.movie.resources.resource.index', ['movie_id' => $nav['id']]) }}">影视资源列表</a></li>
</ul>