<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.lottery.product.resource.edit', ['id' => $nav['id']]) }}">商品基本信息</a></li>
    <li class="{{ $nav['active'] == 'describe' ? 'active' : '' }}"><a href="{{ route('manage.lottery.product.describe', ['id' => $nav['id']]) }}">商品详细介绍</a></li>
    <li class="{{ $nav['active'] == 'picture' ? 'active' : '' }}"><a href="{{ route('manage.lottery.product.picture', ['id' => $nav['id']]) }}">商品相册</a></li>
    <li class="{{ $nav['active'] == 'history' ? 'active' : '' }}"><a href="{{ route('manage.lottery.product.history.resource.index', ['product_id' => $nav['id']]) }}">商品开奖记录</a></li>
</ul>