<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.resource.edit', ['id' => $nav['id']]) }}">steam游戏修改</a></li>
    <li class="{{ $nav['active'] == 'describe' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.describe', ['id' => $nav['id']]) }}">详细介绍</a></li>
    <li class="{{ $nav['active'] == 'picture' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.picture', ['id' => $nav['id']]) }}">相册</a></li>
    <li class="{{ $nav['active'] == 'goods' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.goods.resource.index', ['steam_game_id' => $nav['id']]) }}">steam游戏商品</a></li>
    <li class="{{ $nav['active'] == 'collocation' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.collocation.resource.index', ['steam_game_id' => $nav['id']]) }}">steam游戏配置</a></li>
</ul>