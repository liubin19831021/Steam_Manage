<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.news.resource.index') }}">新闻列表</a></li>
    <li class="{{ $nav['active'] == 'channel' ? 'active' : '' }}"><a href="{{ route('manage.steam.game.channel.resource.index') }}">频道管理</a></li>
</ul>