<ul class="nav nav-tabs">
    <li class="{{ $nav['active'] == 'main' ? 'active' : '' }}"><a href="{{ route('manage.wechat.activity.resource.edit', ['id' => $nav['id']]) }}">公众号活动修改</a></li>
    <li class="{{ $nav['active'] == 'map' ? 'active' : '' }}"><a href="{{ route('manage.wechat.activity.game.map', ['id' => $nav['id']]) }}">公众号活动与游戏关联</a></li>
    <li class="{{ $nav['active'] == 'game' ? 'active' : '' }}"><a href="{{ route('manage.wechat.activity.game.resource.index', ['activity_id' => $nav['id']]) }}">公众号活动游戏列表</a></li>
</ul>