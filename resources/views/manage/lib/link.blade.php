<!-- Bootstrap 3.3.6 -->
<link href="{{asset('asset/bootstrap-3.3.5-dist/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{asset('asset/font-awesome-4.6/css/font-awesome.min.css')}}" rel="stylesheet">
<!-- pace -->
<link rel="stylesheet" href="{{asset('asset/plugins/pace/themes/white/pace-theme-minimal.css')}}" rel="stylesheet">
<!-- Theme style -->
<link href="{{asset('asset/AdminLTE/css/AdminLTE.min.css')}}" rel="stylesheet">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('asset/AdminLTE/plugins/iCheck/flat/green.css')}}" rel="stylesheet">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link href="{{asset('asset/AdminLTE/css/skins/skin-' . $skin . '.css')}}" rel="stylesheet">
<link href="{{asset('asset/AdminLTE/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('asset/AdminLTE/css/style.css')}}" rel="stylesheet">