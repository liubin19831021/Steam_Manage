<!-- daterange picker -->
<script src="{{asset('asset/plugins/linkagesel/linkagesel.min.js')}}"></script>
<script>
    $(function () {
        $('.relation-select').each(function () {
            var input = $(this);
            var inline = $('<div class="form-inline">');
            var group = $('<div class="form-group">');
            var name = input.attr('name');
            var value = input.attr('data');
            value = value ? value.split(',') : null;
            //console.log(value);
            input.parent().append(inline)
            inline.append(group);
            group.append(input);
            var id = input.attr('id');
            var url = input.attr('url');
            var option = {
                select: '#' + id,
                url: url,
                defVal: value,
                head: '请选择',
                selClass: 'form-control',
                level: 10,
                dataReader: {name: 'name', cell: 'child'}
            };
            var sel = new LinkageSel(option);
            sel.onChange(function () {
                var select = input.parent().find('select').get();
                for (var key in select) {
                    $(select[key]).removeAttr('name');
                }
                var count = select.length - 1;
                $(select[count]).attr('name', name);
            });
        });
    });
</script>