<script src="{{asset('asset/plugins/distpicker/distpicker.data.js')}}"></script>
<script src="{{asset('asset/plugins/distpicker/distpicker.js')}}"></script>
<script>
    $(function () {
        'use strict';

        $('.distpicker').each(function () {
            var el = $(this);
            var name = el.attr('name');
            var value = el.val();
            value = value.split(',');
            var option = {};
            option.province = (value[0] != undefined) ? value[0] : null;
            option.city = (value[1] != undefined) ? value[1] : null;
            option.district = (value[2] != undefined) ? value[2] : null;
            var container = $('<div class="form-inline">');
            el.parent().append(container);
            var province = '<div class="form-group"><select name="' + name + '[province]" class="form-control"></select></div>';
            container.append(province);
            var city = '<div class="form-group"><select name="' + name + '[city]" class="form-control"></select></div>';
            container.append(city);
            var district = '<div class="form-group"><select name="' + name + '[district]" class="form-control"></select></div>';
            container.append(district);
            $(this).remove();
            container.distpicker(option);
        });
    });
</script>