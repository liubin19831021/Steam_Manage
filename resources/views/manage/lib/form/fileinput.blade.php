<!-- include bootstrap-fileinput css/js -->
<link href="{{asset('asset/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet"/>
<script src="{{asset('asset/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('asset/plugins/bootstrap-fileinput/js/fileinput_locale_zh.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // 文件上传
        $('.fileinput').each(function () {
            var param = {};
            param.autoReplace = true;
            param.paramuploadAsync = true;
            param.overwriteInitial = false;
            // 限制文件格式
            var accept = $(this).attr('accept').split(',');
            param.allowedFileExtensions = accept;
            // 限制上传数量
            var maxCount = $(this).attr('max-count');
            if (maxCount) {
                param.maxFileCount = maxCount;
            }
            // 文件上传接口
            param.showUpload = false;
            param.initialPreviewShowDelete = false;
            var upload = $(this).attr('upload');
            if (upload) {
                param.uploadUrl = upload;
                param.showUpload = true;
                param.initialPreviewShowDelete = true;
            }
            // 原始文件显示
            if ($(this).attr('data')) {
                var data = $(this).attr('data');
                data = JSON.parse(data);
                param.initialPreview = [];
                param.initialPreviewConfig = [];
                for (var key in data) {
                    param.initialPreviewConfig.push(
                            {
                                'caption': data[key].original,
                                'url': data[key].delete,
                                'key': data[key].id,
                                'extra': {'key': data[key].id}
                            }
                    );
                    param.initialPreview.push('<img key="' + data[key].id + '" src="' + data[key].file + '" class="file-preview-image" alt="' + data[key].original + '" title="' + data[key].original + '"/>');
                }
            }
            $(this).fileinput(param);
        });
    });
</script>