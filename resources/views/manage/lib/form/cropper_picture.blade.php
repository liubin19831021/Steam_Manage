<!-- cropper -->
<link rel="stylesheet" href="{{asset('asset/plugins/jquery-cropper/cropper.min.css')}}">
<script src="{{asset('asset/plugins/jquery-cropper/cropper.min.js')}}"></script>
<!-- 模态框（Modal） -->
<div class="modal fade" id="cropper-modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content cropper-image-modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalLabel">Crop the image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div id="image-container"></div>
                    </div>
                    <div class="col-md-5">
                        <h4 class="title">预览效果</h4>
                        <div class="img-preview preview-lg"></div>
                        <div class="btn-group">
                            <label class="btn btn-primary upload-file" for="upload-file-input">
                                <input type="file" id="upload-file-input" name="upload-file" class="hide" accept="image/*">
                                <span>选择文件</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="btn-group tools">
                            <button type="button" class="btn btn-default zoomIn"><i class="fa fa-search-plus"></i></button>
                            <button type="button" class="btn btn-default zoomOut"><i class="fa fa-search-minus"></i></button>
                            <button type="button" class="btn btn-default rotateLeft"><i class="fa fa-rotate-left"></i></button>
                            <button type="button" class="btn btn-default rotateRight"><i class="fa fa-rotate-right"></i></button>
                            <button type="button" class="btn btn-default moveUp"><i class="fa fa fa-arrow-up"></i></button>
                            <button type="button" class="btn btn-default moveDown"><i class="fa fa fa-arrow-down"></i></button>
                            <button type="button" class="btn btn-default moveLeft"><i class="fa fa fa-arrow-left"></i></button>
                            <button type="button" class="btn btn-default moveRight"><i class="fa fa fa-arrow-right"></i></button>
                            <button type="button" class="btn btn-default mirrorLeft">左镜像</button>
                            <button type="button" class="btn btn-default mirrorRight">右镜像</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-default save">保存</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<script type="text/javascript">
    $(function () {
        $('.cropper-image').each(function () {
            var input = $(this);
            var imageRatio = input.attr('data-ratio');
            var thumbImageSrc = input.attr('data-src');
            var container = $('<div class="cropper-thumb">');
            input.parent().append(container);
            var thumbImage = new Image();
            thumbImage.className = 'thumb-image';

            container.append(thumbImage);
            if (thumbImageSrc) {
                thumbImage.src = thumbImageSrc;
            } else {
                $(thumbImage).hide();
            }
            var button = $('<button type="button" class="btn btn-primary">选择图片文件</button>');
            container.append(button);

            button.click(function () {
                var modal = $('#cropper-modal');
                var image = new Image();
                image.src = input.attr('data-src');
                image.height = 300;
                var $image = $(image);
                $('#image-container').empty().append(image);

                var cropBoxData;
                var canvasData;
                var options = {};
                if (imageRatio) {
                    options.aspectRatio = imageRatio;
                }
                options.preview = '.img-preview';
                options.built = function () {
                    $image.cropper('setCanvasData', canvasData);
                    $image.cropper('setCropBoxData', cropBoxData);
                };

                modal.modal('show');
                modal.on('shown.bs.modal', function () {
                    $image.cropper(options);
                }).on('hidden.bs.modal', function () {
                    $image.cropper('destroy');
                });

                // 选择本地文件
                var uploadInput = modal.find('#upload-file-input');
                var URL = window.URL || window.webkitURL;
                var blobURL;
                if (URL) {
                    uploadInput.change(function() {
                        var files = this.files;
                        var file;
                        if (files && files.length) {
                            file = files[0];
                            if (/^image\/\w+$/.test(file.type)) {
                                blobURL = URL.createObjectURL(file);
                                /*
                                $image.one('built.cropper', function () {
                                    URL.revokeObjectURL(blobURL);
                                }).cropper('reset').cropper('replace', blobURL);
                                console.log(blobURL);
                                */
                                $image.cropper('replace', blobURL);
                            } else {
                                console.log('选择的文件不是图片');
                            }
                        }
                    });
                }

                // 保存裁切的图片
                modal.find('.save').click(function () {
                    var src = $image.attr('src');
                    cropBoxData = $image.cropper('getCropBoxData');
                    canvasData = $image.cropper('getCanvasData');
                    convertToData(src, canvasData, cropBoxData, function(data) {
                        // 回调后的函数处理
                        thumbImage.src = data.base64;
                        input.text(data.base64);
                        //thumbImage.width = data.canvas.width;
                        //thumbImage.height = data.canvas.height;
                        modal.modal('hide');
                    });
                });
            });
        });
        // 生成base64文件编码
        function convertToData(url, canvasdata, cropdata, callback) {
            var cropw = cropdata.width; // 剪切的宽
            var croph = cropdata.height; // 剪切的宽
            var imgw = canvasdata.width; // 图片缩放或则放大后的高
            var imgh = canvasdata.height; // 图片缩放或则放大后的高
            var poleft = canvasdata.left - cropdata.left; // canvas定位图片的左边位置
            var potop = canvasdata.top - cropdata.top; // canvas定位图片的上边位置
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext('2d');
            canvas.width = cropw;
            canvas.height = croph;
            var img = new Image();
            img.src = url;
            img.onload = function() {
                this.width = imgw;
                this.height = imgh;
                // 这里主要是懂得canvas与图片的裁剪之间的关系位置
                ctx.drawImage(this, poleft, potop, this.width, this.height);
                var data = {};
                data.base64 = canvas.toDataURL('image/jpg', 1);  // 这里的“1”是指的是处理图片的清晰度（0-1）之间，当然越小图片越模糊，处理后的图片大小也就越小
                data.canvas = canvas;
                callback && callback(data)      // 回调base64字符串
            }
        }
    });
</script>