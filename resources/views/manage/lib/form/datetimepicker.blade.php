<!-- daterange picker -->
<link href="{{asset('asset/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<!-- date-range-picker -->
<script src="{{asset('asset/plugins/datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('asset/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js')}}"></script>
<script>
    $(function () {
        //Date picker
        $('.datetimepicker').each(function () {
            var el = $(this);
            var container = $('<div class="input-group date">');
            el.parent().append(container);
            var group = $('<div class="input-group-addon">');
            container.append(group);
            var icon = $('<i class="fa fa-calendar">');
            group.append(icon);
            container.append(el);
            el.datetimepicker({
                //language:  'fr',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: 'yyyy-mm-dd hh:ii'
            });
        });
    });
</script>