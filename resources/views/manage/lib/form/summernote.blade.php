<!-- include summernote css/js -->
<link href="{{asset('asset/plugins/summernote/summernote.css')}}" rel="stylesheet"/>
<script src="{{asset('asset/plugins/summernote/summernote.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        'use strict';

        // 富文本编辑器
        $('.summernote').summernote({
            'height': '320',
            callbacks: {
                onImageUpload: function(files) {
                    var data = new FormData();
                    for(var key in files) {
                        data.append('image[]', files[key]);
                    }
                    var el = $(this);
                    $.ajax({
                        url: el.attr('upload-image'),
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){
                            if (data.state == 'SUCCESS') {
                                for (var i in data.file) {
                                    //console.log(data.file[i]);
                                    el.summernote('editor.insertImage', data.file[i]);
                                }
                            } else {
                                alert(data.state);
                            }
                        }
                    });
                }
            }
        });
        //debug
        $('.popover').each(function () {
            $('#wrapper').append(this);
        });
    });
</script>