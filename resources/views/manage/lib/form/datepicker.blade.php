<!-- daterange picker -->
<link href="{{asset('asset/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">
<!-- bootstrap datepicker -->
<link href="{{asset('asset/AdminLTE/plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
<!-- Bootstrap time Picker -->
<link href="{{asset('asset/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
<!-- date-range-picker -->
<script src="{{asset('asset/AdminLTE/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('asset/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('asset/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('asset/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script>
    $(function () {
        //Date picker
        $('.datepicker').each(function () {
            var el = $(this);
            var container = $('<div class="input-group date">');
            el.parent().append(container);
            var group = $('<div class="input-group-addon">');
            container.append(group);
            var icon = $('<i class="fa fa-calendar">');
            group.append(icon);
            container.append(el);
            el.datepicker({
                autoclose: true
            });
        });
    });
</script>