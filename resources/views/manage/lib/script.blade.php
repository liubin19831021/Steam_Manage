<!-- jQuery 2.2.0 -->
<script src="{{asset('asset/jquery/jquery-2.2.1.min.js')}}"></script>
<script src="{{asset('asset/jquery/jquery.cookie.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('asset/bootstrap-3.3.5-dist/js/bootstrap.min.js')}}"></script>
<!-- pace -->
<script src="{{asset('asset/plugins/pace/pace.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('asset/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('asset/AdminLTE/plugins/fastclick/fastclick.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('asset/AdminLTE/plugins/iCheck/icheck.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('asset/AdminLTE/js/app.min.js')}}"></script>
<script src="{{asset('asset/AdminLTE/js/common.js')}}"></script>