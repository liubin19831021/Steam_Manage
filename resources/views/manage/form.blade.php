<!DOCTYPE html>
<html>
<head>
    <title>{{ $manage_title }}</title>
    @include('manage.lib.meta')
    @include('manage.lib.link')
</head>

<body class="hold-transition skin-{{ $skin }} sidebar-mini">
<div class="wrapper" id="wrapper">
    @include('manage.lib.header')
    @include('manage.lib.navigation')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div><strong>{{ $title }}</strong><small></small></div>
            @if(isset($crumbs))
                <ol class="breadcrumb">
                    @foreach ($crumbs as $item)
                        @if($item['active']==1)
                            <li class="active"><a href="#">{{$item['value']}}</a></li>
                        @else
                            <li><a href={{$item['url']}}>{{$item['value']}}</a></li>
                        @endif
                    @endforeach
                </ol>
            @endif
        </section>
        <!-- Main content -->
        <section class="content">
            @include('manage.lib.alert')
            <div class="box box-default">
                <div class="box-body">
                    @if(isset($nav))
                        @include($nav['name'])
                    @endif
                    @if(isset($msg))
                        <p>{{ $msg }}</p>
                    @endif
                    @if(isset($form))
                        {!! Form::open(['url' => $form_url, 'method' => $form_method, 'enctype'=>'multipart/form-data']) !!}
                        <div class="row">
                            @foreach ($form['element'] as $key => $value)
                                <div class="col col-md-{{ $value['col'] }}">
                                    <div class="form-group">
                                        @if (isset($value['name']))
                                            {{ Form::label($value['name']) }}
                                        @endif
                                        @if($value['type'] == 'checkbox-group')
                                            @foreach ($value['element'] as $group)
                                                <div class="form-group">
                                                    <div><label>{{ $group['name'] }}</label></div>
                                                    @foreach ($group['child'] as $k => $val)
                                                        <label>{{ $val['input'] }} {{ $val['name'] }}</label>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        @elseif ($value['type'] == 'checkbox' && is_array($value['element']))
                                            <div class="form-group">
                                                @foreach ($value['element'] as $k => $val)
                                                    <label>{{ $val['input'] }} {{ $val['name'] }}</label>
                                                @endforeach
                                            </div>
                                        @elseif ($value['type'] == 'checkbox-tree' && is_array($value['element']))
                                            <div class="form-group">
                                                @inject('ManageHelper', 'App\Ext\View\ManageHelper')
                                                <div class="checkbox-tree">{!! $ManageHelper->checkboxTree($key, $value['element'], $value['value']) !!}</div>
                                            </div>
                                        @elseif ($value['type'] == 'radio' && is_array($value['element']))
                                            <div class="form-group">
                                                @foreach ($value['element'] as $k => $val)
                                                    <label>{{ $val['input'] }} {{ $val['name'] }}</label>
                                                @endforeach
                                            </div>
                                        @else
                                            {!! $value['element'] !!}
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="row">
                        <div class="col col-md-12">
                            @if (isset($form_button))
                                @foreach ($form_button as $key => $value)
                                    @if ($value['type'] == 'submit')
                                        <button class="btn btn-primary" type="submit" {{ isset($value['route']) ? 'onclick=window.location.href="' .$value['route'] . '"' : '' }}>{{ $value['name'] }}</button>
                                    @else
                                        <button class="btn btn-white" type="{{ $value['type'] }}" {{ isset($value['route']) ? 'onclick=window.location.href="' .$value['route'] . '"' : '' }}>{{ $value['name'] }}</button>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    @include('manage.lib.version')
    @include('manage.lib.sidebar')
</div>
@include('manage.lib.script')
@if (!empty($form['ext']))
    @foreach($form['ext'] as $val)
        @include($val)
    @endforeach
@endif
</body>
</html>