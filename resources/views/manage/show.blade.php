<!DOCTYPE html>
<html>
<head>
    <title>{{ $manage_title }}</title>
    @include('manage.lib.meta')
    @include('manage.lib.link')
</head>

<body class="hold-transition skin-{{ $skin }} sidebar-mini">
<div class="wrapper" id="wrapper">
    @include('manage.lib.header')
    @include('manage.lib.navigation')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(isset($crumbs))
                <ol class="breadcrumb">
                    @foreach ($crumbs as $item)
                        @if($item['active']==1)
                            <li class="active"><a href="#">{{$item['value']}}</a></li>
                        @else
                            <li><a href={{$item['url']}}>{{$item['value']}}</a></li>
                        @endif
                    @endforeach
                </ol>
            @endif
        </section>
        <!-- Main content -->
        <section class="content">
            @include('manage.lib.alert')
            <div class="box box-default">
                <div class="box-title">
                    <h3>{{ $title }}</h3>
                </div>
                <div class="box-body">
                    @foreach ($data as $row)
                        <p>{!! $row !!}</p>
                    @endforeach
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    @include('manage.lib.version')
    @include('manage.lib.sidebar')
</div>
@include('manage.lib.script')
@if (!empty($form['ext']))
    @foreach($form['ext'] as $val)
        @include($val)
    @endforeach
@endif
</body>
</html>