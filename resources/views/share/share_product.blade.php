<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>分享页</title>
    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,initial-scale=1.0,width=device-width"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <meta property="fb:app_id" content="603513066495231" />
    <meta property="og:url" content="{{ $url }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="I win {{ $title }} in 1 Coin Rich" />
    <meta property="og:description" content="I win {{ $title }} in 1 Coin Rich" />
    <meta property="og:image" content={{ $path . $thumb }} />
    <link rel="stylesheet" href="{{asset('asset/mall/css/share.css')}}">
</head>
<body id="app">
<div class="share-all-content">
    <div class="share-right"></div>
    <div class="share-left"></div>
    <div class="share-all">
        <div class="share-pic">
            <img src="{{ $path . $thumb }}">
        </div>
        <div class="share-text">
            <p class="share-title">{{ $title }}</p>
            <p class="share-content">
            {{ $subtitle }}
        </div>
        <div class="share-btn">
            <div class="share-iphone">
                <a href="">
                    <div class="share-iphone-apple">
                        <i class="apple"></i>
                        <i class="apple-text"></i>
                    </div>
                </a>
            </div>
            <div class="share-android">
                <a href="">
                    <div class="share-iphone-apple">
                        <i class="android"></i>
                        <i class="android-text"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>