<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>分享页</title>
    <meta property="fb:app_id" content="603513066495231" />
    <meta property="og:url" content="{{ $url }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="I am in {{ $title }} lucky draw activity" />
    <meta property="og:description" content="I am in {{ $title }} lucky draw activity" />
    <meta property="og:image" content={{ $path . $thumb }} />
</head>
<body>
<h1>当前为分享页</h1>
<p style="color: #f00; font-size: 20px; font-weight: bold;"> 1. facebook分享页要加入头部中的og属性</p>
<div>
    <p style="color: #f00; font-size: 20px; font-weight: bold;"> 2. facebook测试应用中分享地址要注册</p>
    <p><a href="https://developers.facebook.com/tools/debug/og/object" target="_blank">前往注册地址</a></p>
    <img style="padding-left: 20px;" src="share_1.png" alt="分享注意事项">
</div>
</body>
</html>