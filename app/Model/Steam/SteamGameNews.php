<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGameNews extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_news';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}