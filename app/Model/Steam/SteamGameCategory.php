<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGameCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_game_category';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    public static function keyName()
    {
        $region = self::get();
        $result = array();
        foreach ($region as $value){
            $result[$value->id] = $value->name;
        }
        unset($region);
        return $result;
    }

}