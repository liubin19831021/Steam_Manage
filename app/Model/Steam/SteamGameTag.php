<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGameTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_game_tag';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    public static function keyName()
    {
        $region = self::get();
        $result = array();
        foreach ($region as $value){
            $result[$value->id] = $value->title;
        }
        unset($region);
        return $result;
    }

}