<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGame extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_game';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}