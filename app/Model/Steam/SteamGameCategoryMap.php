<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGameCategoryMap extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_game_category_map';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}