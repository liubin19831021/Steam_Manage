<?php
namespace App\Model\Steam;

use Illuminate\Database\Eloquent\Model;

class SteamGameTagMap extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'steam_game_tag_map';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}