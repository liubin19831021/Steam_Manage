<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'region';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function keyName()
    {
        $region = self::get();
        $result = array();
        foreach ($region as $value){
            $result[$value->id] = $value->name;
        }
        unset($region);
        return $result;
    }

    public static function name($province, $city = 0, $district = 0, $join = ' - ')
    {
        $id = [$province, $city, $district];
        $result = self::whereIn('id', $id)->lists('name')->toArray();
        if ($result) return implode($join, $result);
        return null;
    }
}