<?php
namespace App\Model\Game;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Game extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'game';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     *
     * 获取游戏名称
     *
     * @return array
     */
    public static function keyName()
    {
        $game = array();
        $result = self::get();
        foreach ($result as $key => $val) {
            $game[$val->id] = $val->name;
        }
        return $game;
    }

    /**
     * 属于该游戏的活动。
     */
    public function wechat_activity()
    {
        return $this->belongsToMany('App\Model\Wechat\WechatActivity', 'wechat_activity_game', 'game_id', 'wechat_activity_id');
    }
}
