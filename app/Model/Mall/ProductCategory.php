<?php
namespace App\Model\Mall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_category';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     *
     * 获取分类名称
     *
     * @return array
     */
    public static function keyName()
    {
        $category = array();
        $result = self::where('parent_id', 1)->get();
        foreach ($result as $key => $val) {
            $category[$val->id] = $val->name;
        }
        return $category;
    }
}
