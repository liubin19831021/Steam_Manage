<?php

namespace App\Model\Manage;

use Illuminate\Database\Eloquent\Model;

class ManageRole extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manage_role';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}