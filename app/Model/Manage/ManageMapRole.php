<?php

namespace App\Model\Manage;

use Illuminate\Database\Eloquent\Model;

class ManageMapRole extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manage_map_role';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}