<?php

namespace App\Model\Manage;

use Illuminate\Database\Eloquent\Model;

class ManageAction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manage_action';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     *
     * 查询管理员角色的全部权限，并返回分组格式
     *
     * @return array
     */
    public static function groupAction()
    {
        $result = self::get();
        $role = array();
        foreach ($result as $k => $v) {
            if ($v->parent_id == '0') {
                $role[$v->id] = array(
                    'name' => $v->name,
                    'value' => $v->id
                );
            } else {
                $role[$v->parent_id]['child'][] = array(
                    'name' => $v->name,
                    'value' => $v->id
                );
            }
        }
        return $role;
    }
}