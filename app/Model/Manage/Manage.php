<?php

namespace App\Model\Manage;

use Illuminate\Database\Eloquent\Model;

class Manage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'manage';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}