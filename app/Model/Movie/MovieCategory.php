<?php
namespace App\Model\Movie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MovieCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_category';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     *
     * 获取分类名称
     *
     * @return array
     */
    public static function keyName()
    {
        $category = array();
        $result = self::get();
        foreach ($result as $key => $val) {
            $category[$val->id] = $val->name;
        }
        return $category;
    }


}
