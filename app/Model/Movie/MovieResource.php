<?php
namespace App\Model\Movie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MovieResource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_resource';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}
