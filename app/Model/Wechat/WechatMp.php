<?php
namespace App\Model\Wechat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatMp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wechat_mp';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}
