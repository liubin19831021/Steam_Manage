<?php

namespace App\Model\Wechat;

use Illuminate\Database\Eloquent\Model;
class WechatMpGameAward extends Model
{
    protected $table = 'wechat_mp_game_award';
    public $timestamps = false;

}