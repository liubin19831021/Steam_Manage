<?php

namespace App\Model\Wechat;

use Illuminate\Database\Eloquent\Model;
class WechatMpGame extends Model
{
    protected $table = 'wechat_mp_game';
    public $timestamps = false;

}