<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserIntegral extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_integral';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *
     * 使用/获得积分
     *
     * @param $uid 用户ID
     * @param $integral 金额
     * @param $action 使用/获得区分（1为使用，2为获得）
     * @param $note 说明
     * @return bool 是否成功
     */
    public static function useIntegral($uid, $integral, $action, $note){
        $user = User::find($uid);
        # 判断$action是收入还是支出，来判断用户表的余额是增是减，如果是支出($action == 1)，查看一下积分是否充足
        if($action == 1){
            if($integral > $user->integral){
                return false;
            }
            $user->integral -= $integral;
            # 积分使用记录里，要保存负值
            $integral = 0 - $integral;
        }
        elseif ($action == 2){
            $user->integral += $integral;
        }
        DB::beginTransaction();# 事务开始
        # 修改User表integral字段数值
        if(!$user->save()){
            DB::rollBack();
            return false;
        }
        # 添加余额使用记录表
        $data = [
            'member_id' => $uid,
            'action' => $action,
            'number' => $integral,
            'note' => $note,
            'create_time' => currentTime(),
        ];
        $result = self::insert($data);
        if(!$result){
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }
}
