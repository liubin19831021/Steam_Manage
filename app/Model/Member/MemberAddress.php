<?php
namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class MemberAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_address';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *
     * 设置默认收货地址
     *
     * @param $info
     * @param $orderNum
     * @return bool
     */
    public static function focus($memberId, $addId)
    {
        # 事务开始
        DB::beginTransaction();
        # 将当前用户的所有收货地址的默认状态改为0
        try {
            self::where('member_id', $memberId)->update(['is_default' => 0]);
        }
        catch (Exception $e){
            DB::rollBack();
            return false;
        }
        # 将当前收货地址默认状态改为1
        $memberAddress = MemberAddress::find($addId);
        $memberAddress->is_default = 1;
        if(!$memberAddress->save()){
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }
}
