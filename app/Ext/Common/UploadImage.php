<?php
namespace App\Ext\Common;

use File;
use Image;

class UploadImage
{
    /**
     *
     * 保存文件对象
     *
     * @param $file
     * @param $objectPath 对象节点路径
     * @param $objectName 对象名称
     * @param $param 参数
     * example:
     * [
     *      'large' => ['w'=>1024, 'watermark'=>'image/water_mark.png'],
     *      'middle' => ['w'=>512, 'h'=>512],
     *      'small' => ['h'=>256],
     * ]
     *
     * @return bool
     */
    public function save($file, $objectPath, $objectName, $param = null)
    {
        # 移动临时文件到web文件目录
        if (!$file->move($objectPath, $objectName)) return false;
        $filePath = $objectPath . $objectName;
        # 裁切图像文件
        $this->imageHandle($filePath, $objectPath, $param);
        # 保存返回参数
        return asset($filePath);
    }

    /**
     *
     * 删除图像文件和批量处理的裁切图像文件
     *
     * @param $fileName
     * @param $path
     */
    public function delete($fileName, $path)
    {
        # 获取目录下的所有裁切图目录
        $files = [];
        if (is_array($path)) {
            foreach ($path as $val) {
                $files[] = $val . $fileName;
            }
        } else {
            $files[] = $path . $fileName;
        }
        # 批量删除
        File::delete($files);
    }

    /**
     *
     *
     *
     * @param $file
     * @param $type 文件类型限制
     * @return bool
     */
    public function checkMime($file, $type)
    {
        # 检查文件mime类型
        return true;
    }

    /**
     *
     * 根据参数对图片进行缩放裁切添加水印处理
     *
     * @param $file 文件路径
     * @param $path 保存目录
     * @param null $param 图片裁切参数
     * example:
     * [
     *      'large' => ['w'=>1024, 'watermark'=>'image/water_mark.png'],
     *      'middle' => ['w'=>512, 'h'=>512],
     *      'small' => ['h'=>256],
     * ]
     */
    public function imageHandle($file, $path, $param)
    {
        if ($param) {
            $fileName = basename($file);
            foreach ($param as $key => $value) {
                $img = Image::make($file);
                # 固定尺寸裁切
                if (isset($value['w']) && isset($value['h'])) {
                    $img->resize($value['w'], $value['h']);
                } else {
                    # 宽度固定，高度等比
                    if (isset($value['w'])) {
                        $width = $img->width();
                        if ($width > $value['w']) {
                            $height = $img->height() * ($value['w'] / $width);
                            $img->resize($value['w'], $height);
                        }
                    }
                    # 高度固定，宽度等比
                    if (isset($value['h'])) {
                        $height = $img->height();
                        if ($height > $value['h']) {
                            $width = $img->width() * ($value['h'] / $height);
                            $img->resize($width, $value['h']);
                        }
                    }
                }
                if (!empty($value['watermark']) && File::isFile($value['watermark']) == true) {
                    $img->insert($value['watermark'], 'bottom-right');
                }
                $handlePath = $path . $key . '/';
                if (!File::isDirectory($handlePath)) File::makeDirectory($handlePath);
                $img->save($handlePath . $fileName);
            }
        }
    }

    /**
     *
     * 图片添加水印处理
     *
     * @param $file 文件路径
     * @param $path 保存目录
     * @param $mark 水印图路径
     */
    public function waterMark($file, $path, $mark)
    {
        $img = Image::make($file);
        $img->insert($mark, 'bottom-right');
        $fileName = basename($file);
        $img->save($path . $fileName);
    }
}