<?php
namespace App\Ext\View;

class ManageHelper
{
    /**
     *
     * 列表操作菜单是否显示匹配整理
     *
     * @param $option 列表操作菜单
     * @param $row 用于匹配是否显示条件的行数据
     * @return array
     */
    public function optionRow($option, $row)
    {
        $condition = ['1' => '=', '2' => '<', '3' => '>', '4' => '<=', '5' => '>=', '6' => '!='];
        $result = [];
        foreach ($option as $key => $val) {
            if (isset($val['where'])) {
                $flag = true;
                foreach($val['where'] as $v) {
                    if (!is_numeric($v['condition'])) {
                        $v['condition'] = array_search($v['condition'], $condition);
                    }
                    switch ($v['condition']){
                        case 1:
                            # 允许数据等于条件值的显示
                            if ($row->$v['name'] != $v['value']) $flag = false;
                            break;
                        case 2:
                            # 允许数据小于条件值的显示
                            if ($row->$v['name'] >= $v['value']) $flag = false;
                            break;
                        case 3:
                            # 允许数据大于条件值的显示
                            if ($row->$v['name'] <= $v['value']) $flag = false;
                            break;
                        case 4:
                            # 允许数据小于等于条件值的显示
                            if ($row->$v['name'] > $v['value']) $flag = false;
                            break;
                        case 5:
                            # 允许数据大于等于条件值的显示
                            if ($row->$v['name'] < $v['value']) $flag = false;
                            break;
                        case 6:
                            # 允许数据不等于条件值的显示
                            if ($row->$v['name'] == $v['value']) $flag = false;
                            break;
                    }
                    if (!$flag) continue;
                }
                if (!$flag) continue;
            }
            $param = ['id' => $row->id];
            # 需要传值的记录字段值
            if (isset($val['field'])) {
                foreach ($val['field'] as $v) {
                    $param[$v] = $row->$v;
                }
            }
            # 其它附加参数
            if (isset($val['param'])) $param += $val['param'];
            $temp = [];
            $temp['title'] = $val['title'];
            if (!empty($val['route'])) $temp['route'] = route($val['route'], $param);
            if (!empty($val['load'])) $temp['load'] = route($val['load'], $param);
            $result[$key] = $temp;
        }
        return $result;
    }

    /**
     *
     * 树形多选列表
     *
     * @param $name 控件名
     * @param $data 树形结构数据
     * @param int $level 层级
     * @param null $parent 父级ID
     * @return string
     */
    public function checkboxTree($name, $data, $value, $level = 1, $parent = null)
    {
        $collapse = ($level > 1) ? 'collapse' : '';
        $html = '<ul id="' . $parent . '" class="' . $collapse . '">';
        foreach ($data as $key => $val) {
            $html .= '<li class="checkbox">';
            $parent = 'collapse-' . $name . '-' . $key;
            if (isset($val['child'])) {
                $html .= '<a class="collapse-list" data-toggle="collapse" data-parent="#accordion" href="#' . $parent . '"><i class="fa fa fa-plus-square-o"></i></a>';
            }
            $checked = in_array($key, $value) ? 'checked' : '';
            $html .= '<label for="' . $name . '-' . $key . '"><input type="checkbox" class="checkbox-input" id="' . $name . '-' . $key . '" name="' . $name . '[]" value="' . $key .'" ' . $checked . ' />' . $val['name'] . '</label>';
            if (isset($val['child'])) {
                $html .= $this->checkboxTree($name, $val['child'], $value, $level + 1, $parent);
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }
}