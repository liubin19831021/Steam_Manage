<?php

/**
 *
 * 公共正则表达式
 *
 * @param $value
 * @return mixed
 */
function preg($value) {
    $data = [
        'phone' => '/^1[3|4|5|7|8][0-9]\d{8}$/i',
    ];
    if (isset($data[$value])) return $data[$value];
}

/**
 *
 * 是否使用
 *
 * @param $value
 * @return mixed|null
 */
function isUse($value = null) {
    $data = [
        '0' => '不使用',
        '1' => '使用',
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}
/**
 *
 * 管理员账号状态
 *
 * @param $value
 * @return mixed|null
 */
function manageStatus($value) {
    $data = [
        'enabled' => '启用',
        'disabled' => '禁用',
    ];
    if (isset($data[$value])) return $data[$value];
    return null;
}

/**
 *
 * 性别
 *
 * @param $value
 * @return mixed|null
 */
function sex($value = null) {
    $data = [
        '1' => '男',
        '2' => '女',
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 影视类型
 *
 * @param $value
 * @return mixed|null
 */
function movieType($value = null) {
    $data = [
        '0' => '电影',
        '1' => '电视剧',
        '2' => '综艺节目'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}
/**
 *
 * 影视类型
 *
 * @param $value
 * @return mixed|null
 */
function resourcesType($value = null) {
    $data = [
        '0' => '标情',
        '1' => '高清',
        '2' => '超清',
        '3' => '1080P'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 游戏商品类型
 *
 * @param $value
 * @return mixed|null
 */
function gameGoodsType($value = null) {
    $data = [
        '1' => '商品版本',
        '2' => '捆绑包',
        '3' => 'DLC'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 启用与禁用
 *
 * @param $value
 * @return mixed|null
 */
function disable($value = null) {
    $data = [
        'enabled' => '启用',
        'disabled' => '禁用',
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}
/**
 *
 * 系统配置
 *
 * @param $value
 * @return mixed|null
 */
function collocationType($value = null) {
    $data = [
        '1' => 'window',
        '2' => 'mac',
        '3' => 'linux'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}
/**
 *
 * 是与否
 *
 * @param $value
 * @return mixed|null
 */
function YN($value = null) {
    $data = [
        '1' => '是',
        '0' => '否'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 * 比较符号
 * @param $value
 * @return mixed|null
 */
function condition($value = null) {
    $data = [
        '1' => '=',
        '2' => '<',
        '3' => '>',
        '4' => '<=',
        '5' => '>=',
        '6' => '<>',
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 * 比较符号
 * @param $value
 * @return mixed|null
 */
function winWhereType($value = null) {
    $data = [
        '0' => '等于',
        '1' => '大于',
        '2' => '小于',
        '3' => '不等于',
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 * 红包类型
 * @param $value
 * @return mixed|null
 */
function mpRedpackType($value = null) {
    $data = [
        '0' => '普通红包',
        '1' => '裂变红包'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 * 奖品类型
 * @param $value
 * @return mixed|null
 */
function awardType($value = null) {
    $data = [
        '0' => '商品',
        '1' => '红包',
        '2' => '卡券'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 审核状态
 *
 * @param $value
 * @return array|mixed
 */
function reviewStatus($value = null) {
    $data = [
        '0' => '等待审核',
        '1' => '审核通过',
        '2' => '审核未通过'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 订单状态
 *
 * @return array|mixed|null
 */
function orderStatus($value = null) {
    $data = [
        '0' => '未付款',
        '1' => '已经付款',
        '2' => '未中奖',
        '3' => '已中奖',
        '4' => '未退款',
        '5' => '已退款',
    ];
    return ($value !== null) ? $data[$value] : $data;
}

/**
 *
 * 开奖状态
 *
 * @return array|mixed|null
 */
function historyStatus($value = null)
{
    $data = [
        '0' => '未开奖',
        '1' => '已开奖',
        '2' => '已作废（未退款）',
        '3' => '已退款'
    ];
    return ($value !== null) ? $data[$value] : $data;
}

/**
 *
 * 订单物流状态
 *
 * @return array|mixed|null
 */
function logisticStatus($value = null)
{
    $data = [
        '0' => '未发货',
        '1' => '已发货',
        '2' => '已确认收货',
        '3' => '已晒单'
    ];
    return ($value !== null) ? $data[$value] : $data;
}

/**
 *
 * 订单物流类型
 *
 * @return array|mixed|null
 */
function logisticType($value)
{
    $data = [
        '1' => '发货',
        '2' => '补发',
        '3' => '换货',
        '4' => '退货'
    ];
    return ($value !== null) ? $data[$value] : $data;
}

/**
 *
 * 物流公司
 *
 * @param null $value
 * @return array|mixed
 */
function logistics($value = null) {
    $data = [
        '1' => '顺丰',
        '2' => '申通',
        '3' => '韵达',
        '4' => '圆通',
        '6' => '汇通',
        '7' => '中通',
        '8' => '德邦',
        '9' => '天天快递',
        '10' => '全一快递',
        '11' => '全峰快递',
        '12' => '新邦物流 ',
        '13' => '优速快递',
        '14' => '宅急送 ',
        '15' => '安信达',
        '16' => '中铁快运',
        '17' => '优速快递',
        '18' => '中邮物流',
        '19' => '希伊艾斯',
        '20' => 'EMS'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 星级
 *
 * @param null $value
 * @return array|mixed
 */
function star($value = null) {
    $data = [
        '6' => '3星',
        '8' => '4星',
        '10' => '5星'
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 正则表达式
 *
 * @param null $value
 * @return array|mixed|null
 */
function pattern($value = null) {
    $data = [
        'phone' => '/^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\\d{8}$/i', # 手机号
        'price' => '/^([0-9]|([1-9][0-9]{0,9}))((\.[0-9]{1,2})?)$/', # 金额（正浮点数）
        'emall' => '/^(([0-9a-zA-Z]+)|([0-9a-zA-Z]+[_.0-9a-zA-Z-]*[0-9a-zA-Z]+))@([a-zA-Z0-9-]+[.])+([a-zA-Z]{2}|net|NET|com|COM|gov|GOV|mil|MIL|org|ORG|edu|EDU|int|INT)$/', # 电子邮箱
        'positive_integer' => '/^\d+$/', # 年龄（正整数）
        'integer' => '/^-?\d+$/', # 整数
        'floating' => '/^(-?\d+)(\.\d+)?$/', # 浮点数
        'card' => '/^\\d{17}(\\d|x)$/', # 身份证号
        'url' => "@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@", # url地址
        'chinese_characters' => '/^[\x{4e00}-\x{9fa5}A-Za-z0-9_]+$/u', # 汉字
        'special' => '/[^%&\',;=?$\x22]+/', # 是否由特殊符号组成
        'password' => '/^[a-zA-Z]\w{5,17}$/', # 密码格式（以字母开头，长度在6-18之间，只能包含字符、数字、下划线）
        'month' => '/^(0?[1-9]|1[0-2])$/', # 月份数
        'days' => '/^((0?[1-9])|((1|2)[0-9])|30|31)$/', # 日期数（1-31）
        'letter_number' => '/^[A-Za-z0-9]+$/', # 字母与数字组合
        'letter_number_underline' => '/^\w+$/', # 字母与数字组合
    ];
    if (isset($data[$value])) return $data[$value];
    return ($value != null) ? null : $data;
}

/**
 *
 * 公共上传文件路径
 *
 * @param null $value
 * @return array|mixed|null
 */
function path($value = null) {
    $data = [
        'store_logo' => 'upload/image/store/logo/', # 商户LOGO
        'store_content' => 'upload/image/store/content/', # 商户介绍内容图
        'ad' => 'upload/image/ad/', # 广告图
        'product_picture' => 'upload/image/product/', # 商品展示图
        'product_thumb' => 'upload/image/product/thumb/', # 商品缩略图
        'product_article' => 'upload/image/product/article/', # 商品内容图
        'about' => 'upload/image/about/', # 关于我们内容图
        'activity' => 'upload/image/activity/', # 活动管理内容图
        'brand' => 'upload/image/brand/', # 品牌LOGO
        'user_avatar' => 'upload/image/user/avatar/', # 会员头像
        'activity_thumb'=>'upload/image/activity/thumb/' ,  # 活动页图片
        'comment'=>'upload/image/comment/' ,  # 晒单图片
        'movie'=>'upload/image/movie/' ,  # 影视图片
        'game'=>'upload/image/game/' ,  # 游戏图片
        'msg'=>'upload/image/msg/' ,  # 站内信图片
        'content_msg'=>'upload/image/content/' ,  # 站内信文本编辑器图片
        'steam_game_picture' => 'upload/image/steam/game/', # 游戏展示图
        'steam_game_thumb' => 'upload/image/steam/game/thumb/', # 游戏缩略图
        'steam_game_article' => 'upload/image/steam/game/article/', # 游戏内容图

    ];
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}

/**
 *
 * 公共文件路径
 *
 * @param null $value
 * @return array|mixed|null
 */
function filePath($value = null) {
     $data = [
         'water_mark' => 'asset/water_mark.png', # 水印图文件路径
     ];
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}

function aboutCategory($value = null) {
    $data = [
        '1' => '购物指南',
        '2' => '物流配送',
        '3' => '售后服务',
        '4' => '特色服务',
    ];
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}

function activityCategory($value = null) {
    $data = [
        '1' => '购物指南',
        '2' => '物流配送',
        '3' => '售后服务',
        '4' => '特色服务',
        '5' => '特色服务',
        '6' => '特色服务',
    ];
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}


/**
 * 苹果支付金额键值对
 * @param null $value
 * @return array|mixed|null
 */
function appleProduct($value = null) {
    $data = [
        'com.ebo.yydb.100' => 100,
        'com.ebo.yydb.500' => 500,
        'com.ebo.yydb.1000' => 1000,
        'com.ebo.yydb.2500' => 2500,
        'com.ebo.yydb.5000' => 5000,
        'com.ebo.yydb.10000' => 10000
    ];
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}

function apiMsg($value = null, $code = '', $language = 0) {
    if($language == 1) {
        $data = [
            'query.success' => '查询成功',
            'query.fail' => '查询成功',
            'partake.success' => '参与成功',
            'confirm.success' => '收货成功',
            'comment.sun.success' => '上传成功',
            'comment.success' => '晒单成功',
            'avatar.upload.success' => '头像上传成功',
            'order.logistic.success' => '物流单填写成功',
            'login.success' => '登录成功',
            'register.success' => '注册成功',
            'register.code.message' => '【夺宝】您好，您的验证码：' . $code . '。2小时内有效', 
            'register.code.success' => '短信发送成功',
            'forgot.password.success' => '修改成功',
            'logout.success' => '退出登录成功',
            'update.password.success' => '修改成功',
            'share.success' => '分享成功',
            'address.delete.success' => '删除成功',
            'address.focus.success' => '默认地址设置成功',
            'address.save.success' => '收货地址保存成功',
            'member.update.success' => '修改成功',
            'iap.validate.success' => '充值成功',
            'no.id' => '未找到id',
            'no.member' => '用户不存在',
            'no.password' => '密码不能为空',
            'no.member.id' => '未找到用户id',
            'no.order.id' => '未找到订单id',
            'no.adds.id' => '未找到地址id',
            'no.token' => '用户信息错误',
            'account.password.error' => '用户名或密码不正确',
            'confirm.fail' => '收货失败',
            'upload.file.error1' => '超过上传文件大小限制，文件上传的最大限制为:',
            'upload.file.error2' => '上传的文件格式限制为"jpg,jpeg,png',
            'upload.file.error3' => '文件上传失败',
            'history.info2' => '开奖活动未找到',
            'history.info3' => '活动尚未开始',
            'history.info4' => '商品未找到',
            'product.description2' => '该商品不存在',
            'product.announced2' => '未找到商品',
            'order2' => '商品不存在',
            'order3' => '该商品不能开奖',
            'order4' => '活动尚未开始',
            'order5' => '账户余额不足',
            'order6' => '超出购买限制，每个会员最多购买个数：',
            'order7' => '该商品可购买份数已不足，请减少购买数量',
            'order8' => '参与夺宝使用余额，商品名称：',
            'order9' => '下单失败',
            'order.cart2' => '商品未找到',
            'order.cart3' => '下单失败',
            'order.cart4' => '下单失败，有些活动尚未开始',
            'order.cart5' => '该商品不能参与夺宝',
            'order.cart6' => '超出购买限制',
            'order.cart7' => '该商品可购买份数已不足，请减少购买数量',
            'order.cart8' => '账户余额不足',
            'order.cart9' => '参与夺宝使用余额，商品名称：',
            'order.cart10' => '结算失败',
            'comment.sun1' => '未找到晒单图片',
            'comment1' => '订单未找到',
            'comment2' => '商品未找到',
            'comment3' => '晒单失败',
            'order.logistic4' => '未找到该订单',
            'order.logistic5' => '没有操作该订单的权限',
            'order.logistic6' => '该订单不可配送',
            'order.logistic7' => '您已经填写过物流单',
            'order.logistic8' => '配送地址不正确',
            'order.logistic9' => '此商品不存在',
            'order.logistic11' => '您的余额不足',
            'order.logistic12' => '物流单填写失败',
            'login1' => '用户名密码不能为空',
            'login2' => '账号格式不正确',
            'login3' => '账号不存在',
            'login4' => '登录错误次数过多，请24小时后再进行登录！',
            'login5' => '用户名或密码不正确',
            'login6' => '登录信息写入失败',
            'register1' => '用户名不能为空',
            'register3' => '短信验证码不能为空',
            'register4' => '手机格式不正确',
            'register5' => '密码格式不正确',
            'register6' => '确认密码不能为空',
            'register7' => '确认密码格式不正确',
            'register8' => '两次密码不一致',
            'register9' => '短信验证码格式不正确',
            'register10' => '短信验证码不正确',
            'register11' => '短信验证码已过期，请重新发送！',
            'register12' => '账号已经被注册',
            'register13' => '注册失败',
            'register.code1' => '手机格式不正确',
            'register.code2' => '国家代码格式不正确',
            'register.code3' => '手机号：' . $code . '已注册',
            'register.code4' => '短信发送失败',
            'register.code5' => '生成短信验证码错误',
            'forgot.password12' => '修改失败',
            'forgot.code3' => '用户不存在',
            'update.password6' => '新密码不能为空',
            'update.password7' => '新密码格式不正确',
            'update.password8' => '确认密码不能为空',
            'update.password9' => '确认密码格式不正确',
            'update.password10' => '两次密码不一致',
            'update.password11' => '修改失败',
            'share2' => '未找到分享用户用户ID',
            'share4' => '未找到分享用户',
            'share5' => '不能分享给自己',
            'share6' => '该用户是您分享的，不可交叉分享',
            'share7' => '你们已是好友，不可重复分享',
            'share8' => '分享好友失败',
            'address1' => '没有找到这条地址信息',
            'address2' => '无权管理当前地址',
            'address3' => '删除失败',
            'address4' => '默认地址设置失败',
            'address5' => '收货人不能为空',
            'address6' => '手机格式不正确',
            'address7' => '邮编为空或格式不正确',
            'address8' => '所在省市区格式不正确',
            'address9' => '所在省和市不能都为空',
            'address10' => '所在城市未找到',
            'address11' => '所在省未找到',
            'address12' => '详细地址不能为空',
            'address13' => '收货地址保存失败',
            'address14' => '数据异常',
            'order.status1' => '未开奖',
            'order.status2' => '中奖',
            'order.status3' => '未中奖',
            'my.record1' => '数据异常',
            'member.update3' => '修改失败',
            'avatar.upload2' => '没有接受到上传的文件',
            'avatar.upload4' => '未找到该用户',
            'avatar.upload5' => '上传失败',
            'msg2' => '未找到站内信',
            'msg3' => '数据异常',
            'iap.validate3' => '未找到产品',
            'iap.validate4' => 'token没找到',
            'iap.validate5' => 'transaction_id没找到',
            'iap.validate6' => '重复交易',
            'iap.validate7' => '交易凭证没找到',
            'iap.validate8' => '交易验证失败：',
            'iap.validate9' => '交易验证失败： 来源异常',
            'iap.validate10' => '金额信息异常',
            'iap.validate11' => '充值信息记录添加失败',
            'iap.validate12' => '充值失败',
            'iap.validate13' => '苹果支付记录异常',
            'iap.validate.str' => '苹果支付充值'
        ];
    }
    elseif($language == 0){
        $data = [
            'query.success' => 'check successfully',
            'query.fail' => 'check failed',
            'partake.success' => 'join successfully',
            'confirm.success' => 'take delivery successfully',
            'comment.sun.success' => 'upload successfully',
            'comment.success' => 'showing successfully',
            'avatar.upload.success' => 'head portrait upload successfully',
            'order.logistic.success' => 'fill in logistics successfully',
            'login.success' => 'log in successfully',
            'register.success' => 'register successfully',
            'register.code.message' => '【1 coin rich】hello,your verification code is:'. $code . ',valid in 2hours.',
            'register.code.success' => 'send note successfully',
            'forgot.password.success' => 'modify successfully',
            'logout.success' => 'log out successfully',
            'update.password.success' => 'modify successfully',
            'share.success' => 'share successfully',
            'address.delete.success' => 'delete successfully',
            'address.focus.success' => 'set default address successfully',
            'address.save.success' => 'save delivery address successfully ',
            'member.update.success' => 'modify successfully',
            'iap.validate.success' => 'recharge successfully',
            'no.id' => 'id not found',
            'no.member' => 'user does not exist',
            'no.password' => 'the password can not be empty',
            'no.member.id' => 'user id not found',
            'no.order.id' => 'order id not found',
            'no.adds.id' => 'address id not found',
            'no.token' => 'user infomation is wrong',
            'account.password.error' => 'user name or password is wrong',
            'confirm.fail' => 'take delivery failed',
            'upload.file.error1' => 'over the size limit of documents, the maximum limitation is:',
            'upload.file.error2' => 'the document format must be jpg,jpeg,png',
            'upload.file.error3' => 'upload failed',
            'history.info2' => 'draw activity not found',
            'history.info3' => 'the activity has not yet start',
            'history.info4' => 'goods not found',
            'product.description2' => 'goods not exist',
            'product.announced2' => 'goods not found',
            'order2' => 'goods not exist',
            'order3' => 'the good can not be drawed',
            'order4' => 'the activity has not yet start',
            'order5' => 'the account balance is insufficient',
            'order6' => 'over buying limitation, the maximun buying number of each member is:',
            'order7' => 'the number of goods are not enough,please reduce your buying number',
            'order8' => 'join in 1 coin rich and use your balance, the name of good is:',
            'order9' => 'order failed',
            'order.cart2' => 'goods not found',
            'order.cart3' => 'order failed',
            'order.cart4' => 'order failed, the activity is not yet start',
            'order.cart5' => 'the good can not  join in drawin',
            'order.cart6' => 'over the buying limitation',
            'order.cart7' => 'the number of goods are not enough,please reduce your buying number',
            'order.cart8' => 'the account balance is insufficient',
            'order.cart9' => 'join in 1 coin rich and use your balance, the name of good is:',
            'order.cart10' => 'settle failed',
            'comment.sun1' => 'showing pictures not found',
            'comment1' => 'order not found',
            'comment2' => 'goods not found',
            'comment3' => 'showing failed',
            'order.logistic4' => 'the order not found',
            'order.logistic5' => 'you don\'t have the authority to operate this order',
            'order.logistic6' => 'the order can bot be delivered',
            'order.logistic7' => 'you  had fill in the logistics before',
            'order.logistic8' => 'the delivery address is wrong ',
            'order.logistic9' => 'goods not exist',
            'order.logistic11' => 'your balance is insufficient',
            'order.logistic12' => 'Fill logistics info. Failed.',
            'login1' => 'Please enter user name and password',
            'login2' => 'Wrong format of account',
            'login3' => 'Unexisted account',
            'login4' => 'Too many login errors. Please try it again after 24 hours.',
            'login5' => 'Wrong user name or password',
            'login6' => 'Enter login info. Failed.',
            'register1' => 'Please enter user name',
            'register3' => 'Please enter captcha',
            'register4' => 'Wrong format of phone number',
            'register5' => 'Wrong format of password',
            'register6' => 'Please enter password',
            'register7' => 'Wrong format of retyped password',
            'register8' => 'Two passwords are inconsistent.',
            'register9' => 'Wrong format of message captcha.',
            'register10' => 'Incorrect captcha',
            'register11' => 'Message Captcha is over time, please send again.',
            'register12' => 'The account has been registered.',
            'register13' => 'Register failed',
            'register.code1' => 'Wrong format of phone number',
            'register.code2' => 'Wrong format of national code',
            'register.code3' => 'Phone number: '. $code . 'has been registered.',
            'register.code4' => 'Messaging failed',
            'register.code5' => 'Send message captche failed',
            'forgot.password12' => 'Revise failed',
            'forgot.code3' => 'Unfound user',
            'update.password6' => 'Please enter new password',
            'update.password7' => 'wrong format of new password',
            'update.password8' => 'Please retype password',
            'update.password9' => 'The format of retyped password is incorrect.',
            'update.password10' => 'Two passwords are inconsistent.',
            'update.password11' => 'Revise failed',
            'share2' => 'Unfound share ID',
            'share4' => 'unfound share user.',
            'share5' => 'Unallow to share to youself.',
            'share6' => 'You can\'t make crossed sharing. ',
            'share7' => 'You are already friends, unable to repeated sharing',
            'share8' => 'Share failed',
            'address1' => 'This address is unfound',
            'address2' => 'Unauthorized management of the current address.',
            'address3' => 'Deletion failed',
            'address4' => 'Setting of defaulted address failed.',
            'address5' => 'Please enter Consignee.',
            'address6' => 'Wrong format of phone number',
            'address7' => 'Wrong format of postcode.',
            'address8' => 'wrong format of address',
            'address9' => 'Please enter your province and city.',
            'address10' => 'Unfound city',
            'address11' => 'Unfound Province',
            'address12' => 'Please enter detailed address.',
            'address13' => 'Storage of delivery address failed.',
            'address14' => 'Data anomaly',
            'order.status1' => 'Unexposed Winner',
            'order.status2' => 'Win',
            'order.status3' => 'No winning',
            'my.record1' => 'Data anomaly',
            'member.update3' => 'Revise failed',
            'avatar.upload2' => 'Uploaded document is not received.',
            'avatar.upload4' => 'This user is unfound',
            'avatar.upload5' => 'Upload failed',
            'msg2' => 'No found platform information',
            'msg3' => 'Data Anomaly',
            'iap.validate3' => 'No found products',
            'iap.validate4' => 'No found token',
            'iap.validate5' => 'transaction_id没找到',
            'iap.validate6' => 'No found id',
            'iap.validate7' => 'No found trade certification',
            'iap.validate8' => 'Trade Vertification Failed',
            'iap.validate9' => 'The trading captache failed, unknown error',
            'iap.validate10' => 'Balance cann\'t present',
            'iap.validate11' => 'Top up Record Addition Failed',
            'iap.validate12' => 'Top up Failed',
            'iap.validate13' => 'Apple Payment Record Error',
            'iap.validate.str' => 'Apple Payment'
        ];
    }
    if ($value != null) return isset($data[$value]) ? $data[$value] : null;
    return $data;
}
