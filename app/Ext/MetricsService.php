<?php
namespace App\Ext;

class MetricsService
{

    /**
     *
     * 列表操作菜单是否显示匹配整理
     *
     * @param $option 列表操作菜单
     * @param $row 用于匹配是否显示条件的行数据
     * @return array
     */
    function optionRow($option, $row)
    {
        $condition = ['1' => '=', '2' => '<', '3' => '>', '4' => '<=', '5' => '>=', '6' => '!=', '7' => 'same'];
        $result = [];
        foreach ($option as $key => $val) {
            if (isset($val['where'])) {
                $flag = true;
                foreach($val['where'] as $v) {
                    if (!is_numeric($v['condition'])) {
                        $v['condition'] = array_search($v['condition'], $condition);
                    }
                    switch ($v['condition']){
                        case 1:
                            # 允许数据等于条件值的显示
                            if ($row->$v['name'] != $v['value']) $flag = false;
                            break;
                        case 2:
                            # 允许数据小于条件值的显示
                            if ($row->$v['name'] >= $v['value']) $flag = false;
                            break;
                        case 3:
                            # 允许数据大于条件值的显示
                            if ($row->$v['name'] <= $v['value']) $flag = false;
                            break;
                        case 4:
                            # 允许数据小于等于条件值的显示
                            if ($row->$v['name'] > $v['value']) $flag = false;
                            break;
                        case 5:
                            # 允许数据大于等于条件值的显示
                            if ($row->$v['name'] < $v['value']) $flag = false;
                            break;
                        case 6:
                            # 允许数据不等于条件值的显示
                            if ($row->$v['name'] == $v['value']) $flag = false;
                            break;
                        case 7:
                            # 允许两列数据相同的值的显示
                            if ($row->$v['name'] != $row->$v['value']) $flag = false;
                            break;
                    }
                    if (!$flag) continue;
                }
                if (!$flag) continue;
            }
            $param = ['id' => $row->id];
            # 需要传值的记录字段值
            if (isset($val['field'])) {
                foreach ($val['field'] as $v) {
                    $param[$v] = $row->$v;
                }
            }
            # 其它附加参数
            if (isset($val['param'])) $param += $val['param'];
            $temp = [];
            $temp['title'] = $val['title'];
            if (!empty($val['route'])) $temp['route'] = route($val['route'], $param);
            if (!empty($val['load'])) $temp['load'] = route($val['load'], $param);
            $result[$key] = $temp;
        }
        return $result;
    }
}