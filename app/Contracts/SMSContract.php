<?php
namespace App\Contracts;

interface SMSContract
{
    /**
     * 获取普通TOKEN
     * @param $phone
     * @param $msg
     * @return mixed
     */
    public function send($phone, $msg);
}