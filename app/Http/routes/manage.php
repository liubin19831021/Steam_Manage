<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| 来宾入口
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'as' => 'manage.',
        'prefix' => 'manage',
        'namespace' => 'Manage',
    ],
    function () {

        # 登录页面
        Route::get('login', ['as' => 'login', 'uses' => 'GuestController@login']);

        # 验证登录
        Route::post('login', ['as' => 'auth', 'uses' => 'GuestController@auth']);

        # 退出登录
        Route::get('logout', ['as' => 'logout', 'uses' => 'GuestController@logout']);

    }
);

/*
|--------------------------------------------------------------------------
| 基本功能
|--------------------------------------------------------------------------
*/


Route::group(
    [
        'prefix' => 'manage',
        'namespace' => 'Manage',
        'middleware' => 'auth.manage'
    ],
    function() {
        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.'
            ],
            function() {
                # 主页
                Route::get('index', ['as' => 'index', 'uses' => 'IndexController@index']);
                Route::get('test', ['as' => 'test', 'uses' => 'IndexController@testIndex']);
            }
        );

    }
);

Route::group(
    [
        'prefix' => 'manage',
        'namespace' => 'Manage\Manage',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 管理员列表
                Route::resource('user/resource', 'ManageController');

                # 管理员角色授权
                Route::resource('user/rule/resource', 'ManageRuleController', ['only' => ['index', 'store']]);

                # 管理员角色列表
                Route::resource('role/resource', 'RoleController');

                # 管理员角色动作列表
                Route::resource('role/action/resource', 'RoleActionController');

                # 配送地区管理列表
                Route::resource('region/resource', 'RegionController');

            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.'
            ],
            function() {
                # 修改密码
                Route::post('password', ['as' => 'password', 'uses' => 'ManageController@password']);

            }
        );

    }
);


Route::group(
    [
        'prefix' => 'manage/member',
        'namespace' => 'Manage\Member',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 会员列表
                Route::resource('resource', 'MemberController');
                # 会员收货地址管理
                Route::resource('adds/resource', 'MemberAddressController');
                # 卖家列表
                Route::resource('stores/resource', 'StoreController');

            }
        );
        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.member.'
            ],
            function() {
                # 会员启用禁用设置
                Route::post('disable/{id}', ['as' => 'disable', 'uses' => 'MemberController@disable']);

                # 微信公众账号详细描述内容图片上传
                Route::post('stores/{id}/upload', ['as' => 'stores.upload', 'uses' => 'StoreController@upload']);
                # 商户绑定会员
                Route::get('stores/{id}/bind', ['as' => 'stores.bind', 'uses' => 'StoreController@bind']);
                # 商户绑定会员
                Route::post('stores/{id}/bind', ['as' => 'stores.bind', 'uses' => 'StoreController@setBind']);
                # 商户会员解绑
                Route::post('stores/{id}/unbind', ['as' => 'stores.unbind', 'uses' => 'StoreController@setBind']);

                # 设为默认地址
                Route::post('adds/{id}/default', ['as' => 'adds.default', 'uses' => 'MemberAddressController@setDefault']);
            }
        );
    }
);
/*
|--------------------------------------------------------------------------
| Steam管理
|--------------------------------------------------------------------------
*/
Route::group(
    [
        'prefix' => 'manage/steam',
        'namespace' => 'Manage\Steam\Game',
        'middleware' => 'auth.manage'
    ],
    function() {

        # steam游戏

        Route::group(
            [],
            function() {

                # steam游戏列表
                Route::resource('game/resource', 'SteamGameController');
                # steam游戏商品
                Route::resource('game/goods/resource', 'SteamGameGoodsController');
                # steam游戏分类列表
                Route::resource('game/category/resource', 'SteamGameCategoryController');
                # steam游戏标签
                Route::resource('game/tag/resource', 'SteamGameTagController');
                # steam游戏配置
                Route::resource('game/collocation/resource', 'SteamGameCollocationController');

                # steam游戏新闻管理
                Route::resource('game/news/resource', 'SteamGameNewsController');

                # steam游戏新闻频道管理
                Route::resource('game/channel/resource', 'SteamGameChannelController');
            }
        );
        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.steam.'
            ],
            function() {

                # 游戏详细描述
                Route::get('game/{id}/describe', ['as' => 'game.describe', 'uses' => 'SteamGameDescribeController@index']);

                # 详细描述内容编辑
                Route::post('game/{id}/describe/update', ['as' => 'game.describe.update', 'uses' => 'SteamGameDescribeController@update']);

                # 详细描述内容图片上传
                Route::post('game/{id}/describe/upload', ['as' => 'game.describe.upload', 'uses' => 'SteamGameDescribeController@upload']);

                # 图册列表
                Route::get('game/{id}/picture', ['as' => 'game.picture', 'uses' => 'SteamGamePictureController@index']);

                # 上传图册的图片文件
                Route::post('game/picture/upload', ['as' => 'game.picture.upload', 'uses' => 'SteamGamePictureController@upload']);

                # 删除图册的图片文件
                Route::post('game/picture/delete', ['as' => 'game.picture.delete', 'uses' => 'SteamGamePictureController@delete']);

                # 设置标签页面
                Route::get('tag/{id}', ['as' => 'game.tag', 'uses' => 'SteamGameController@tag']);
                # 设置标签选中保存
                Route::post('tag/{id}', ['as' => 'game.tag', 'uses' => 'SteamGameController@tagForm']);

                # 设置分类页面
                Route::get('category/{id}', ['as' => 'game.category', 'uses' => 'SteamGameController@category']);
                # 设置分类选中保存
                Route::post('category/{id}', ['as' => 'game.category', 'uses' => 'SteamGameController@categoryForm']);


            }
        );


    }
);


/*
|--------------------------------------------------------------------------
| 微信公众账号管理
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'prefix' => 'manage/wechat',
        'namespace' => 'Manage\Wechat',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 微信公众账号列表
                Route::resource('mp/resource', 'WechatMpController');
                # 新微信活动
                Route::resource('mp/game/resource', 'WechatMpGameController');
                Route::resource('mp/game/award/resource', 'WechatMpGameAwardController');
                # 微信红包
                Route::resource('mp/redpack/resource', 'WechatMpRedpackController');
            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.wechat.'
            ],
            function() {

                Route::post('mp/game/{id}/focus', ['as' => 'mp.game.focus', 'uses' => 'WechatMpGameController@focus']); # 将游戏状态置为使用
            }
        );

    }
);


/*
|--------------------------------------------------------------------------
| 游戏管理
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'prefix' => 'manage/game',
        'namespace' => 'Manage\Game',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 游戏列表
                Route::resource('resource', 'GameController');
            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.game.'
            ],
            function() {

                # 微信公众账号上下架
                Route::get('status/{id}', ['as' => 'status', 'uses' => 'MovieController@status']);
                Route::get('resources/{id}/status', ['as' => 'resources.status', 'uses' => 'MovieResourceController@status']);
                Route::get('map/{id}/category', ['as' => 'map.category', 'uses' => 'MovieController@getMap']);
                Route::post('map/{id}/category', ['as' => 'map.category', 'uses' => 'MovieController@setMap']);

            }
        );

    }
);

/*
|--------------------------------------------------------------------------
| 商城管理
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'prefix' => 'manage/mall',
        'namespace' => 'Manage\Mall',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 商品分类列表
                Route::resource('category/resource', 'ProductCategoryController');
                # 商品列表
                Route::resource('product/resource', 'ProductController');
                # 订单列表
                Route::resource('order/resource', 'OrderController');
            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.mall.'
            ],
            function() {

                # 商品上下架
                Route::get('product/{id}/status', ['as' => 'product.status', 'uses' => 'ProductController@status']);

            }
        );

    }
);

/*
|--------------------------------------------------------------------------
| 影视管理
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'prefix' => 'manage/movie',
        'namespace' => 'Manage\Movie',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 影视列表
                Route::resource('resource', 'MovieController');
                # 影视资源列表
                Route::resource('resources/resource', 'MovieResourceController');
                # 影视分类列表
                Route::resource('category/resource', 'MovieCategoryController');
            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.movie.'
            ],
            function() {

                # 商品上下架
                Route::get('status/{id}', ['as' => 'status', 'uses' => 'MovieController@status']);
                Route::get('resources/{id}/status', ['as' => 'resources.status', 'uses' => 'MovieResourceController@status']);
                Route::get('map/{id}/category', ['as' => 'map.category', 'uses' => 'MovieController@getMap']);
                Route::post('map/{id}/category', ['as' => 'map.category', 'uses' => 'MovieController@setMap']);

            }
        );

    }
);

/*
|--------------------------------------------------------------------------
| 广告管理
|--------------------------------------------------------------------------
*/

Route::group(
    [
        'prefix' => 'manage/ad',
        'namespace' => 'Manage\AD',
        'middleware' => 'auth.manage'
    ],
    function() {

        # REST-ful 资源接口

        Route::group(
            [],
            function() {

                # 广告管理
                Route::resource('resource', 'AdController');

                # 广告位管理
                Route::resource('category/resource', 'AdCategoryController');

            }
        );

        # 功能扩展接口

        Route::group(
            [
                'as' => 'manage.ad.'
            ],
            function() {

            }
        );

    }
);


