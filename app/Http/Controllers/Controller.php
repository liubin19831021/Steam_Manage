<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Log;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     *
     *  根据路由别名获取API接口URL
     *
     * @param $name
     * @param $param
     * @return mixed
     */
    protected function getApiRoute($name, $param = null)
    {
        return app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.' . $name, $param);
    }

    protected function log($msg, $type, $path = null, $day = 30)
    {
        $file = ($path != null) ? $path : $type;
        Log::useDailyFiles(storage_path() . '/logs/' . $file . '.log', $day);
        Log::write($type, $msg);
    }
}