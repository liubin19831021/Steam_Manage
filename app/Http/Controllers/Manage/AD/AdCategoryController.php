<?php
namespace App\Http\Controllers\Manage\AD;

use App\Http\Controllers\Manage\CommonController;
use App\Model\AD\Ad;
use App\Model\AD\AdCategory;

class AdCategoryController extends CommonController
{
    /**
     *
     * 广告位列表页
     * 
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'name' => '名称',
            'total' => '数量限制',
        ];
        $viewData = [];
        $viewData['title'] = '广告位管理';
        $viewData['nav'] = $this->navTab('ad', 'category');
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加广告位',
                'route' => route('manage.ad.category.resource.create')
            ]
        ];
        $page = $this->request->input('page', 1);
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title' => '修改',
                'route' => 'manage.ad.category.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
//            'trash' => [
//                'title' => '删除',
//                'route' => 'manage.ad.category.resource.destroy',
//            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
//            'trash' => [
//                'method' => 'DELETE',
//                'title' => '删除记录',
//                'msg' => '确定要删除广告位吗'
//            ]
        ];
        #列表数据
        $adCategory = new AdCategory;
        $viewData['list'] = $this->constraintSelect($adCategory, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除广告位
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $count  = Ad::where('category_id', $id)->count();
        if ($count > 0) {
            return redirect()->back()->with('error', '此广告位有广告内容不能删除!');
        }
        $result = AdCategory::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '广告位删除成功!');
        }
        return redirect()->back()->with('error', '广告位删除失败!');
    }

    /**
     * 添加广告位页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'name' => [
                'name' => '名称',
                'type' => 'text',
                'col' => '12'
            ],
            'total' => [
                'name' => '最大数量',
                'type' => 'number',
                'col' => '12'
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加商城广告位';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.ad.category.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回广告位列表',
                'type' => 'button',
                'route' => route('manage.ad.category.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建品牌操作
     * @return mixed
     */
    public function store()
    {
        # 插入新数据
        $adCategory = new AdCategory;
        $adCategory->name = $this->request->input('name');
        $total = $this->request->input('total');
        if($total < 1){
            return redirect()->back()->with('error', '广告位限制数量最少为1');
        }
        $adCategory->total = $total;
        if ($adCategory->save()) {
            return redirect()->route('manage.ad.category.resource.index')->with('success', '广告位添加成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '广告位添加失败!');
    }

    /**
     * 修改品牌信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = AdCategory::find($id);
        # 表单元素
        $param = [
            'name' => [
                'name' => '名称',
                'type' => 'text',
                'col' => '12',
                'value' => $data->name
            ],
            'total' => [
                'name' => '最大数量',
                'type' => 'number',
                'col' => '12',
                'value' => $data->total
            ]
        ];
        $viewData = [];
        $viewData['title'] = '修改广告';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.ad.category.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回广告位列表',
                'type' => 'button',
                'route' => route('manage.ad.category.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 修改品牌信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        # 插入新数据
        $adCategory = AdCategory::find($id);
        $adCategory->name = $this->request->input('name');
        $total = $this->request->input('total');
        if($total < 1){
            return redirect()->back()->with('error', '广告位限制数量最少为1');
        }
        $adCount = Ad::where('category_id', $id)->count();
        if($adCount > $total){
            return redirect()->back()->with('error', '当前位置的广告已经多于' . $total . '个，广告位修改失败!');
        }
        $adCategory->total = $total;
        if (!$adCategory->save()) {
            return redirect()->back()->with('error', '广告位修改失败!');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.ad.category.resource.index', $param)->with('success', '广告位修改成功!'); # 更新成功后跳转到列表页
    }
}