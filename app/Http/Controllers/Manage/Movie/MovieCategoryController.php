<?php
namespace App\Http\Controllers\Manage\Movie;

use App\Http\Controllers\Manage\CommonController;;
use App\Model\Movie\MovieCategory;
use App\Model\Movie\MovieMapCategory;
use Illuminate\Support\Facades\DB;

class MovieCategoryController extends CommonController
{
    /**
     * 查看分类
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'name' => '分类名称',
            'parent_id' => '父级分类编号',
            'level' => '分类层级'
        ];
        $range = [];
        $viewData = [];
        $viewData['title'] = '分类列表';
        # 查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = $range;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加分类',
                'route' => route('manage.movie.category.resource.create')
            ]
        ];
        # 行操作项的路由
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.movie.category.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.movie.category.resource.destroy'
            ]
        ];
        # 模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'title' => '删除记录',
                'method' => 'DELETE',
                'msg' => '确定要删除分类吗'
            ]
        ];
        # 查询记录
        $movieCategory = new MovieCategory();
        $viewData['list'] = $this->constraintSelect($movieCategory, $field, $range);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除分类
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $count = MovieMapCategory::where('category_id', $id)->count();
        if ($count > 0) {
            return redirect()->back()->with('error', '该分类下有影视，不能删除！');
        }
        if (!MovieCategory::destroy($id)) {
            return redirect()->back()->with('success', '分类删除失败!');
        }
        return redirect()->back()->with('success', '分类删除成功!');
    }

    /**
     * 创建分类页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'name' => [
                'name' => '分类名称',
                'type' => 'text',
                'col' => '4'
            ]
        ];

        $viewData = [];
        $viewData['title'] = '添加分类';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.movie.category.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.movie.category.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);

    }

    /**
     * 创建分类操作
     * @return mixed
     */
    public function store()
    {
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写分类名称!');
        }
        # 插入新数据
        $movieCategory = new MovieCategory();
        $movieCategory->name = $this->request->input('name');
        $movieCategory->parent_id = 0;
        $movieCategory->level = 1;
        $movieCategory->create_user = $this->user['id'];
        if ($movieCategory->save()) {
            return redirect()->route('manage.movie.category.resource.index', ['id' => $movieCategory->id])->with('success', '分类创建成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '分类创建失败!');
    }

    /**
     * 修改分类信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = MovieCategory::find($id);
        # 表单元素
        $param = [
            'name' => [
                'name' => '分类名称',
                'type' => 'text',
                'col' => '4',
                'value' => $data->name,
            ]
        ];

        $viewData = [];
        $viewData['title'] = '查看分类';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.movie.category.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.movie.category.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改分类信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写分类名称!');
        }
        # 更新数据
        $movieCategory = MovieCategory::find($id);
        $movieCategory->name = $this->request->input('name');
        if (!$movieCategory->save()) {
            return redirect()->back()->with('error', '分类信息修改失败');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.movie.category.resource.index', $param)->with('success', '分类信息修改成功!'); # 更新成功后跳转到列表页
    }

}