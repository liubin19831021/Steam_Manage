<?php
namespace App\Http\Controllers\Manage\Movie;

use App\Http\Controllers\Manage\CommonController;
use App\Http\Requests\MoviePostRequest;
use App\Model\Movie\MovieCategory;
use App\Model\Movie\MovieResource;
use Illuminate\Support\Facades\DB;

class MovieResourceController extends CommonController
{
    /**
     * 查看影视资源
     * @return mixed
     */
    public function index()
    {
        $movieId = $this->request->input('movie_id');
        $field = [
            'id' => 'ID',
            'movie_id' => '影视ID',
            'title' => '资源名称',
            'type' => '资源类型',
            'rank' => '权重',
            'click' => '点击率',
            'price' => '收费价格',
            'is_new' => '是否最新',
            'is_hot' => '是否最热',
            'create_time' => '创建时间',
            'status' => '状态'
        ];
        $range = [
            'status' => [ 0 =>'下架', 1 => '上架'],
            'is_new' => YN(),
            'is_hot' => YN(),
            'type' => resourcesType()
        ];
        $viewData = [];
        $viewData['title'] = '影视资源管理';
        # 查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = $range;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加影视资源',
                'route' => route('manage.movie.resources.resource.create', ['movie_id' => $movieId])
            ]
        ];
        # 行操作项的路由
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.movie.resources.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],

            'eye' => [
                'title' => '上架',
                'route' => 'manage.movie.resources.status',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '0']
                ]
            ],
            'eye-slash' => [
                'title' => '下架',
                'route' => 'manage.movie.resources.status',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '1']
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.movie.resources.resource.destroy'
            ]
        ];
        # 模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'title' => '删除记录',
                'method' => 'DELETE',
                'msg' => '确定要删除开奖活动吗'
            ]
        ];
        $viewData['nav'] = $this->navTab('movie', 'resources', ['id' => $movieId]);
        # 查询记录
        $resources = MovieResource::where('movie_id', $movieId)->where('is_delete', 0)->orderBy('rank');
        $viewData['list'] = $this->constraintSelect($resources, $field, $range);

        $viewData['form_button'] = [
            'list' => [
                'name' => '返回影视列表',
                'type' => 'button',
                'route' => route('manage.movie.resource.index')
            ]
        ];
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除影视
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $movieResource = MovieResource::find($id);
        $movieResource->is_delete = 1;
        $movieResource->update_user = $this->user['id'];
        $this->log(json_encode('manage:' . $this->user['id'] . ' remove movie resources:' . $id . ' time:' . currentTime()), 'info', 'product_destroy');
        if ($movieResource->save()) {
            return redirect()->back()->with('success', '影视资源删除成功!');
        }
        return redirect()->back()->with('error', '影视资源删除失败!');
    }

    /**
     * 创建影视资源页面
     * @return mixed
     */
    public function create()
    {
        $movieId = $this->request->input('movie_id');
        $maxRank = MovieResource::where('movie_id', $movieId)->max('rank');
        $rank = ($maxRank) ? $maxRank + 1 : 1;
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '资源名称',
            'type' => 'text',
            'col' => '9'
        ];
        $param['type'] = [
            'name' => '影视资源类型',
            'type' => 'select',
            'col' => '3',
            'range' => resourcesType()
        ];
        $param['price'] = [
            'name' => '收费价格',
            'type' => 'number',
            'col' => '3'
        ];
        $param['rank'] = [
            'name' => '权重',
            'type' => 'number',
            'col' => '3',
            'value' => $rank
        ];
        $param['is_new'] = [
            'name' => '是否最新',
            'type' => 'select',
            'range' => YN(),
            'col' => '3'
        ];
        $param['is_hot'] = [
            'name' => '是否最热',
            'type' => 'select',
            'range' => YN(),
            'col' => '3'
        ];
        $param['path'] = [
            'name' => '资源地址',
            'type' => 'text',
            'col' => '12'
        ];
        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
            if($param['rank']['value'] == null){
                $param['rank']['value'] = $rank;
            }
        }
        $viewData = [];
        $viewData['title'] = '添加影视资源';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.movie.resources.resource.store', ['movie_id' => $movieId]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回影视资源列表',
                'type' => 'button',
                'route' => route('manage.movie.resources.resource.index', ['movie_id' => $movieId])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建影视页面
     *
     * @param MoviePostRequest $request
     * @return mixed
     */
    public function store(MoviePostRequest $request)
    {
        $this->request->flash();
        $movieId = $this->request->input('movie_id');
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写影视名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写资源收费价格');
        }
        if (!$this->request->has('path')) {
            return redirect()->back()->with('error', '请填写影视资源地址');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写权重');
        }
        $rank = $this->request->input('rank');
        $hasId = MovieResource::where('movie_id', $movieId)->where('rank', $rank)->count();
        if($hasId > 0) {
            return redirect()->back()->with('error', '权重已存在，请重新填写');
        }
        # 插入新数据
        $movieResource = new MovieResource();
        $movieResource->title = $this->request->input('title');
        $movieResource->price = $this->request->input('price');
        $movieResource->rank = $rank;
        $movieResource->movie_id = $movieId;
        $movieResource->is_hot = $this->request->input('is_hot');
        $movieResource->is_new = $this->request->input('is_new');
        $movieResource->type = $this->request->input('type');
        $movieResource->path = $this->request->input('path');;
        $movieResource->status = 1;
        $movieResource->create_user = $this->user['id'];
        if ($movieResource->save()) {
            # 创建成功后跳转到影视详细介绍编辑页
            return redirect()->route('manage.movie.resources.resource.index', ['movie_id' => $movieId])->with('success', '影视资源添加成功!');
        }
        return redirect()->back()->with('error', '影视资源添加失败!');
    }
    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $movieResource = MovieResource::find($id);
        if(!$movieResource) {
            return redirect()->back()->with('error', '未找到资源信息!');
        }
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '资源名称',
            'type' => 'text',
            'col' => '9',
            'value' => $movieResource->title
        ];
        $param['type'] = [
            'name' => '影视资源类型',
            'type' => 'select',
            'col' => '3',
            'range' => resourcesType(),
            'value' => $movieResource->type
        ];
        $param['price'] = [
            'name' => '收费价格',
            'type' => 'number',
            'col' => '3',
            'value' => $movieResource->price
        ];
        $param['rank'] = [
            'name' => '权重',
            'type' => 'number',
            'col' => '3',
            'value' => $movieResource->rank
        ];
        $param['is_new'] = [
            'name' => '是否最新',
            'type' => 'select',
            'range' => YN(),
            'col' => '3',
            'value' => $movieResource->is_new
        ];
        $param['is_hot'] = [
            'name' => '是否最热',
            'type' => 'select',
            'range' => YN(),
            'col' => '3',
            'value' => $movieResource->is_hot
        ];
        $param['path'] = [
            'name' => '资源地址',
            'type' => 'text',
            'col' => '12',
            'value' => $movieResource->path
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改影视资源信息';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.movie.resources.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回影视资源列表',
                'type' => 'button',
                'route' => route('manage.movie.resources.resource.index', ['movie_id' => $movieResource->movie_id])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改管理员信息操作
     *
     * @param MoviePostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(MoviePostRequest $request, $id)
    {
        $this->request->flash();
        $movieResource = MovieResource::find($id);
        if(!$movieResource) {
            return redirect()->back()->with('error', '未找到资源信息!');
        }
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写影视名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写资源收费价格');
        }
        if (!$this->request->has('path')) {
            return redirect()->back()->with('error', '请填写影视资源地址');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写权重');
        }
        $rank = $this->request->input('rank');
        if($rank != $movieResource->rank) {
            $hasId = MovieResource::where('movie_id', $movieResource->movie_id)->where('rank', $rank)->count();
            if ($hasId > 0) {
                return redirect()->back()->with('error', '权重已存在，请重新填写');
            }
            $movieResource->rank = $rank;
        }
        # 插入新数据
        $movieResource->title = $this->request->input('title');
        $movieResource->price = $this->request->input('price');
        $movieResource->is_hot = $this->request->input('is_hot');
        $movieResource->is_new = $this->request->input('is_new');
        $movieResource->type = $this->request->input('type');
        $movieResource->path = $this->request->input('path');
        $movieResource->update_user = $this->user['id'];
        if (!$movieResource->save()) {
            return redirect()->back()->with('error', '影视资源修改失败!');
        }
        # 创建成功后跳转到影视详细介绍编辑页
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page,
            'movie_id' => $movieResource->movie_id
        ];
        return redirect()->route('manage.movie.resources.resource.index', $param)->with('success', '影视资源修改成功!');
    }

    /**
     *
     * 影视资源上架下架
     *
     * @param $id
     * @return mixed
     */
    public function status($id)
    {
        $movieResource = MovieResource::find($id);
        if(!$movieResource) {
            return redirect()->back()->with('error', '影视资源信息异常！');
        }
        $status = ($movieResource->status == '1') ? '0' : '1';
        $msg = ($movieResource->status == '1') ? '下架' : '上架';
        $movieResource->status = $status;
        if(!$movieResource->save()) {
            return redirect()->back()->with('error', '影视资源' . $msg . '失败！');
        }
        return redirect()->route('manage.movie.resources.resource.index', ['movie_id' => $movieResource->movie_id])->with('success', '影视资源' . $msg . '成功！');
    }

}