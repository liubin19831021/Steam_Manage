<?php
namespace App\Http\Controllers\Manage\Mall;

use App\Http\Controllers\Manage\CommonController;
use App\Http\Requests\OrderPostRequest;
use App\Model\Mall\Order;

class OrderController extends CommonController
{
    /**
     *
     * 查看订单
     *
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'member_id' => '用户ID',
            'product_id' => '商品ID',
            'status' => '订单状态',
            'create_time' => '订单生成时间'
        ];
        $viewData = [];
        $viewData['title'] = '订单管理';
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'status' => orderStatus()
        ];
        # 列表操作菜单
        $viewData['option_title'] = [];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.lottery.order.resource.edit'
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.lottery.order.resource.destroy',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '0']
                ],
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除商品',
                'msg' => '确定要删除订单吗？'
            ],
        ];
        # 列表数据
        $lotteryOrder = Order::where('deleted_at', 0);
        $viewData['list'] = $this->constraintSelect($lotteryOrder, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     *
     * 删除订单
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $lotteryOrder = Order::where('deleted_at', 0)->find($id);
        if(!$lotteryOrder){
            return redirect()->back()->with('success', '订单异常!');
        }
        if(!$lotteryOrder->status > 0){
            return redirect()->back()->with('success', '已付款订单不可删除!');
        }
        # 执行订单删除
        $result = Order::orderDestroy($id, $this->user['id'], '删除订单返还余额，订单号：');
        if ($result) {
            return redirect()->back()->with('success', '订单删除成功!');
        }
        return redirect()->back()->with('error', '订单删除失败!');
    }

    /**
     *
     * 修改订单信息页面
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = Order::find($id);
        # 表单元素
        $param = [
            'pay_type' => [
                'type' => 'select',
                'col' => '4',
                'name' => '用户选择的支付方式',
                'value' => $data->pay_type,
                'range' => ['0'=>'未知', '1'=>'支付宝', '2'=>'微信', '3'=>'银联在线']
            ],
            'rebate_user' => [
                'type' => 'number',
                'col' => '4',
                'name' => '接受分成的用户ID',
                'value' => $data->rebate_user
            ],
            'rebate_money' => [
                'type' => 'text',
                'col' => '4',
                'name' => '分成金额',
                'value' => $data->rebate_money,
            ],
            'rebate_status' => [
                'type' => 'select',
                'col' => '4',
                'name' => '分成状态',
                'value' => $data->rebate_status,
                'range' => ['0'=>'未分成', '1'=>'未提现', '2'=>'已提现']
            ],
            'from' => [
                'type' => 'select',
                'col' => '4',
                'name' => '订单来源（WEB或APP）',
                'value' => $data->order_from,
                'range' => ['0'=>'WEB', '1'=>'APP']
            ],
        ];
        $viewData = [];
        $viewData['title'] = '修改订单';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.lottery.order.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回订单列表',
                'type' => 'button',
                'route' => route('manage.lottery.order.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改订单信息操作
     *
     * @param OrderPostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(OrderPostRequest $request,$id)
    {
        # 查询是否存在订单，如不存在提示未找到当前订单
        $lotteryOrder = Order::find($id);
        if (!$lotteryOrder) {
            return redirect()->back()->with('error', '未找到当前订单');
        }
        $this->request->flash();
        # 更新数据
        $lotteryOrder->pay_type = $this->request->input('pay_type');
        $lotteryOrder->rebate_user = $this->request->input('rebate_user');
        $lotteryOrder->rebate_money = $this->request->input('rebate_money');
        $lotteryOrder->rebate_status = $this->request->input('rebate_status');
        $lotteryOrder->from = $this->request->input('from');
        $lotteryOrder->update_user = $this->user['id'];
        if($lotteryOrder->save()){
            return redirect()->route('manage.lottery.order.resource.index')->with('success', '订单信息修改成功!');
        }
        return redirect()->back()->with('error', '订单信息修改失败!');
    }
}
