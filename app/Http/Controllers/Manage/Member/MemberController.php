<?php
namespace App\Http\Controllers\Manage\Member;

use App\Http\Controllers\Manage\CommonController;

use App\Model\Member\Member;
use Hash;

class MemberController extends CommonController
{
    /**
     * 查看会员信息
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'mobile' => '手机',
            'username' => '账号',
            'create_time' => '注册时间',
            'sex' => '性别',
            'ip' => '上次登录IP',
            'status' => '状态',
        ];
        $viewData = [];
        $viewData['title'] = '会员管理';
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'status' => [1 => '正常', 0 => '禁用'],
            'sex' => [1 => '男', 2 => '女']
        ];
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加会员',
                'route' => route('manage.member.resource.create')
            ]
        ];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.member.resource.edit'
            ],
            'disable' => [
                'title' => '禁用',
                'route' => 'manage.member.disable',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '1']
                ]
            ],
            'enable' => [
                'title' => '启用',
                'route' => 'manage.member.disable',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '0'],
                    ['name' => 'username', 'condition' => '!=', 'value' => '']
                ]
            ],
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'disable' => [
                'title' => '禁用',
                'method' => 'POST',
                'msg' => '确定要禁用该用户吗'
            ],
            'enable' => [
                'title' => '启用',
                'method' => 'POST',
                'msg' => '确定要启用该用户吗'
            ]
        ];
        # 列表数据
        $member = new Member;
        $viewData['list'] = $this->constraintSelect($member, $field);
        # 渲染视图
        return view('manage.table', $viewData);

    }

    /**
     * 创建会员页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'username' => [
                'type' => 'text',
                'col' => '4',
                'name' => '账号名',
            ],
            'password' => [
                'type' => 'password',
                'col' => '4',
                'name' => '密码',
            ],
            'password_confirm' => [
                'type' => 'password',
                'col' => '4',
                'name' => '确认密码',
            ],
            'mobile' => [
                'type' => 'text',
                'col' => '4',
                'name' => '手机号',
            ],
            'nickname' => [
                'type' => 'text',
                'col' => '4',
                'name' => '昵称',
            ],
            'sex' => [
                'type' => 'select',
                'col' => '4',
                'name' => '性别',
                'range' => sex(),
            ],
            'avatar' =>  [
                'type' => 'thumb',
                'col' => '4',
                'name' => '会员头像（建议256×256像素）',
                'accept' => 'image',
                'ratio' => '1',
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加会员';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.member.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.member.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 添加会员操作
     *
     * @return mixed
     */
    public function store()
    {
        $username = $this->request->input('username');
        if (empty($username)) {
            return redirect()->back()->with('warning', '请输入会员名！');
        }
        $password = $this->request->input('password');
        if (empty($password)) {
            return redirect()->back()->with('warning', '请输入密码！');
        }
        $passwordConfirm = $this->request->input('password_confirm');
        if (empty($passwordConfirm)) {
            return redirect()->back()->with('warning', '请确认密码！');
        }
        if ($password != $passwordConfirm) {
            return redirect()->back()->with('warning', '两次输入密码不一致！');
        }
        $mobile = $this->request->input('mobile');
        if (empty($mobile)) {
            return redirect()->back()->with('warning', '请输联系电话！');
        }
        $count = Member::where('username', $username)->count();
        if ($count > 0) {
            return redirect()->back()->with('error', '该用户名已存在！');
        }
        $count = Member::where('mobile', $mobile)->count();
        if ($count > 0) {
            return redirect()->back()->with('error', '该手机号已存在！');
        }
        $member = new Member;
        $member->username = $username;
        $member->mobile = $mobile;
        $member->nickname = $this->request->input('nickname');
        $member->password = Hash::make($password);
        $member->sex = $this->request->input('sex', 1);
        $member->ip = $this->request->ip();
        $member->create_user = $this->user['id'];
        # 修改商品预览图
        $avatar = null;
        if ($this->request->has('avatar')) {
            $avatar = $this->request->input('avatar');
            $avatar = $this->uploadBase64Image($avatar, 'user_avatar');
            if (!$avatar) {
                return redirect()->back()->with('error', '头像文件上传失败!');
            }
        }
        if ($avatar) $member->avatar = $avatar;
        if ($member->save()) {
            return redirect()->route('manage.member.resource.index')->with('success', '会员创建成功!');
        }
        return redirect()->back()->with('error', '会员创建失败');
    }

    /**
     * 修改会员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = Member::find($id);
        # 表单元素
        $param = [
            'username' => [
                'type' => 'text',
                'col' => '4',
                'name' => '账号名',
                'value' => $data->username,
            ],
            'password' => [
                'type' => 'password',
                'col' => '4',
                'name' => '密码',
                'value' => null,
            ],
            'password_confirm' => [
                'type' => 'password',
                'col' => '4',
                'name' => '确认密码',
                'value' => null,
            ],
            'mobile' => [
                'type' => 'text',
                'col' => '4',
                'name' => '手机号',
                'value' => $data->mobile,
            ],
            'nickname' => [
                'type' => 'text',
                'col' => '4',
                'name' => '昵称',
                'value' => $data->nickname,
            ],
            'sex' => [
                'type' => 'select',
                'col' => '4',
                'name' => '性别',
                'range' => sex(),
                'value' => $data->sex,
            ],
            'avatar' =>  [
                'type' => 'thumb',
                'col' => '4',
                'name' => '会员头像（建议256×256像素）',
                'accept' => 'image',
                'value' => $this->domain . $this->path['user_avatar'] . $data->avatar,
                'ratio' => '1',
            ]
        ];
        $viewData = [];
        $viewData['title'] = '修改会员';
        $viewData['nav'] = $this->navTab('member', 'main', ['id' => $id]);
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.member.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回会员列表',
                'type' => 'button',
                'route' => route('manage.member.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改会员信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $username = $this->request->input('username');
        if (empty($username)) {
            return redirect()->back()->with('warning', '请输入会员名！');
        }
        $password = $this->request->input('password');
        if (empty($password)) {
            return redirect()->back()->with('warning', '请输入密码！');
        }
        $passwordConfirm = $this->request->input('password_confirm');
        if (empty($passwordConfirm)) {
            return redirect()->back()->with('warning', '请确认密码！');
        }
        if ($password != $passwordConfirm) {
            return redirect()->back()->with('warning', '两次输入密码不一致！');
        }
        $mobile = $this->request->input('mobile');
        if (empty($mobile)) {
            return redirect()->back()->with('warning', '请输联系电话！');
        }
        $member = Member::find($id);
        if ($username != $member->username) {
            $count = Member::where('username', $username)->count();
            if ($count > 0) {
                return redirect()->back()->with('error', '该用户名已存在！');
            }
        }
        if ($mobile != $member->mobile) {
            $count = Member::where('mobile', $mobile)->count();
            if ($count > 0) {
                return redirect()->back()->with('error', '该手机号已存在！');
            }
        }
        $member->username = $username;
        $member->password = Hash::make($password);
        $member->mobile = $mobile;
        $member->nickname = $this->request->input('nickname');
        $member->sex = $this->request->input('sex', 1);
        $member->update_user = $this->user['id'];
        # 修改商品预览图
        $avatar = null;
        if ($this->request->has('avatar')) {
            $avatar = $this->request->input('avatar');
            $avatar = $this->uploadBase64Image($avatar, 'user_avatar');
            if (!$avatar) {
                return redirect()->back()->with('error', '头像文件上传失败!');
            }
        }
        if ($avatar) $member->avatar = $avatar;
        if ($member->save()) {
            return redirect()->route('manage.member.resource.index')->with('success', '会员修改成功!');
        }
        return redirect()->back()->with('error', '会员修改失败');
    }

    /**
     * 会员启用禁用设置
     * @param $id
     * @return mixed
     */
    public function disable($id)
    {
        $member = Member::find($id);
        $type = ($member->status == 0) ? '启用' : '禁用';
        $member->status = ($member->status == 0) ? 1 : 0;
        if ($member->save()) {
            return redirect()->back()->with('success', $type . '成功!');
        }
        return redirect()->back()->with('error', $type . '失败!');
    }
}