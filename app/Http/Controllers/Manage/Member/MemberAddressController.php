<?php
namespace App\Http\Controllers\Manage\Member;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Member\MemberAddress;
use Mockery\CountValidator\Exception;

class MemberAddressController extends CommonController
{
    /**
     *
     * 查看会员地址信息
     *
     * @param $memberID
     * @return mixed
     */
    public function index()
    {
        $memberID = $this->request->input('member');
        $field = [
            'id' => 'ID',
            'member_id' => '会员ID',
            'name' => '收货人',
            'address' => '详细地址',
            'phone' => '联系方式',
            'is_default' => '是否为默认'
        ];
        $viewData = [];
        $viewData['title'] = '地址管理';
        $viewData['range'] = [
            'is_default' => ['0' => '否', '1' => '是']
        ];
        $viewData['nav'] = $this->navTab('member', 'address', ['id' => $memberID]);
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加地址',
                'route' => route('manage.member.adds.resource.create', ['member' => $memberID])
            ]
        ];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.member.adds.resource.edit',
                'param' => ['member' => $memberID],
            ],
            'set_default' => [
                'title' => '设为默认',
                'route' => 'manage.member.adds.default',
                'param' => ['member' => $memberID],
                'where' => [
                    ['name' => 'is_default', 'condition' => '=', 'value' => '0']
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.member.adds.resource.destroy',
                'param' => ['member' => $memberID],
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'set_default' => [
                'method' => 'POST',
                'title' => '设为默认',
                'msg' => '确认设为默认地址吗？'
            ],
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除记录',
                'msg' => '确定要删除该条记录吗？'
            ]
        ];
        #列表数据
        $memberAddress = MemberAddress::where('member_id', $memberID);
        $viewData['list'] = $this->constraintSelect($memberAddress, $field);
        return view('manage.table', $viewData);
    }

    /**
     *
     * 创建会员地址页面
     *
     * @param $memberID
     * @return mixed
     */
    public function create()
    {
        $memberID = $this->request->input('member');
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '收货人',
            ],
            'phone' => [
                'type' => 'text',
                'col' => '4',
                'name' => '联系电话',
                'hascode' => 1,
            ],
            'zipcode' => [
                'type' => 'text',
                'col' => '4',
                'name' => '邮编',
            ],
            'region' => [
                'type' => 'distpicker',
                'col' => '4',
                'name' => '区域'
            ],
            'address' => [
                'type' => 'text',
                'col' => '4',
                'name' => '详细地址'
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加地址';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.member.adds.resource.store',['member' => $memberID]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回地址列表',
                'type' => 'button',
                'route' => route('manage.member.adds.resource.index', ['member' => $memberID])
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建会员地址操作
     *
     * @param $memberID
     * @return mixed
     */
    public function store()
    {
        $memberID = $this->request->input('member');
        if (!$this->request->has('name')) {
            return redirect()->back()->with('warning', '请输入收货人姓名！');
        }
        if (!$this->request->has('phone')) {
            return redirect()->back()->with('warning', '请输入联系电话！');
        }
        if (!$this->request->has('zipcode')) {
            return redirect()->back()->with('warning', '请输邮编！');
        }
        $region = $this->request->input('region');
        $memberAddress = new MemberAddress;
        $memberAddress->member_id = $memberID;
        $memberAddress->name = $this->request->input('name');
        $memberAddress->province = $region['province'];
        $memberAddress->city =  $region['city'];
        $memberAddress->district = $region['district'];
        $memberAddress->address = $this->request->input('address');
        $memberAddress->zipcode = $this->request->input('zipcode');
        $memberAddress->phone = $this->request->input('phone');
        $memberAddress->create_time = currentTime();
        if ($memberAddress->save()) {
            return redirect()->route('manage.member.adds.resource.index', ['member' => $memberID])->with('success', '会员地址添加成功!');
        }
        return redirect()->back()->with('error', '会员地址添加失败!');
    }

    /**
     *
     * 删除会员地址
     *
     * @param $memberID
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = MemberAddress::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '会员地址删除成功!');
        }
        return redirect()->back()->with('error', '删除失败!');
    }

    /**
     *
     * 修改会员地址信息页面
     *
     * @param $memberID
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = MemberAddress::find($id);
        $memberID = $data->member_id;
        $region = implode(',', [$data->province, $data->city, $data->district]);
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '收货人',
                'value' => $data->name,
            ],
            'phone' => [
                'type' => 'text',
                'col' => '4',
                'name' => '联系电话',
                'value' => $data->phone,
            ],
            'zipcode' => [
                'type' => 'text',
                'col' => '4',
                'name' => '邮编',
                'value' => $data->zipcode,
            ],
            'region' => [
                'type' => 'distpicker',
                'col' => '4',
                'name' => '地址',
                'value' => $region
            ],
            'address' => [
                'type' => 'text',
                'col' => '4',
                'name' => '详细信息',
                'value' => $data->address,
            ]
        ];
        $viewData = [];
        $viewData['title'] = '修改地址';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.member.adds.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回地址列表',
                'type' => 'button',
                'route' => route('manage.member.adds.resource.index', ['member' => $memberID])
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改会员信息操作
     *
     * @param $memberID
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $memberAddress = MemberAddress::find($id);
        $memberID = $memberAddress->member_id;
        if (!$memberAddress) {
            return redirect()->back()->with('error', '地址记录不存在！');
        }
        if (!$this->request->has('name')) {
            return redirect()->back()->with('warning', '请输入收货人姓名！');
        }
        if (!$this->request->has('phone')) {
            return redirect()->back()->with('warning', '请输入联系电话！');
        }
        if (!$this->request->has('zipcode')) {
            return redirect()->back()->with('warning', '请输邮编！');
        }
        $region = $this->request->input('region');
        $memberAddress->name = $this->request->input('name');
        $memberAddress->province = $region['province'];
        $memberAddress->city =  $region['city'];
        $memberAddress->district = $region['district'];
        $memberAddress->address = $this->request->input('address');
        $memberAddress->zipcode = $this->request->input('zipcode');
        $memberAddress->phone = $this->request->input('phone');
        if ($memberAddress->save()) {
            return redirect()->route('manage.member.adds.resource.index', ['member' => $memberID])->with('success', '会员地址修改成功!');
        }
        return redirect()->back()->with('error', '会员地址修改失败!');
    }

    /**
     *
     * 设为默认地址
     *
     * @param $memberID
     * @param $id
     * @return mixed
     */
    public function setDefault($id)
    {
        $memberAddress = MemberAddress::find($id);
        $memberID = $memberAddress->member_id;
        $result =  MemberAddress::focus($memberID, $id);
        if($result) {
            return redirect()->route('manage.member.adds.resource.index', ['member' => $memberID])->with('success', '默认地址设置成功!');
        }
        return redirect()->back()->with('error', '默认地址设置失败!');
        
    }
}