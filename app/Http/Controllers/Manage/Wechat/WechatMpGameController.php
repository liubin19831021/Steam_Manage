<?php
namespace App\Http\Controllers\Manage\Wechat;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Game\Game;
use App\Model\Wechat\WechatMpGame;
use Illuminate\Support\Facades\DB;
use Form;

class WechatMpGameController extends CommonController
{
    /**
     * 查看活动
     * @return mixed
     */
    public function index()
    {
        $wechatMpID = $this->request->input('wechat_mp_id');
        $field = [
            'id' => '活动编号',
            'title' => '活动名称',
            'game_id' => '游戏',
            'use_amount' => '游戏总使用次数',
            'day_use_amount' => '每日游戏使用次数',
            'status' => '是否使用',
            'create_time' => '发布时间'
        ];
        $viewData = [];
        $viewData['title'] = '活动管理';
        $range = [
            'status' => YN(),
            'game_id' => Game::keyName()
        ];
        $viewData['range'] = $range;
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新游戏',
                'route' => route('manage.wechat.mp.game.resource.create', ['wechat_mp_id' => $wechatMpID])
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.wechat.mp.game.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'focus' => [
                'title' => '使用',
                'route' => 'manage.wechat.mp.game.focus',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '0']
                ]
            ],
            'noFocus' => [
                'title' => '取消使用',
                'route' => 'manage.wechat.mp.game.focus',
                'where' => [
                    ['name' => 'status', 'condition' => '=', 'value' => '1']
                ]
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'focus' => [
                'method' => 'POST',
                'title' => '使用游戏',
                'msg' => '确定要为该公众号使用该游戏'
            ],
            'noFocus' => [
                'method' => 'POST',
                'title' => '使用游戏',
                'msg' => '确定要为取消使用该游戏'
            ]
        ];
        $viewData['nav'] = $this->navTab('wechat_mp', 'activity', ['id' => $wechatMpID]);
        # 查询记录
        $wechatMpGame = WechatMpGame::where('wechat_mp_id', $wechatMpID);
        $urlParm = 'wechat_mp_id=' . $wechatMpID;
        $viewData['list'] = $this->constraintSelect($wechatMpGame, $field, $range, $urlParm);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 创建活动页面
     * @return mixed
     */
    public function create()
    {
        $wechatMpID = $this->request->input('wechat_mp_id');
        $gameList =  Game::keyName();
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '活动标题',
            'type' => 'text',
            'col' => '12'
        ];
        $param['subtitle'] = [
            'name' => '小标题',
            'type' => 'text',
            'col' => '12'
        ];
        $param['share_amount'] = [
            'name' => '分享奖励数值',
            'type' => 'number',
            'col' => '3'
        ];
        $param['share_use_amount'] = [
            'name' => '分享奖励游戏使用次数',
            'type' => 'number',
            'col' => '3'
        ];
        $param['win_odds'] = [
            'name' => '获奖概率',
            'type' => 'number',
            'col' => '3'
        ];
        $param['win_where'] = [
            'name' => '获奖条件',
            'type' => 'select',
            'range' => winWhereType(),
            'col' => '3'
        ];
        $param['win_value'] = [
            'name' => '获奖数值',
            'type' => 'number',
            'col' => '3'
        ];
        $param['use_amount'] = [
            'name' => '游戏总使用次数',
            'type' => 'number',
            'col' => '3'
        ];
        $param['day_use_amount'] = [
            'name' => '每日游戏使用次数',
            'type' => 'number',
            'col' => '3'
        ];
        $param['share_amount'] = [
            'name' => '分享奖励数值',
            'type' => 'number',
            'col' => '3'
        ];
        $param['share_use_amount'] = [
            'name' => '分享奖励游戏使用次数',
            'type' => 'number',
            'col' => '3'
        ];
        $param['content'] = [
            'name' => '活动说明',
            'type' => 'text',
            'col' => '12'
        ];
        $param['game_id'] = [
            'name' => '游戏列表',
            'type' => 'radio',
            'range' => $gameList,
            'col' => '12'
        ];
        $param['thumb'] = [
            'name' => '游戏缩略预览图（建议256×256像素）',
            'type' => 'thumb',
            'col' => '3',
            'accept' => 'image',
            'ratio' => '1'
        ];
        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        if($param['game_id']['value'] == null) $param['game_id']['value'] = 1;
        $viewData = [];
        $viewData['title'] = '发布新游戏';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.wechat.mp.game.resource.store', ['wechat_mp_id' => $wechatMpID]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回活动列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.game.resource.index', ['wechat_mp_id' => $wechatMpID])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建活动页面
     *
     * @param ProductPostRequest $request
     * @return mixed
     */
    public function store()
    {
        $wechatMpID = $this->request->input('wechat_mp_id');
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写活动名称');
        }
        if (!$this->request->has('win_odds')) {
            return redirect()->back()->with('error', '请填写获奖概率');
        }
        $winOdds = $this->request->input('win_odds');
        if($winOdds > 100 || $winOdds < 0) {
            return redirect()->back()->with('error', '获奖概率需要0-100之间');
        }
        if (!$this->request->has('win_value')) {
            return redirect()->back()->with('error', '请填写获奖数值');
        }
        if (!$this->request->has('game_id')) {
            return redirect()->back()->with('error', '请选择游戏');
        }
        $gameId = $this->request->input('game_id');
        $hasWMG = WechatMpGame::where('wechat_mp_id', $wechatMpID)->where('game_id', $gameId)->count();
        if($hasWMG > 0) {
            return redirect()->back()->with('error', '公众号下已存在该游戏');
        }
        if (!$this->request->has('thumb')) {
            return redirect()->back()->with('error', '必须上传影视缩略图');
        }
        $thumb = $this->request->input('thumb');
        $thumb = $this->uploadBase64Image($thumb, 'game');
        if (!$thumb) {
            return redirect()->back()->with('error', '游戏预览图文件上传失败!');
        }
        # 插入新数据
        $wechatMpGame = new WechatMpGame();
        $wechatMpGame->title = $this->request->input('title');
        $wechatMpGame->subtitle = $this->request->input('subtitle');
        $wechatMpGame->share_amount = $this->request->input('share_amount');
        $wechatMpGame->share_use_amount = $this->request->input('share_use_amount');
        $wechatMpGame->win_odds = $winOdds;
        $wechatMpGame->win_where = $this->request->input('win_where');
        $wechatMpGame->win_value = $this->request->input('win_value');
        $wechatMpGame->use_amount = $this->request->input('use_amount');
        $wechatMpGame->day_use_amount = $this->request->input('day_use_amount');
        $wechatMpGame->share_amount = $this->request->input('share_amount');
        $wechatMpGame->share_use_amount = $this->request->input('share_use_amount');
        $wechatMpGame->content = $this->request->input('content');
        $wechatMpGame->game_id = $this->request->input('game_id');
        $wechatMpGame->wechat_mp_id = $wechatMpID;
        $wechatMpGame->thumb = $thumb;
        $wechatMpGame->create_user = $this->user['id'];
        if ($wechatMpGame->save()) {
            # 创建成功后跳转到活动详细介绍编辑页
            return redirect()->route('manage.wechat.mp.game.resource.index', ['wechat_mp_id' => $wechatMpID])->with('success', '公众号游戏创建成功!');
        }
        return redirect()->back()->with('error', '公众号游戏创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $wechatMpGame = WechatMpGame::find($id);
        $gameList =  Game::keyName();
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '活动标题',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpGame->title
        ];
        $param['subtitle'] = [
            'name' => '小标题',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpGame->subtitle
        ];
        $param['share_amount'] = [
            'name' => '分享奖励数值',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->share_amount
        ];
        $param['share_use_amount'] = [
            'name' => '分享奖励游戏使用次数',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->share_use_amount
        ];
        $param['win_odds'] = [
            'name' => '获奖概率',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->win_odds
        ];
        $param['win_where'] = [
            'name' => '获奖条件',
            'type' => 'select',
            'range' => winWhereType(),
            'col' => '3',
            'value' => $wechatMpGame->win_where
        ];
        $param['win_value'] = [
            'name' => '获奖数值',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->win_value
        ];
        $param['use_amount'] = [
            'name' => '游戏总使用次数',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->use_amount
        ];
        $param['day_use_amount'] = [
            'name' => '每日游戏使用次数',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->day_use_amount
        ];
        $param['share_amount'] = [
            'name' => '分享奖励数值',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->share_amount
        ];
        $param['share_use_amount'] = [
            'name' => '分享奖励游戏使用次数',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGame->share_use_amount
        ];
        $param['content'] = [
            'name' => '活动说明',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpGame->content
        ];
        $param['game_id'] = [
            'name' => '游戏列表',
            'type' => 'radio',
            'range' => $gameList,
            'col' => '12',
            'value' => $wechatMpGame->game_id
        ];
        $param['thumb'] = [
            'type' => 'thumb',
            'col' => '3',
            'name' => '游戏缩略预览图（建议256×256像素）',
            'accept' => 'image',
            'value' => $this->domain . $this->path['game'] . $wechatMpGame->thumb,
            'ratio' => '1',
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改活动游戏信息';
        $viewData['nav'] = $this->navTab('wechat_mp_game', 'main', ['id' => $id]);
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.wechat.mp.game.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回活动列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.game.resource.index', ['wechat_mp_id' => $wechatMpGame->wechat_mp_id])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改管理员信息操作
     *
     * @param WechatActivityPostRequest $request
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写活动名称');
        }
        if (!$this->request->has('win_odds')) {
            return redirect()->back()->with('error', '请填写获奖概率');
        }
        $winOdds = $this->request->input('win_odds');
        if($winOdds > 100 || $winOdds < 0) {
            return redirect()->back()->with('error', '获奖概率需要0-100之间');
        }
        if (!$this->request->has('win_value')) {
            return redirect()->back()->with('error', '请填写获奖数值');
        }
        # 插入新数据
        $wechatMpGame = WechatMpGame::find($id);
        if (!$this->request->has('game_id')) {
            return redirect()->back()->with('error', '请选择游戏');
        }
        $gameId = $this->request->input('game_id');
        $hasWMG = WechatMpGame::where('wechat_mp_id', $wechatMpGame->wechat_mp_id)->where('game_id', $gameId)->where('id', '<>', $id)->count();
        if($hasWMG > 0) {
            return redirect()->back()->with('error', '公众号下已存在该游戏');
        }
        # 修改影视预览图
        $thumb = null;
        if ($this->request->has('thumb')) {
            $thumb = $this->request->input('thumb');
            $thumb = $this->uploadBase64Image($thumb, 'game');
            if (!$thumb) {
                return redirect()->back()->with('error', '游戏预览图文件上传失败!');
            }
        }
        $wechatMpGame->title = $this->request->input('title');
        $wechatMpGame->subtitle = $this->request->input('subtitle');
        $wechatMpGame->share_amount = $this->request->input('share_amount');
        $wechatMpGame->share_use_amount = $this->request->input('share_use_amount');
        $wechatMpGame->win_odds = $winOdds;
        $wechatMpGame->win_where = $this->request->input('win_where');
        $wechatMpGame->win_value = $this->request->input('win_value');
        $wechatMpGame->use_amount = $this->request->input('use_amount');
        $wechatMpGame->day_use_amount = $this->request->input('day_use_amount');
        $wechatMpGame->share_amount = $this->request->input('share_amount');
        $wechatMpGame->share_use_amount = $this->request->input('share_use_amount');
        $wechatMpGame->content = $this->request->input('content');
        $wechatMpGame->game_id = $this->request->input('game_id');
        $wechatMpGame->thumb = $thumb;
        $wechatMpGame->update_user = $this->user['id'];
        if (!$wechatMpGame->save()) {
            return redirect()->back()->with('error', '活动修改失败');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page,
            'wechat_mp_id' => $wechatMpGame->wechat_mp_id
        ];
        return redirect()->route('manage.wechat.mp.game.resource.index', $param)->with('success', '活动修改成功');
    }



    /**
     * 将游戏状态置为使用
     * @param $id
     */
    public function focus($id)
    {
        $wechatMpGame = WechatMpGame::find($id);
        if(!$wechatMpGame){
            return redirect()->back()->with('error', '未找到游戏!');
        }
        $wechatMpGame->status = ($wechatMpGame->status == 1) ? 0 : 1;
        if(!$wechatMpGame->save()) {
            return redirect()->back()->with('error', '状态修改失败!');
        }
        $param = [
          'wechat_mp_id' => $wechatMpGame->wechat_mp_id
        ];
        if($this->request->has('page')) {
            $param['page'] = $this->request->input('page');
        }
        return redirect()->route('manage.wechat.mp.game.resource.index', $param)->with('success', '游戏状态修改成功');
    }

}
