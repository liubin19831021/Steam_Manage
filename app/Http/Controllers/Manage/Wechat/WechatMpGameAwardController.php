<?php
namespace App\Http\Controllers\Manage\Wechat;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Wechat\WechatActivityGame;
use App\Model\Wechat\WechatMpGameAward;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class WechatMpGameAwardController extends CommonController
{
    /**
     * 查看活动游戏列表
     * @return mixed
     */
    public function index()
    {    
        $mpGameId = $this->request->input('wechat_mp_game_id');
        $field = [
            'id' => '奖品编号',
            'title' => '奖品名称',
            'type' => '奖品类型',
            'amount' => '库存数量',
            'create_time' => '发布时间'
        ];
        $viewData = [];
        $viewData['title'] = '游戏关联管理';
        $range = [
            'type' => awardType()
        ];
        $viewData['range'] = $range;
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新奖品',
                'route' => route('manage.wechat.mp.game.award.resource.create', ['wechat_mp_game_id' => $mpGameId])
            ]];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.wechat.mp.game.award.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [];
        $viewData['nav'] = $this->navTab('wechat_mp_game', 'game', ['id' => $mpGameId]);
        # 查询记录
        $wechatMpGameAward = WechatMpGameAward::where('wechat_mp_game_id', $mpGameId)->where('is_delete', 0);
        $urlParm = 'wechat_mp_game_id=' . $mpGameId;
        $data = $this->constraintSelect($wechatMpGameAward, $field, $range, $urlParm);
        $viewData['list'] = $data;
        # 渲染视图
        return view('manage.table', $viewData);
    }


    /**
     * 创建活动页面
     * @return mixed
     */
    public function create()
    {
        $wechatMpGameId = $this->request->input('wechat_mp_game_id');
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '奖品名称',
            'type' => 'text',
            'col' => '6'
        ];
        $param['amount'] = [
            'name' => '库存数量',
            'type' => 'number',
            'col' => '3'
        ];
        $param['type'] = [
            'name' => '商品类型',
            'type' => 'select',
            'range' => awardType(),
            'col' => '3'
        ];
        $param['product_id'] = [
            'name' => '商品ID',
            'type' => 'number',
            'col' => '3'
        ];
        $param['wechat_mp_redpack_id'] = [
            'name' => '微信红包',
            'type' => 'number',
            'col' => '3'
        ];
        $param['wechat_mp_card_id'] = [
            'name' => '微信卡券',
            'type' => 'number',
            'col' => '3'
        ];
        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '发布新奖品';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.wechat.mp.game.award.resource.store', ['wechat_mp_game_id' => $wechatMpGameId]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回活动列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.game.award.resource.index', ['wechat_mp_game_id' => $wechatMpGameId])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建活动——保存
     *
     * @param ProductPostRequest $request
     * @return mixed
     */
    public function store()
    {
        $wechatMpGameId = $this->request->input('wechat_mp_game_id');
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写奖品名称');
        }
        if (!$this->request->has('amount')) {
            return redirect()->back()->with('error', '请填写库存');
        }
        $type = $this->request->input('type');
        $wechatMpGameAward = new WechatMpGameAward();
        $msg = '';
        switch ($type) {
            case 0:
                if (!$this->request->has('product_id')) {
                    $msg = '请填写商品ID';
                }
                $wechatMpGameAward->product_id = $this->request->input('product_id');
                break;
            case 1:
                if (!$this->request->has('wechat_mp_redpack_id')) {
                    $msg = '请填写红包ID';
                }
                $wechatMpGameAward->wechat_mp_redpack_id = $this->request->input('wechat_mp_redpack_id');
                break;
            case 2:
                if (!$this->request->has('wechat_mp_card_id')) {
                    $msg = '请填写卡券ID';
                }
                $wechatMpGameAward->wechat_mp_card_id = $this->request->input('wechat_mp_card_id');
                break;
            default:
                break;
        }
        if($msg != '') {
            return redirect()->back()->with('error', $msg);
        }
        # 插入新数据
        $wechatMpGameAward->title = $this->request->input('title');
        $wechatMpGameAward->wechat_mp_game_id = $wechatMpGameId;
        $wechatMpGameAward->type = $type;
        $wechatMpGameAward->amount = $this->request->input('amount');
        $wechatMpGameAward->status = 1;
        $wechatMpGameAward->create_user = $this->user['id'];
        if ($wechatMpGameAward->save()) {
            # 创建成功后跳转到活动详细介绍编辑页
            return redirect()->route('manage.wechat.mp.game.award.resource.index', ['wechat_mp_game_id' => $wechatMpGameId])->with('success', '奖品创建成功!');
        }
        return redirect()->back()->with('error', '奖品创建失败!');
    }

    /**
     * 修改游戏规则
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $wechatMpGameAward = WechatMpGameAward::find($id);
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '奖品名称',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMpGameAward->title
        ];
        $param['amount'] = [
            'name' => '库存数量',
            'type' => 'number',
            'col' => '3',
            'value' => $wechatMpGameAward->amount
        ];
        $param['type'] = [
            'name' => '商品类型',
            'type' => 'label',
            'col' => '3',
            'value' => awardType($wechatMpGameAward->type)
        ];
        $param['product_id'] = [
            'name' => '商品ID',
            'type' => ($wechatMpGameAward->type == 0) ? 'number' : 'label',
            'col' => '3',
            'value' => ($wechatMpGameAward->type == 0)  ? $wechatMpGameAward->product_id : ' '
        ];
        $param['wechat_mp_redpack_id'] = [
            'name' => '微信红包',
            'type' => ($wechatMpGameAward->type == 1) ? 'number' : 'label',
            'col' => '3',
            'value' => ($wechatMpGameAward->type == 1)  ? $wechatMpGameAward->wechat_mp_redpack_id : ' '
        ];
        $param['wechat_mp_card_id'] = [
            'name' => '微信卡券',
            'type' => ($wechatMpGameAward->type == 2) ? 'number' : 'label',
            'col' => '3',
            'value' => ($wechatMpGameAward->type == 2)  ? $wechatMpGameAward->wechat_mp_card_id : ' '
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改游戏规则';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.wechat.mp.game.award.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回游戏列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.game.award.resource.index', ['wechat_mp_game_id' => $wechatMpGameAward->wechat_mp_game_id])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }


    public function update($id)
    {
        $wechatMpGameAward = WechatMpGameAward::find($id);
        $this->request->flash();
        $msg = '';
        switch ($wechatMpGameAward->type) {
            case 0:
                if (!$this->request->has('product_id')) {
                    $msg = '请填写商品ID';
                }
                $wechatMpGameAward->product_id = $this->request->input('product_id');
                break;
            case 1:
                if (!$this->request->has('wechat_mp_redpack_id')) {
                    $msg = '请填写红包ID';
                }
                $wechatMpGameAward->wechat_mp_redpack_id = $this->request->input('wechat_mp_redpack_id');
                break;
            case 2:
                if (!$this->request->has('wechat_mp_card_id')) {
                    $msg = '请填写卡券ID';
                }
                $wechatMpGameAward->wechat_mp_card_id = $this->request->input('wechat_mp_card_id');
                break;
            default:
                break;
        }
        if($msg != '') {
            return redirect()->back()->with('error', $msg);
        }
        $wechatMpGameAward->title = $this->request->input('title');
        $wechatMpGameAward->amount = $this->request->input('amount');
        $wechatMpGameAward->update_user = $this->user['id'];
        if (!$wechatMpGameAward->save()) {
            return redirect()->back()->with('error', '奖品修改失败');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page,
            'wechat_mp_game_id' => $wechatMpGameAward->wechat_mp_game_id
        ];
        return redirect()->route('manage.wechat.mp.game.award.resource.index', $param)->with('success', '奖品修改成功!');
    }
}