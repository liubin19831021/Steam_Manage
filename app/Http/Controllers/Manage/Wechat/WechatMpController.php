<?php
namespace App\Http\Controllers\Manage\Wechat;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Wechat\WechatMp;
use Illuminate\Support\Facades\DB;

class WechatMpController extends CommonController
{
    /**
     * 查看公众号
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => '公众号编号',
            'title' => '公众号名称',
            'app_id' => '应用ID',
            'url' => '服务器地址',
            'create_time' => '发布时间'
        ];
        $viewData = [];
        $viewData['title'] = '公众号管理';
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新公众号',
                'route' => route('manage.wechat.mp.resource.create')
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.wechat.mp.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [];
        # 查询记录
        $wechatMp = new WechatMp();
        $viewData['list'] = $this->constraintSelect($wechatMp, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 创建公众号页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '公众号名称',
            'type' => 'text',
            'col' => '12'
        ];
        $param['app_id'] = [
            'name' => 'app_id',
            'type' => 'text',
            'col' => '6'
        ];
        $param['app_secret'] = [
            'name' => '应用密钥',
            'type' => 'text',
            'col' => '6'
        ];
        $param['token'] = [
            'name' => '令牌',
            'type' => 'text',
            'col' => '6'
        ];
        $param['encoding_aes_key'] = [
            'name' => '消息加解密密钥',
            'type' => 'text',
            'col' => '6'
        ];
        $param['url'] = [
            'name' => '服务器地址',
            'type' => 'text',
            'col' => '6'
        ];
        $param['mch_id'] = [
            'name' => '支付ID',
            'type' => 'text',
            'col' => '6'
        ];
        $param['api_key'] = [
            'name' => '支付秘钥',
            'type' => 'text',
            'col' => '6'
        ];
        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '发布新公众号';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.wechat.mp.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回公众号列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建公众号页面
     *
     * @param ProductPostRequest $request
     * @return mixed
     */
    public function store()
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写公众号名称');
        }
        if (!$this->request->has('url')) {
            return redirect()->back()->with('error', '请填写服务器地址');
        }
        # 插入新数据
        $wechatMp = new WechatMp();
        $wechatMp->title = $this->request->input('title');
        $wechatMp->app_id = $this->request->input('app_id');
        $wechatMp->app_secret = $this->request->input('app_secret');
        $wechatMp->token = $this->request->input('token');
        $wechatMp->encoding_aes_key = $this->request->input('encoding_aes_key');
        $wechatMp->url = $this->request->input('url');
        $wechatMp->mch_id = $this->request->input('mch_id');
        $wechatMp->api_key = $this->request->input('api_key');
        $wechatMp->create_user = $this->user['id'];
        if ($wechatMp->save()) {
            # 创建成功后跳转到公众号详细介绍编辑页
            return redirect()->route('manage.wechat.mp.resource.index')->with('success', '公众号创建成功!');
        }
        return redirect()->back()->with('error', '公众号创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $wechatMp = WechatMp::find($id);
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '公众号名称',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMp->title
        ];
        $param['app_id'] = [
            'name' => 'app_id',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->app_id
        ];
        $param['app_secret'] = [
            'name' => '应用密钥',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->app_secret
        ];
        $param['token'] = [
            'name' => '令牌',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->token
        ];
        $param['encoding_aes_key'] = [
            'name' => '消息加解密密钥',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->encoding_aes_key
        ];
        $param['url'] = [
            'name' => '服务器地址',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->url
        ];
        $param['mch_id'] = [
            'name' => '支付ID',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->mch_id
        ];
        $param['api_key'] = [
            'name' => '支付秘钥',
            'type' => 'text',
            'col' => '6',
            'value' => $wechatMp->api_key
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改公众号信息';
        $viewData['nav'] = $this->navTab('wechat_mp', 'main', ['id' => $id]);
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.wechat.mp.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回公众号列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改管理员信息操作
     *
     * @param WechatMpPostRequest $request
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $wechatMp = WechatMp::find($id);
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写公众号名称');
        }
        if (!$this->request->has('url')) {
            return redirect()->back()->with('error', '请填写服务器地址');
        }
        $wechatMp->title = $this->request->input('title');
        $wechatMp->app_id = $this->request->input('app_id');
        $wechatMp->app_secret = $this->request->input('app_secret');
        $wechatMp->token = $this->request->input('token');
        $wechatMp->encoding_aes_key = $this->request->input('encoding_aes_key');
        $wechatMp->url = $this->request->input('url');
        $wechatMp->mch_id = $this->request->input('mch_id');
        $wechatMp->api_key = $this->request->input('api_key');
        $wechatMp->update_user = $this->user['id'];
        if (!$wechatMp->save()) {
            return redirect()->back()->with('error', '公众号修改成功');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.wechat.mp.resource.index', $param)->with('success', '公众号修改成功');
    }

}
