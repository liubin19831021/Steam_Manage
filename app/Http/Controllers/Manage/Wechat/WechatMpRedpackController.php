<?php
namespace App\Http\Controllers\Manage\Wechat;

use App\Http\Controllers\Manage\CommonController;
use App\Http\Requests\MpRedpackPostRequest;
use App\Model\Wechat\WechatMpRedpack;
use Illuminate\Support\Facades\DB;
use Form;

class WechatMpRedpackController extends CommonController
{
    /**
     * 查看红包
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => '编号',
            'title' => '标题名称',
            'type' => '类型',
            'price' => '红包金额',
            'max_price' => '最大金额',
            'min_price' => '最小金额',
            'create_time' => '发布时间'
        ];
        $viewData = [];
        $viewData['title'] = '红包管理';
        $viewData['range'] = [
            'status' => YN(),
            'type' => mpRedpackType()
        ];
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新红包',
                'route' => route('manage.wechat.mp.redpack.resource.create')
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.wechat.mp.redpack.resource.edit',
                'param' =>   [
                        'page' => $page
                    ]
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [];
        # 查询记录
        $wechatMpRedpack = new WechatMpRedpack();
        $viewData['list'] = $this->constraintSelect($wechatMpRedpack, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 创建红包页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '标题名称',
            'type' => 'text',
            'col' => '12'
        ];
        $param['type'] = [
            'name' => '类型',
            'type' => 'select',
            'col' => '3',
            'range' => mpRedpackType()
        ];
        $param['price'] = [
            'name' => '红包金额',
            'type' => 'text',
            'col' => '3'
        ];
        $param['max_price'] = [
            'name' => '最大金额',
            'type' => 'text',
            'col' => '3'
        ];
        $param['min_price'] = [
            'name' => '最小金额',
            'type' => 'text',
            'col' => '3'
        ];
        $param['wishing'] = [
            'name' => '红包祝福语',
            'type' => 'text',
            'col' => '12'
        ];
        $param['remark'] = [
            'name' => '备注',
            'type' => 'text',
            'col' => '12'
        ];
        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '发布新红包';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.wechat.mp.redpack.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回红包列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.redpack.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建红包页面
     *
     * @param MpRedpackPostRequest $request
     * @return mixed
     */
    public function store(MpRedpackPostRequest $request)
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标题名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写红包金额');
        }
        if (!$this->request->has('wishing')) {
            return redirect()->back()->with('error', '请填写红包祝福语');
        }
        # 插入新数据
        $wechatMpRedpack = new WechatMpRedpack();
        $wechatMpRedpack->title = $this->request->input('title');
        $wechatMpRedpack->type = $this->request->input('type');
        $wechatMpRedpack->price = $this->request->input('price');
        $wechatMpRedpack->max_price = $this->request->input('max_price');
        $wechatMpRedpack->min_price = $this->request->input('min_price');
        $wechatMpRedpack->wishing = $this->request->input('wishing');
        $wechatMpRedpack->remark = $this->request->input('remark');
        $wechatMpRedpack->create_user = $this->user['id'];
        if (!$wechatMpRedpack->save()) {
            return redirect()->back()->with('error', '红包创建失败!');
        }
        # 创建成功后跳转到红包详细介绍编辑页
        return redirect()->route('manage.wechat.mp.redpack.resource.index')->with('success', '红包创建成功!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $wechatMpRedpack = WechatMpRedpack::find($id);
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '标题名称',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpRedpack->title
        ];
        $param['type'] = [
            'name' => '类型',
            'type' => 'select',
            'col' => '3',
            'range' => mpRedpackType(),
            'value' => $wechatMpRedpack->type
        ];
        $param['price'] = [
            'name' => '红包金额',
            'type' => 'text',
            'col' => '3',
            'value' => $wechatMpRedpack->price
        ];
        $param['max_price'] = [
            'name' => '最大金额',
            'type' => 'text',
            'col' => '3',
            'value' => $wechatMpRedpack->max_price
        ];
        $param['min_price'] = [
            'name' => '最小金额',
            'type' => 'text',
            'col' => '3',
            'value' => $wechatMpRedpack->min_price
        ];
        $param['wishing'] = [
            'name' => '红包祝福语',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpRedpack->wishing
        ];
        $param['remark'] = [
            'name' => '备注',
            'type' => 'text',
            'col' => '12',
            'value' => $wechatMpRedpack->remark
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改红包信息';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.wechat.mp.redpack.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回红包列表',
                'type' => 'button',
                'route' => route('manage.wechat.mp.redpack.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改管理员信息操作
     *
     * @param MpRedpackPostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(MpRedpackPostRequest $request, $id)
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标题名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写红包金额');
        }
        if (!$this->request->has('wishing')) {
            return redirect()->back()->with('error', '请填写红包祝福语');
        }
        $wechatMpRedpack = WechatMpRedpack::find($id);
        $wechatMpRedpack->title = $this->request->input('title');
        $wechatMpRedpack->type = $this->request->input('type');
        $wechatMpRedpack->price = $this->request->input('price');
        $wechatMpRedpack->max_price = $this->request->input('max_price');
        $wechatMpRedpack->min_price = $this->request->input('min_price');
        $wechatMpRedpack->wishing = $this->request->input('wishing');
        $wechatMpRedpack->remark = $this->request->input('remark');
        $wechatMpRedpack->update_user = $this->user['id'];
        if (!$wechatMpRedpack->save()) {
            return redirect()->back()->with('error', '红包修改失败');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.wechat.mp.redpack.resource.index', $param)->with('success', '红包修改成功');
    }
}
