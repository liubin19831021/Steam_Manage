<?php
namespace App\Http\Controllers\Manage;

use App\Model\Mall\Product;
use App\Model\Mall\Order;
use App\Model\Member\Member;
use Illuminate\Support\Facades\DB;

class IndexController extends CommonController
{
    public function index()
    {
        # 会员总数
        $userCount= Member::where('status',1)->count();
//        # 商品总数
//        $goodsCount= Product::where('is_delete',0)->count();
//        # 订单总数
//        $orderCount= Order::where('status','<>',0)->count();
        # 订单数量分布

//        $data = DB::table('order_goods')->select(DB::raw('SUM(goods_number) as num',"date_format(create_time,'%Y-%c') as date"))
//            ->where(DB::raw("date_format(create_time,'%Y')"),"date_format(NOW(),'%Y')")
//            ->groupBy(DB::raw("date_format(create_time,'%Y-%c')"))
//            ->orderBy('create_time')
//            ->get();
       // p(DB::getQueryLog());
//        $data=DB::select("SELECT sum(goods_number) as num,date_format(create_time,'%Y-%c') as date  from emall_lottery_product where date_format(create_time,'%Y') = date_format(NOW(),'%Y')
//        GROUP BY date_format(create_time,'%Y-%c') ORDER BY create_time");
//        $data = json_encode($data);

        return view('manage.index')
            ->with('userCount',$userCount)
//            ->with('storeCount',$storeCount)
            ->with('goodsCount',0)
            ->with('orderCount',0)
            ->with('year',date('Y',time()))
            ->with('data',null);
    }

    public function testImage()
    {
        //echo captcha_img();
        /*
        Region::name(0,0,0);
        exit;
        $t1 = microtime(true);
        $result = Region::get()->toArray();
        $address_list = treeCategory($result, 'id', 'name', 'parent_id');
        echo count($address_list);
        $t2 = microtime(true);
        echo '耗时'.round($t2-$t1,3).'秒';
        //$result = curlGet('http://42.62.98.45/user/account/register?phone=13102219912&password=123456&username=abc123&occupation=1');
        //var_dump($result);
        //echo formatBytes(798600);
        //formatBytes(798600);
        */
        /*
        //$this->image('image/test.jpg', 'file');
        */
        # 无限树形下来列表

        //$ca = Category::get()->toArray();
        $t1 = microtime(true);
        /*
        $result = tree($ca, 'id', 'name', 'parent_id');
        print_r($result);
        */
        //p(Category::getParentCategoryName(164));
        $t2 = microtime(true);
//        echo '耗时'.round($t2-$t1,3).'秒';
        echo str_pad(12,8,'0',STR_PAD_RIGHT);

/*
        $arr = array(); # 资源数组
        $result = Category::get();
        foreach ($result as $value) {
            $arr[$value->parent_id][] = $value->id;
        }
        $set = $arr[9];
        $flag = $set;
        while (count($flag) > 0) {
            $temp = array();
            foreach ($flag as $v) {
                if (isset($arr[$v])) {
                    $set = array_merge($set, $arr[$v]);
                    $temp = array_merge($temp, $arr[$v]);
                }
            }
            $flag = $temp;
        }
        print_r($set);
*/
    }

    public function testIndex()
    {
        $uniqid = uniqid();
        $uniqid = substr($uniqid, (strlen($uniqid) - 6));
        $time = time();
        $time = substr($time, 8);
//        echo $time;
        $code = strtoupper($uniqid . $time);
        echo $code;
//        $str = '10a1' + '2b33';
//        echo $time;
//        echo ($uniqid);

//        echo sha1(uniqid());
//        echo md5(time() . mt_rand(1,1000000));
    }
}
