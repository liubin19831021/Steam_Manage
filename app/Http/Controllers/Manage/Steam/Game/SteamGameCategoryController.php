<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameCategory;

class SteamGameCategoryController extends CommonController
{
    /**
     * 查看分类
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => '编号',
            'name' => '分类名称',
            'rank' => '顺序'
        ];
        $viewData = [];
        $viewData['title'] = 'Steam游戏分类';

        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新分类',
                'route' => route('manage.steam.game.category.resource.create')
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.steam.game.category.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.category.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除分类',
                'msg' => '确定要删除该分类吗'
            ],
        ];
        # 查询记录
        $steamGameCategory = new SteamGameCategory();
        $viewData['list'] = $this->constraintSelect($steamGameCategory, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除分类
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameCategory::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '分类删除成功!');
        }
        return redirect()->back()->with('error', '分类删除失败!');

    }

    /**
     * 添加分类
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [];
        $param['name'] = [
            'name' => '分类名称',
            'type' => 'text',
            'col' => '5'
        ];
        $param['rank'] = [
            'name' => '顺序',
            'type' => 'text',
            'col' => '1'
        ];

        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }

        $viewData = [];
        $viewData['title'] = '发布新分类';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.category.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回分类列表',
                'type' => 'button',
                'route' => route('manage.steam.game.category.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建分类
     *
     * @param  $request
     * @return mixed
     */
    public function store()
    {
        $this->request->flash();
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写分类名称');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序');
        }

        # 插入新数据
        $SteamGameCategory = new SteamGameCategory();
        $SteamGameCategory->name = $this->request->input('name');
        $SteamGameCategory->rank = $this->request->input('rank');
        $SteamGameCategory->create_user = $this->user['id'];
        if ($SteamGameCategory->save()) {
            # 创建成功后跳转到公众号详细介绍编辑页
            return redirect()->route('manage.steam.game.category.resource.index')->with('success', '分类创建成功!');
        }
        return redirect()->back()->with('error', '分类创建失败!');
    }

    /**
     * 修改分类
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $SteamGameCategory = SteamGameCategory::find($id);
        # 表单元素
        $param = [];
        $param['name'] = [
            'name' => '分类名称',
            'type' => 'text',
            'col' => '5',
            'value' => $SteamGameCategory->name
        ];
        $param['rank'] = [
            'name' => '顺序',
            'type' => 'text',
            'col' => '1',
            'value' => $SteamGameCategory->rank
        ];

        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改分类信息';
        
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.category.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回分类列表',
                'type' => 'button',
                'route' => route('manage.steam.game.category.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改分类操作
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $SteamGameCategory = SteamGameCategory::find($id);
        $this->request->flash();
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写分类名称');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序');
        }
        $SteamGameCategory->name = $this->request->input('name');
        $SteamGameCategory->rank = $this->request->input('rank');

        if (!$SteamGameCategory->save()) {
            return redirect()->back()->with('error', '分类修改成功');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.steam.game.category.resource.index', $param)->with('success', '分类修改成功');
    }
}