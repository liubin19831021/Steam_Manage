<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameNews;
use App\Model\Steam\SteamGameChannel;
use App\Model\Steam\SteamGame;
class SteamGameNewsController extends CommonController
{
    /**
     * 新闻列表页
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'title' => '标题',
            'author' => '作者',
            'issue_date' => '发布日期',
            'rank' => '顺序',
            'channel_id' => '频道',
            'game_id' => '关联游戏编号',
        ];
        $viewData = [];
        $viewData['title'] = '新闻管理';
        $viewData['nav'] = $this->navTab('steam_news', 'main');
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'channel_id' => $this->getChannel()
        ];
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '添加新闻',
                'route' => route('manage.steam.game.news.resource.create')
            ]
        ];
        $page = $this->request->input('page', 1);
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title' => '修改',
                'route' => 'manage.steam.game.news.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.news.resource.destroy',
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除新闻',
                'msg' => '确定要删除新闻吗'
            ]
        ];
        #列表数据
        $steamGameNews = new SteamGameNews;
        $viewData['list'] = $this->constraintSelect($steamGameNews, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除新闻
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameNews::where('id', $id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '新闻删除成功!');
        }
        return redirect()->back()->with('error', '新闻删除失败!');
    }

    /**
     * 创建新闻页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'title' => [
                'type' => 'text',
                'col' => '12',
                'name' => '标题',
            ],
            'channel_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '频道',
                'range' => $this->getChannel()
            ],
            'author' => [
                'type' => 'text',
                'col' => '4',
                'name' => '作者'
            ],
            'rank' => [
                'type' => 'text',
                'col' => '4',
                'name' => '顺序'
            ],
            'game_id' => [
                'type' => 'text',
                'col' => '4',
                'name' => '关联游戏编号'
            ],
            'issue_date' => [
                'type' => 'datetimepicker',
                'col' => '4',
                'name' => '发布日期'
            ],
            'content' => [
                'type' => 'editor',
                'col' => '12',
                'name' => '',
                'upload_image' => route('manage.steam.game.describe.upload',['id' => 1]),
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加新闻';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.news.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回新闻列表',
                'type' => 'button',
                'route' => route('manage.steam.game.news.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建新闻操作
     * @return mixed
     */
    public function store()
    {
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标题!');
        }
        if (!$this->request->has('author')) {
            return redirect()->back()->with('error', '请填写作者!');
        }
        if (!$this->request->has('issue_date')) {
            return redirect()->back()->with('error', '请填写发布日期!');
        }
        if (!$this->request->has('channel_id')) {
            return redirect()->back()->with('error', '请填写频道!');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序!');
        }
        if (!$this->request->has('content')) {
            return redirect()->back()->with('error', '请填写内容!');
        }
        if (!$this->request->has('game_id')) {
            return redirect()->back()->with('error', '请填写游戏关联编号!');
        }
        $steamGame = SteamGame::where('id',$this->request->input('game_id'))->count();
        if($steamGame==0)
        {
            return redirect()->back()->with('error', '该游戏不存在');
        }
//        $category= $this->request->input('category');
//        $adCategory = AdCategory::find($category);
//        if ($adCategory->total > 0) {
//            $count = Ad::where('category_id', $category)->count();
//            if ($count >= $adCategory->total) {
//                return redirect()->back()->with('error', '当前广告位已到达最大限制!');
//            }
//        }
//        else{
//            return redirect()->back()->with('error', '当前广告位数量限制异常!');
//        }
//        if (!$this->request->hasFile('picture')) {
//            return redirect()->back()->with('error', '请选择广告图!');
//        }
//        $picture = $this->request->file('picture');
//        $file = $this->uploadImage($picture, 'ad');
//        if (isset($file['error'])) {
//            return redirect()->back()->with('error', $file['error']);
//        }
        $steamGameNews = new SteamGameNews;
        $steamGameNews->create_user = $this->user['id'];
        $steamGameNews->rank = $this->request->input('rank');
        $steamGameNews->title = $this->request->input('title');
        $steamGameNews->author = $this->request->input('author');
        $steamGameNews->channel_id = $this->request->input('channel_id');
        $steamGameNews->issue_date = $this->request->input('issue_date');
        $steamGameNews->content = $this->request->input('content');
        $steamGameNews->game_id = $this->request->input('game_id');
        if ($steamGameNews->save()) {
            return redirect()->route('manage.steam.game.news.resource.index')->with('success', '新闻创建成功!');
        }
        return redirect()->back()->with('error', '新闻创建失败!');
    }

    /**
     * 修改广告信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = SteamGameNews::find($id);
//        $picture = array();
//        $picture[] = [
//            'original' => '原始文件(' . $data->img_original . ')',
//            'name' => $data->picture,
//            'file' => $this->domain . $this->path['ad'] . $data->picture,
//            'id' => $id,
//        ];
//        $picture = json_encode($picture);
        # 表单元素
        $param = [
            'title' => [
                'type' => 'text',
                'col' => '12',
                'name' => '标题',
                'value'=>$data->title
            ],
            'channel_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '频道',
                'range' => $this->getChannel(),
                'value'=>$data->channel_id
            ],
            'author' => [
                'type' => 'text',
                'col' => '4',
                'name' => '作者',
                'value'=>$data->channel_id
            ],
            'rank' => [
                'type' => 'text',
                'col' => '4',
                'name' => '顺序',
                'value'=>$data->rank
            ],
            'game_id' => [
                'type' => 'text',
                'col' => '4',
                'name' => '关联游戏编号',
                'value'=>$data->game_id
            ],
            'issue_date' => [
                'type' => 'datetimepicker',
                'col' => '4',
                'name' => '发布日期',
                'value'=>$data->issue_date
            ],
            'content' => [
                'type' => 'editor',
                'col' => '12',
                'name' => '',
                'upload_image' => route('manage.steam.game.describe.upload',['id' => 1]),
                'value'=>$data->content
            ]
        ];
        $viewData = [];
        $viewData['title'] = '修改新闻';
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.news.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '返回新闻列表',
                'type' => 'button',
                'route' => route('manage.steam.game.news.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 修改新闻信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标题!');
        }
        if (!$this->request->has('author')) {
            return redirect()->back()->with('error', '请填写作者!');
        }
        if (!$this->request->has('issue_date')) {
            return redirect()->back()->with('error', '请填写发布日期!');
        }
        if (!$this->request->has('channel_id')) {
            return redirect()->back()->with('error', '请填写频道!');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序!');
        }
        if (!$this->request->has('content')) {
            return redirect()->back()->with('error', '请填写内容!');
        }
        if (!$this->request->has('game_id')) {
            return redirect()->back()->with('error', '请填写游戏关联编号!');
        }
        $steamGame = SteamGame::where('id',$this->request->input('game_id'))->count();
        if($steamGame==0)
        {
            return redirect()->back()->with('error', '该游戏不存在');
        }
        $steamGameNews = SteamGameNews::find($id);
        $steamGameNews->update_user = $this->user['id'];
        $steamGameNews->rank = $this->request->input('rank');
        $steamGameNews->title = $this->request->input('title');
        $steamGameNews->author = $this->request->input('author');
        $steamGameNews->channel_id = $this->request->input('channel_id');
        $steamGameNews->issue_date = $this->request->input('issue_date');
        $steamGameNews->content = $this->request->input('content');
        $steamGameNews->game_id = $this->request->input('game_id');
//        $category = $this->request->input('category');
//        # 获取当前广告位
//        $adCategory = AdCategory::find($category);
//        if ($adCategory->total < 1) {
//            return redirect()->back()->with('error', '当前广告位最大限制异常!');
//        }
//        # 当前广告位广告总数
//        $count = Ad::where('category_id', $category)->count();
//        # 如果没有修改广告位置，只需要判断当前广告位是否溢出
//        if ($category == $ad->category_id) {
//            if ($count > $adCategory->total) {
//                return redirect()->back()->with('error', '当前广告位已到达最大限制!');
//            }
//        }
//        else{
//            # 如果没有修改广告位置，需要判断当前广告位是否饱和
//            if ($count >= $adCategory->total) {
//                return redirect()->back()->with('error', '当前广告位已到达最大限制!');
//            }
//        }
//        $ad->category_id = $category;
//        if ($this->request->hasFile('picture')) {
//            $picture = $this->request->file('picture');
//            $file = $this->uploadImage($picture, 'ad');
//            if (isset($file['file'])) {
//                $ad->picture = $file['file']['name'];
//            }
//        }
//        $ad->name = $this->request->input('name');
//        $ad->link = $this->request->input('link');
        if (!$steamGameNews->save()) {
            return redirect()->back()->with('error', '新闻修改失败!');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.steam.game.news.resource.index', $param)->with('success', '新闻修改成功!');
    }

    /**
     * 获取频道列表
     * @return mixed
     */
    private function getChannel()
    {
        $category = array();
        $result = SteamGameChannel::get();
        foreach ($result as $key => $val) {
            $category[$val->id] = $val->name;
        }
        return $category;
    }
}