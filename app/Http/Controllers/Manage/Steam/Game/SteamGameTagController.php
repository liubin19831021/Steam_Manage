<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameTag;

class SteamGameTagController extends CommonController
{
    /**
     * 查看游戏标签
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => '编号',
            'title' => '标签名称',
            'status' => '状态'
        ];
        $viewData = [];
        $viewData['title'] = 'Steam标签管理';
        $viewData['range'] = [
            'status' => isUse()
        ];
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新标签',
                'route' => route('manage.steam.game.tag.resource.create')
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.steam.game.tag.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.tag.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除标签',
                'msg' => '确定要删除该标签吗'
            ],
        ];
        # 查询记录
        $steamGameTag = new SteamGameTag();
        $viewData['list'] = $this->constraintSelect($steamGameTag, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 添加steam游戏标签
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '标签名称',
            'type' => 'text',
            'col' => '5'
        ];
        $param['status'] = [
            'name' => '状态',
            'type' => 'select',
            'col' => '1',
            'range' =>isUse()
        ];
        

        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        if($param['status']['value'] == null) $param['status']['value'] = 1;
        $viewData = [];
        $viewData['title'] = '发布新标签';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.tag.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回标签列表',
                'type' => 'button',
                'route' => route('manage.steam.game.tag.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建游戏标签
     *
     * @param  $request
     * @return mixed
     */
    public function store()
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标签名称');
        }
        if (!$this->request->has('status')) {
            return redirect()->back()->with('error', '请填写状态');
        }

        # 插入新数据
        $steamGameTag = new SteamGameTag();
        $steamGameTag->title = $this->request->input('title');
        $steamGameTag->status = $this->request->input('status');

        $steamGameTag->create_user = $this->user['id'];
        if ($steamGameTag->save()) {
            # 创建成功后跳转到公众号详细介绍编辑页
            return redirect()->route('manage.steam.game.tag.resource.index')->with('success', '标签创建成功!');
        }
        return redirect()->back()->with('error', '标签创建失败!');
    }

    /**
     * 修改游戏页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $steamGameTag = SteamGameTag::find($id);
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '标题',
            'type' => 'text',
            'col' => '6',
            'value' => $steamGameTag->title
        ];
        $param['status'] = [
            'name' => '状态',
            'type' => 'select',
            'col' => '1',
            'range' =>isUse(),
            'value' => $steamGameTag->status
        ];

        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改标签信息';
        
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.tag.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回标签列表',
                'type' => 'button',
                'route' => route('manage.steam.game.tag.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改标签操作
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $steamGameTag = SteamGameTag::find($id);
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写标签名称');
        }
        if (!$this->request->has('status')) {
            return redirect()->back()->with('error', '请填写状态');
        }
        $steamGameTag->title = $this->request->input('title');
        $steamGameTag->status = $this->request->input('status');

        $steamGameTag->update_user = $this->user['id'];
        if (!$steamGameTag->save()) {
            return redirect()->back()->with('error', '标签修改成功');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.steam.game.tag.resource.index', $param)->with('success', '标签修改成功');
    }
    /**
     * 删除标签
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameTag::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '标签删除成功!');
        }
        return redirect()->back()->with('error', '标签删除失败!');

    }
}