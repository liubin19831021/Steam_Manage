<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Ext\Common\UploadImage;
use App\Http\Controllers\Manage\CommonController;

use App\Model\Steam\SteamGame;

class SteamGameDescribeController extends CommonController
{
    /**
     * 获取指定商品详细描述信息
     * @return mixed
     */
    public function index($id)
    {
        $data = SteamGame::find($id);
        $viewData = [];
        $viewData['title'] = '详细介绍';
        $param = [
            'description' => [
                'name' => '',
                'type' => 'editor',
                'col' => '12',
                'upload_image' => route('manage.steam.game.describe.upload', ['id' => $id]),
                'value' => $data->description
            ],
        ];
        $viewData['nav'] = $this->navTab('steam_game', 'describe', ['id' => $id]);
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.describe.update', ['id' => $id]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回游戏列表',
                'type' => 'button',
                'route' => route('manage.steam.game.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'button',
                'route' => route('manage.steam.game.describe', ['id' => $id])
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 保存指定游戏详细描述信息
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $game = SteamGame::find($id);
        if ($game) {
            $game->description = $this->request->input('description');
            $game->update_user = $this->user['id'];
            if ($game->save()) {
                return redirect()->route('manage.steam.game.describe', ['id' => $id])->with('success', '游戏详细介绍修改成功!');
            }
        }
        return redirect()->back()->with('error', '游戏详细介绍修改失败!');
    }

    /**
     *
     * 上传文章图片接口
     * 返回summernote编辑器的值
     *
     * @return mixed
     */
    public function upload($id)
    {
        $response = array();
        if (count($_FILES) < 1) {
            $response['state'] = '没有接受到上传的文件!';
            return response()->json($response);
        }
        $files = $this->request->file('image');
        $images = array();
        foreach ($files as $file) {
            # 原始文件名
            $originalName = $file->getClientOriginalName();
            # 新文件名
            $fileName = time() . substr(mt_rand(), 5);
            $fileName = $fileName . '.' . $file->getClientOriginalExtension();
            # 验证图片大小
            $sizeMax = formatBytes($file->getMaxFilesize());
            $sizeMax = floor($sizeMax);
            $fileSize = formatBytes($file->getClientSize());
            if ($fileSize > $sizeMax) {
                $response['state'] = '文件"' . $originalName . '"容量为' . $fileSize . 'MB，超过上传文件大小限制，文件上传的最大限制为' . $sizeMax . 'MB';
                return response()->json($response);
            }
            # 验证图片格式
            $image = new UploadImage();
            $mime = ['jpg','jpeg','png']; # 文件类型限制
            if (!$image->checkMime($file, $mime)) {
                $response['state'] = '上传的文件格式限制为"jpg,jpeg,png"';
                return response()->json($response);
            }

            # 保存文件对象
            $imageParam = [
                'max' => ['w' => 1024, 'watermark' => $this->filePath['water_mark']],
                'small' => ['w' => 24, 'h' => 24],
            ];
            $result = $image->save($file, $this->path['steam_game_article'], $fileName, $imageParam);
            # 上传到云存储
            if ($result) $images[] = $result;
        }
        $response['state'] = 'SUCCESS';
        $response['file'] = $images;
        return response()->json($response);
    }
}
