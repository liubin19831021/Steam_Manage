<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameGoods;
class SteamGameGoodsController extends CommonController
{
    /**
     * 查看steam游戏商品
     * @return mixed
     */
    public function index()
    {
        $steamGameID = $this->request->input('steam_game_id');
        $field = [
            'id' => '编号',
            'title' => '标题',
            'price' => '价格',
            'type' => '类型'
        ];
        $viewData = [];
        $viewData['title'] = 'Steam游戏商品管理';
        $viewData['range'] = [
            'type' => gameGoodsType()
        ];
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新商品',
                'route' => route('manage.steam.game.goods.resource.create', ['steam_game_id' => $steamGameID])
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.steam.game.goods.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.goods.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除商品',
                'msg' => '确定要删除该商品吗'
            ],
        ];
        $viewData['nav'] = $this->navTab('steam_game', 'goods', ['id' => $steamGameID]);
        # 查询记录
        $steamGameGoods = SteamGameGoods::where('game_id', $steamGameID);
        $viewData['list'] = $this->constraintSelect($steamGameGoods, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }
    /**
     * 创建游戏商品
     * @return mixed
     */
    public function create()
    {
        $steamGameID = $this->request->input('steam_game_id');
        //$gameList =  Game::keyName();
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '活动标题',
            'type' => 'text',
            'col' => '12'
        ];
        $param['price'] = [
            'name' => '价格',
            'type' => 'text',
            'col' => '6'
        ];
        $param['type'] = [
            'name' => '类型',
            'type' => 'select',
            'col' => '6',
            'range' => gameGoodsType()
        ];

        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        if($param['type']['value'] == null) $param['type']['value'] = 1;
        $viewData = [];
        $viewData['title'] = '发布新商品';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.goods.resource.store', ['steam_game_id' => $steamGameID]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回商品列表',
                'type' => 'button',
                'route' => route('manage.steam.game.goods.resource.index', ['steam_game_id' => $steamGameID])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }
    /**
     *
     * 创建游戏商品
     *
     * @param  $request
     * @return mixed
     */
    public function store()
    {
        $steamGameID = $this->request->input('steam_game_id');
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写商品名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写商品价格');
        }

        if (!$this->request->has('type')) {
            return redirect()->back()->with('error', '请填写商品类型');
        }

//        $gameId = $this->request->input('game_id');
//        $hasWMG = WechatMpGame::where('wechat_mp_id', $wechatMpID)->where('game_id', $gameId)->count();
//        if($hasWMG > 0) {
//            return redirect()->back()->with('error', '公众号下已存在该游戏');
//        }
//        if (!$this->request->has('thumb')) {
//            return redirect()->back()->with('error', '必须上传影视缩略图');
//        }
//        $thumb = $this->request->input('thumb');
//        $thumb = $this->uploadBase64Image($thumb, 'game');
//        if (!$thumb) {
//            return redirect()->back()->with('error', '游戏预览图文件上传失败!');
//        }
        # 插入新数据
        $steamGameGoods = new SteamGameGoods();
        $steamGameGoods->title = $this->request->input('title');
        $steamGameGoods->game_id = $steamGameID;
        $steamGameGoods->price = $this->request->input('price');
        $steamGameGoods->type = $this->request->input('type');
//        $wechatMpGame->win_odds = $winOdds;
//        $wechatMpGame->win_where = $this->request->input('win_where');
//        $wechatMpGame->win_value = $this->request->input('win_value');
//        $wechatMpGame->use_amount = $this->request->input('use_amount');
//        $wechatMpGame->day_use_amount = $this->request->input('day_use_amount');
//        $wechatMpGame->share_amount = $this->request->input('share_amount');
//        $wechatMpGame->share_use_amount = $this->request->input('share_use_amount');
//        $wechatMpGame->content = $this->request->input('content');
//        $wechatMpGame->game_id = $this->request->input('game_id');
//        $wechatMpGame->wechat_mp_id = $wechatMpID;
//        $wechatMpGame->thumb = $thumb;
        $steamGameGoods->create_user = $this->user['id'];
        if ($steamGameGoods->save()) {
            # 创建成功后跳转到活动详细介绍编辑页
            return redirect()->route('manage.steam.game.goods.resource.index', ['steam_game_id' => $steamGameID])->with('success', '游戏商品创建成功!');
        }
        return redirect()->back()->with('error', '游戏商品创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $steamGameGoods = SteamGameGoods::find($id);
        //$gameList =  Game::keyName();
        # 表单元素
        $param = [];
        $param['title'] = [
            'name' => '活动标题',
            'type' => 'text',
            'col' => '12',
            'value' => $steamGameGoods->title
        ];
        $param['price'] = [
            'name' => '价格',
            'type' => 'text',
            'col' => '6',
            'value' => $steamGameGoods->price
        ];
        $param['type'] = [
            'name' => '类型',
            'type' => 'select',
            'col' => '6',
            'range' => gameGoodsType(),
            'value' => $steamGameGoods->type
        ];
        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改游戏商品信息';
       
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.goods.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回商品列表',
                'type' => 'button',
                'route' => route('manage.steam.game.goods.resource.index', ['steam_game_id' => $steamGameGoods->game_id])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改游戏商品信息操作
     *
     * @param  $request
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $this->request->flash();
        if (!$this->request->has('title')) {
            return redirect()->back()->with('error', '请填写商品名称');
        }
        if (!$this->request->has('price')) {
            return redirect()->back()->with('error', '请填写商品价格');
        }

        if (!$this->request->has('type')) {
            return redirect()->back()->with('error', '请填写商品类型');
        }
        # 插入新数据
        $steamGameGoods = SteamGameGoods::find($id);
//        if (!$this->request->has('game_id')) {
//            return redirect()->back()->with('error', '请选择游戏');
//        }
//        $gameId = $this->request->input('game_id');
//        $hasWMG = WechatMpGame::where('wechat_mp_id', $wechatMpGame->wechat_mp_id)->where('game_id', $gameId)->where('id', '<>', $id)->count();
//        if($hasWMG > 0) {
//            return redirect()->back()->with('error', '公众号下已存在该游戏');
//        }
        # 修改影视预览图
//        $thumb = null;
//        if ($this->request->has('thumb')) {
//            $thumb = $this->request->input('thumb');
//            $thumb = $this->uploadBase64Image($thumb, 'game');
//            if (!$thumb) {
//                return redirect()->back()->with('error', '游戏预览图文件上传失败!');
//            }
//        }
        $steamGameGoods->title = $this->request->input('title');
        //$steamGameGoods->game_id = $steamGameID;
        $steamGameGoods->price = $this->request->input('price');
        $steamGameGoods->type = $this->request->input('type');
        $steamGameGoods->update_user = $this->user['id'];
        if (!$steamGameGoods->save()) {
            return redirect()->back()->with('error', '商品修改失败');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page,
            'steam_game_id' => $steamGameGoods->game_id
        ];
        return redirect()->route('manage.steam.game.goods.resource.index', $param)->with('success', '商品修改成功');
    }
    /**
     * 删除游戏
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameGoods::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '商品删除成功!');
        }
        return redirect()->back()->with('error', '商品删除失败!');

    }
}