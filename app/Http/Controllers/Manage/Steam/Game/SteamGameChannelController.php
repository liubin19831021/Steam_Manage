<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameNews;
use App\Model\Steam\SteamGameChannel;
class SteamGameChannelController extends CommonController
{
    /**
     * 查看频道
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => '编号',
            'name' => '名称',
            'is_comprehensive' => '综合新闻',
            'rank' => '顺序',

        ];
        $viewData = [];
        $viewData['title'] = '频道管理';
        $viewData['range'] = [
            'is_comprehensive' => YN(),
        ];
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新频道',
                'route' => route('manage.steam.game.channel.resource.create')
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.steam.game.channel.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.channel.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除频道',
                'msg' => '确定要删除该频道吗'
            ],
        ];
        # 查询记录
        $steamGameChannel = new SteamGameChannel();
        $viewData['list'] = $this->constraintSelect($steamGameChannel, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 添加steam频道页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [];
        $param['name'] = [
            'name' => '名称',
            'type' => 'text',
            'col' => '4'
        ];
        $param['is_comprehensive'] = [
            'name' => '综合新闻',
            'type' => 'select',
            'col' => '4',
            'range' =>YN()
        ];
        $param['rank'] = [
            'name' => '顺序',
            'type' => 'text',
            'col' => '4',
        ];


        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        if($param['is_comprehensive']['value'] == null) $param['is_comprehensive']['value'] = 0;


        $viewData = [];
        $viewData['title'] = '发布新频道';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.channel.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回频道列表',
                'type' => 'button',
                'route' => route('manage.steam.game.channel.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建游戏页面
     *
     * @param  $request
     * @return mixed
     */
    public function store()
    {
        $this->request->flash();
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写名称');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序');
        }



        # 插入新数据
        $steamGameChannel = new SteamGameChannel();

        $steamGameChannel->name = $this->request->input('name');
        $steamGameChannel->rank = $this->request->input('rank');
        $steamGameChannel->is_comprehensive = $this->request->input('is_comprehensive');


        $steamGameChannel->create_user = $this->user['id'];
        if ($steamGameChannel->save()) {
            # 创建成功后跳转到公众号详细介绍编辑页
            return redirect()->route('manage.steam.game.channel.resource.index')->with('success', '频道创建成功!');
        }
        return redirect()->back()->with('error', '频道创建失败!');
    }

    /**
     * 修改游戏页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $steamGameChannel = SteamGameChannel::find($id);
        # 表单元素
        $param = [];
        $param['name'] = [
            'name' => '标题',
            'type' => 'text',
            'col' => '4',
            'value' => $steamGameChannel->name
        ];
        $param['is_comprehensive'] = [
            'name' => '综合新闻',
            'type' => 'select',
            'col' => '4',
            'range' =>YN(),
            'value' => $steamGameChannel->is_comprehensive
        ];
        $param['rank'] = [
            'name' => '顺序',
            'type' => 'text',
            'col' => '4',
            'value' => $steamGameChannel->rank
        ];

        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改频道信息';
        //$viewData['nav'] = $this->navTab('steam_game', 'main', ['id' => $id]);
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.channel.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回频道列表',
                'type' => 'button',
                'route' => route('manage.steam.game.channel.resource.index')
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改游戏操作
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $steamGameChannel = SteamGameChannel::find($id);
        $this->request->flash();
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写名称');
        }
        if (!$this->request->has('rank')) {
            return redirect()->back()->with('error', '请填写顺序');
        }
        $steamGameChannel->name = $this->request->input('name');
        $steamGameChannel->rank = $this->request->input('rank');
        $steamGameChannel->is_comprehensive = $this->request->input('is_comprehensive');

        $steamGameChannel->update_user = $this->user['id'];
        if (!$steamGameChannel->save()) {
            return redirect()->back()->with('error', '游戏修改成功');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page
        ];
        return redirect()->route('manage.steam.game.channel.resource.index', $param)->with('success', '频道修改成功');
    }
    /**
     * 删除游戏
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameChannel::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '频道删除成功!');
        }
        return redirect()->back()->with('error', '频道删除失败!');
    }
}