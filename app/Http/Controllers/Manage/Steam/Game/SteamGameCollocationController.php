<?php
namespace App\Http\Controllers\Manage\Steam\Game;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Steam\SteamGameCollocation;

class SteamGameCollocationController extends CommonController
{
    /**
     * 查看游戏配置
     * @return mixed
     */
    public function index()
    {
        $steamGameID = $this->request->input('steam_game_id');
        $field = [
            'id' => '编号',
            'type' => '系统类型'
        ];
        $viewData = [];
        $viewData['title'] = 'Steam配置管理';
        $viewData['range'] = [
            'type' => collocationType()
        ];
        # 查询字段
        $viewData['field'] = $field;
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title' => '发布新配置',
                'route' => route('manage.steam.game.collocation.resource.create', ['steam_game_id' => $steamGameID])
            ]
        ];
        # 行表操作菜单
        $page = $this->request->input('page', 1);
        $viewData['option_row'] = [
            'edit' => [
                'title' => '编辑',
                'route' => 'manage.steam.game.collocation.resource.edit',
                'param' =>   [
                    'page' => $page
                ]
            ],
            'trash' => [
                'title' => '删除',
                'route' => 'manage.steam.game.collocation.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除配置',
                'msg' => '确定要删除该配置吗'
            ],
        ];
        $viewData['nav'] = $this->navTab('steam_game', 'collocation', ['id' => $steamGameID]);
        # 查询记录
        $steamGameCollocation = SteamGameCollocation::where('game_id', $steamGameID);
        $viewData['list'] = $this->constraintSelect($steamGameCollocation, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 添加steam游戏配置
     * @return mixed
     */
    public function create()
    {
        $steamGameID = $this->request->input('steam_game_id');
        # 表单元素
        $param = [];
        $param['type'] = [
            'name' => '系统类型',
            'type' => 'select',
            'col' => '12',
            'range' =>collocationType()
        ];
        $param['content'] = [
            'name' => '',
            'type' => 'editor',
            'col' => '12',
            'upload_image' => route('manage.steam.game.describe.upload', ['id' => $steamGameID]),
        ];


        foreach ($param as $key => $value) {
            $param[$key]['value'] = $this->request->old($key);
        }
        if($param['type']['value'] == null) $param['type']['value'] = 1;
        $viewData = [];
        $viewData['title'] = '发布新配置';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.steam.game.collocation.resource.store', ['steam_game_id' => $steamGameID]);
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回配置列表',
                'type' => 'button',
                'route' => route('manage.steam.game.collocation.resource.index', ['steam_game_id' => $steamGameID])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     *
     * 创建游戏配置
     *
     * @param  $request
     * @return mixed
     */
    public function store()
    {
        $steamGameID = $this->request->input('steam_game_id');
        $this->request->flash();
        if (!$this->request->has('type')) {
            return redirect()->back()->with('error', '请填写系统类型');
        }
        if (!$this->request->has('content')) {
            return redirect()->back()->with('error', '请填写内容');
        }
        $have = SteamGameCollocation::where('game_id',$steamGameID)->where('type',$this->request->input('type'))->count();
        if ($have>0) {
            return redirect()->back()->with('error', '该记录已经存在');
        }
        # 插入新数据
        $steamGameCollocation = new SteamGameCollocation();
        $steamGameCollocation->type = $this->request->input('type');
        $steamGameCollocation->content = $this->request->input('content');
        $steamGameCollocation->game_id = $steamGameID;
        $steamGameCollocation->create_user = $this->user['id'];
        if ($steamGameCollocation->save()) {
            # 创建成功后跳转到公众号详细介绍编辑页
            return redirect()->route('manage.steam.game.collocation.resource.index',['steam_game_id' => $steamGameID])->with('success', '标签创建成功!');
        }
        return redirect()->back()->with('error', '标签创建失败!');
    }

    /**
     * 修改配置页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $steamGameCollocation = SteamGameCollocation::find($id);
        # 表单元素
        $param = [];
        $param['type'] = [
            'name' => '系统类型',
            'type' => 'select',
            'col' => '12',
            'range' =>collocationType(),
            'value' => $steamGameCollocation->type
        ];
        $param['content'] = [
            'name' => '',
            'type' => 'editor',
            'col' => '12',
            'upload_image' => route('manage.steam.game.describe.upload', ['id' => $id]),
            'value' => $steamGameCollocation->content
        ];

        foreach ($param as $key => $value) {
            if ($this->request->old($key)) $param[$key]['value'] = $this->request->old($key);
        }
        $viewData = [];
        $viewData['title'] = '修改配置信息';
        
        $viewData['form'] = $this->form($param);
        $page = $this->request->input('page', 1);
        $viewData['form_url'] = route('manage.steam.game.collocation.resource.update', ['id' => $id, 'page' => $page]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'back' => [
                'name' => '返回配置列表',
                'type' => 'button',
                'route' => route('manage.steam.game.collocation.resource.index', ['steam_game_id' => $steamGameCollocation->game_id])
            ],
            'reset' => [
                'name' => '重置',
                'type' => 'reset'
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     *
     * 修改配置操作
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        $steamGameCollocation = SteamGameCollocation::find($id);
        $this->request->flash();
        if (!$this->request->has('type')) {
            return redirect()->back()->with('error', '请填写系统类型');
        }
        if (!$this->request->has('content')) {
            return redirect()->back()->with('error', '请填写内容');
        }
        $have = SteamGameCollocation::where('game_id',$steamGameCollocation->game_id)->where('type',$this->request->input('type'))->count();
        if ($have>0) {
            return redirect()->back()->with('error', '该记录已经存在');
        }
        $steamGameCollocation->type = $this->request->input('type');
        $steamGameCollocation->content = $this->request->input('content');

        $steamGameCollocation->update_user = $this->user['id'];
        if (!$steamGameCollocation->save()) {
            return redirect()->back()->with('error', '配置修改成功');
        }
        $page = $this->request->input('page', 1);
        $param = [
            'page' => $page,
             'steam_game_id' => $steamGameCollocation->game_id
        ];
        return redirect()->route('manage.steam.game.collocation.resource.index', $param)->with('success', '配置修改成功');
    }
    /**
     * 删除配置
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = SteamGameCollocation::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '配置删除成功!');
        }
        return redirect()->back()->with('error', '配置删除失败!');

    }
}