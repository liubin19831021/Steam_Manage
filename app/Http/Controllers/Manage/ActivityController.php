<?php
namespace App\Http\Controllers\Manage;

use App\Model\Activity;
use App\Model\Store;

class ActivityController extends CommonController
{

    /**
     *
     * 活动列表
     * 
     * @return mixed
     */
    public function index()
    {
        $virtualStore = Store::virtualName();
        $field = [
            'id' => 'ID',
            'title' => '活动主题',
            'subtitle' => '副主题',
            'note' => '注释',
            'store_id' => '活动店铺',
            'category_id' => '活动类型',
            'start_date'=>'开始时间',
            'end_date'=>'结束时间',
        ];
        $viewData = [];
        $viewData['title'] = '商城公告';
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'category_id' => activityCategory(),
            'store_id' => $virtualStore
        ];
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title'=>'添加活动',
                'route' => route('manage.activity.create')
            ]
        ];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title'=>'编辑',
                'route'=>'manage.activity.edit'
            ],
            'trash' => [
                'title'=>'删除',
                'route'=>'manage.activity.destroy',
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'trash' => [
                'method' => 'DELETE',
                'title' => '删除记录',
                'msg' => '确定要删除此活动吗'
            ]
        ];
        # 列表数据
        $activity = new Activity;
        $viewData['list'] = $this->constraintSelect($activity, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除活动
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = Activity::find($id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '活动删除成功!');
        }
        return redirect()->back()->with('error', '活动删除失败!');
    }

    /**
     * 创建品牌页面 update by liudong 活动管理修改 20160818 
     * @return mixed
     */
    public function create()
    {
        $virtualStore = Store::virtualName('<>');
        # 表单元素
        $param = [
            'title' => [
                'type' => 'text',
                'col' => '4',
                'name' => '活动主题',
            ],
            'subtitle' => [
                'type' => 'text',
                'col' => '4',
                'name' => '活动副主题',
            ],
            'note' => [
                'type' => 'text',
                'col' => '4',
                'name' => '注释',
            ],
            'category_id' => [
                'type' => 'select',
                'col' => '3',
                'name' => '活动分类',
                'range' => activityCategory(),
            ],
            'store_id' => [
                'type' => 'select',
                'col' => '3',
                'name' => '店铺',
                'range' => $virtualStore,
                'default' => '0:请选择店铺'
            ],

            'start_date' => [
                'name' => '活动开始日期',
                'type' => 'date',
                'col' => '3'
            ],
            'end_date' => [
                'name' => '活动结束日期',
                'type' => 'date',
                'col' => '3'
            ],
            'thumb' => [
                'name' => '活动缩略预览图（建议256×256像素）',
                'type' => 'thumb',
                'col' => '3',
                'accept' => 'image',
                'ratio' => '1'
            ],
            'content' => [
                'name' => '活动内容',
                'type' => 'editor',
                'col' => '12',
                'upload_image' => $this->getApiRoute('upload.editor.image', ['token' => $this->user['token'], 'path' => 'activity'])
            ],
        ];
        $viewData = [];
        $viewData['title'] = '添加活动';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.activity.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.activity.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 存储活动
     * @return mixed
     */
    public function store()
    {
        //dd(Input::all());
        $title = $this->request->input('title');
        $subtitle = $this->request->input('subtitle');
        $note = $this->request->input('note');
        $categoryId = $this->request->input('category_id');
        $store_id = $this->request->input('store_id');
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $content = $this->request->input('content');
        if (!$title) {
            return redirect()->back()->with('error', '请输入活动主题!');
        }
        if (!$content) {
            return redirect()->back()->with('error', '请输入活动内容!');
        }
        if (!$this->request->has('thumb')) {
            return redirect()->back()->with('error', '预览图文件必须上传!');
        }
        $thumb = $this->request->input('thumb');
        //dd($thumb);
        $thumb = $this->uploadBase64Image($thumb, 'activity_thumb');

        if (!$thumb) {
            return redirect()->back()->with('error', '预览图文件上传失败!');
        }
        # 插入新数据
        $activity = new Activity();
        $activity->title = $title;
        $activity->subtitle = $subtitle;
        $activity->note = $note;
        $activity->category_id = $categoryId;
        $activity->store_id = $store_id ;
        $activity->content = $content;
        $activity->start_date = $start_date;
        $activity->end_date = $end_date;
        $activity->thumb = $thumb;
        //$activity->create_user = $this->user['id'];
        $activity->create_time = date('Y-m-d H:i:s',time());
        $activity->update_time = date('Y-m-d H:i:s',time());
        if ($activity->save()) {
            return redirect()->route('manage.activity.index')->with('success', '活动添加成功!'); # 更新成功后跳转到列表页
        }

        return redirect()->back()->with('error', '活动添加失败!');
    }

    /**
     * 修改活动信息页面 update by liudong 活动管理修改 20160818 
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = Activity::where('id', $id)->first();
        $virtualStore = Store::virtualName('<>');
        # 表单元素
        $param = [
            'title' => [
                'type' => 'text',
                'col' => '4',
                'name' => '活动主题题',
                'value' => $data->title
            ],
            'subtitle' => [
                'type' => 'text',
                'col' => '4',
                'name' => '活动副主题',
                'value' => $data->subtitle
            ],
            'note' => [
                'type' => 'text',
                'col' => '4',
                'name' => '活动注释',
                'value' => $data->note
            ],
            'category_id' => [
                'type' => 'select',
                'col' => '3',
                'name' => '活动分类',
                'range' => aboutCategory(),
                'value' => $data->category_id
            ],
            'store_id' => [
                'type' => 'select',
                'col' => '3',
                'name' => '店铺',
                'range' => $virtualStore,
                'value' => $data->store_id,
            ],
            'start_date' => [
                'name' => '活动开始日期',
                'type' => 'date',
                'col' => '3',
                'value' => $data->start_date
            ],
            'end_date' => [
                'name' => '活动结束日期',
                'type' => 'date',
                'col' => '3',
                'value' => $data->end_date
            ],
            'thumb' => [
                'name' => '活动缩略预览图（建议256×256像素）',
                'type' => 'thumb',
                'col' => '3',
                'accept' => 'image',
                'ratio' => '1',
                'value'  => $this->domain .'/'. $this->path['activity_thumb'] . $data->thumb,
            ],
            'content' => [
                'type' => 'editor',
                'col' => '12',
                'name' => '活动内容',
                'upload_image' => $this->getApiRoute('upload.editor.image', ['token' => $this->user['token'], 'path' => 'activity']),
                'value' => $data->content
            ],
        ];
        $viewData = [];
        $viewData['title'] = '修改文章';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.activity.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.activity.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 更新信息操作 update by liudong 活动管理修改 20160818 
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        # 接受输入信息
        $title = $this->request->input('title');
        $subtitle = $this->request->input('subtitle');
        $note = $this->request->input('note');
        $categoryId = $this->request->input('category_id');
        $store_id = $this->request->input('store_id');
        $start_date = $this->request->input('start_date');
        $end_date = $this->request->input('end_date');
        $content = $this->request->input('content');
        if (!$title) {
            return redirect()->back()->with('error', '请输入活动主题!');
        }
        if (!$content) {
            return redirect()->back()->with('error', '请输入活动内容!');
        }
        if (!$title) {
            return redirect()->back()->with('error', '请输入活动主题!');
        }
        if (!$content) {
            return redirect()->back()->with('error', '请输入活动内容!');
        }
        # 修改商品预览图
        $thumb = null;
        if ($this->request->has('thumb')) {
            $thumb = $this->request->input('thumb');
            $thumb = $this->uploadBase64Image($thumb, 'activity_thumb');
            if (!$thumb) {
                return redirect()->back()->with('error', '预览图文件上传失败!');
            }
        }
        # 更新数据
        $activity = Activity::find($id);
        $activity->title = $title;
        $activity->title = $title;
        $activity->subtitle = $subtitle;
        $activity->category_id = $categoryId;
        $activity->store_id = $store_id ;
        $activity->content = $content;
        $activity->start_date = $start_date;
        $activity->end_date = $end_date;
        if ($thumb) $activity->thumb = $thumb;
        //$activity->create_user = $this->user['id'];
        $activity->update_time = date('Y-m-d H:i:s',time());
        if ($activity->save()) {
            return redirect()->route('manage.activity.index')->with('success', '活动修改成功!'); # 更新成功后跳转到列表页
        }

        return redirect()->back()->with('error', '活动修改失败!');
    }
}