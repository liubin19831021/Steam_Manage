<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use PRedis;
use App\Model\Manage\Manage;
use App\Model\Manage\ManageMapRole;
use Illuminate\Support\Facades\URL;

class GuestController extends Controller
{
    protected $request;
    protected $user;

    function __construct(Request $request)
    {
        $this->request = $request;
        # 管理台视图全局数据
        view()->share('skin', 'blue-green');
        view()->share('domain', env('APP_URL'));
        view()->share('manage_title', trans('manage.title'));
        view()->share('manage_welcome',trans('manage.welcome_title'));
    }

    /**
     *
     * 进入登录页
     *
     * @return mixed
     */
    public function login()
    {
        return view('manage.login');
    }

    /**
     *
     * 认证来宾身份
     *
     * @return mixed
     */
    public function auth()
    {
        # 认证用户身份
        if($this->request->isMethod('post')){
            if ($this->request->has('username') && $this->request->has('password')) {
                $username = $this->request->input('username');
                $password = $this->request->input('password');
                # 查询管理员账号信息
                $manage = Manage::where('username', $username)->first();
                if (empty($manage->id) || empty($manage->password)) {
                    return redirect()->route('manage.login')->with('error', '用户名和密码不能为空！');
                }
                # 检查密码是否正确
                if (!Hash::check($password, $manage->password)) {
                    return redirect()->route('manage.login')->with('error', '用户名或密码不正确！');
                }
                # 保存登录操作信息
//                $manage->last_ip = $this->request->ip();
//                $manage->save();
                # 读取用户拥有的角色信息
//                $role = ManageMapRole::where('manage_id', $manage->id)->lists('role_id')->toArray();
                # 生产用户身份信息, 缓存用户信息到Redis
                $role = [1, 2, 3];
                $token = md5($manage->id . UUID());
                $this->user = [
                    'id' => $manage->id,
                    'username' => $manage->username,
                    'role' => implode(',', $role),
                    'token' => $token
                ];
                # 缓存管理员信息
                PRedis::pipeline(function ($pipe) {
                    $key = 'manage:' . $this->user['id'];
                    foreach ($this->user as $k => $val) {
                        $pipe->Hset($key, $k, $val);
                    }
                });
                # 缓存token
                PRedis::pipeline(function ($pipe) {
                    $key = 'token:' . $this->user['token'];
                    $pipe->Hset($key, 'type', 'manage');
                    $pipe->Hset($key, 'uid', $this->user['id']);
                });
                # 在session中保存用户ID
                $this->request->session()->put('manage', $this->user['id']);
                # 认证身份成功，跳转到管理台主页
                return redirect()->route('manage.index');
            }
        }
        # 认证失败，跳转到来宾登录页面
        return redirect()->route('manage.login')->with('error', '登录失败');
    }

    /**
     * 退出登录
     * @return mixed
     */
    public function logout()
    {
        # 从Session中获取用户ID
        $uid = $this->request->session()->get('manage');
        # 清除用户session
        $this->request->session()->forget('manage');
        # 读取用户信息，获取用户token
        $token = PRedis::HGet('manage:' . $uid, 'token');
        # 清除用户token
        PRedis::del('token:' . $token);
        # 清除管理员缓存信息
        PRedis::del('manage:' . $uid);
        return redirect()->route('manage.login');
    }
}
