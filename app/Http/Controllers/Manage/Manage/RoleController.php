<?php
namespace App\Http\Controllers\Manage\Manage;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Manage\ManageRole;
use App\Model\Manage\ManageAction;
use App\Model\Manage\ManageMapRole;
use App\Model\Manage\ManageRoleMapAction;
use FormElement;
use PRedis;

class RoleController extends CommonController
{
    /**
     * 查看管理员角色
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'name' => '角色名称',
            'status' => '描述'
        ];
        $viewData = [];
        $viewData['title'] = '管理员角色';
        # 查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $range = [
            'status' => [
                0 => '禁用',
                1 => '启用'
            ]
        ];
        $viewData['range'] = $range;
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => ['title'=>'查看', 'route'=>'manage.role.resource.edit'],
            'destroy' => [
                'title' => '删除',
                'route' => 'manage.role.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'destroy' => [
                'method' => 'DELETE',
                'title' => '删除记录',
                'msg' => '确定要彻底删除该角色吗'
            ],
        ];
        # 添加记录的路由
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title'=>'添加管理员角色',
                'route' => route('manage.role.resource.create')
            ]
        ];
        # 查询记录
        $manageRole = new ManageRole;
        $viewData['list'] = $this->constraintSelect($manageRole, $field, $range);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除管理员角色
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        # 删除角色并且删除管理员与角色的关联关系
        ManageMapRole::where('role_id', $id)->delete();
        $result = ManageRole::where('id', $id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '管理员角色删除成功!');
        }
        return redirect()->back()->with('error', '管理员角色删除失败或记录已被删除!');
    }

    /**
     * 创建管理员页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '角色名称',
            ],
            'action' => [
                'type' => 'checkbox-group',
                'col' => '12',
                'name' => '权限',
                'range' => ManageAction::groupAction()
            ],
            'describe' => [
                'type' => 'text',
                'col' => '12',
                'name' => '描述',
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加角色';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.role.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.role.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建管理员操作
     * @return mixed
     */
    public function store()
    {
        if (!$this->request->has('name')){
            return redirect()->back()->with('error', '请填写角色名称!');
        }
        # 插入新数据
        $ManageRole = new ManageRole;
        $ManageRole->name = $this->request->input('name');
        $ManageRole->describe = $this->request->input('describe', null);
        if ($ManageRole->save()) {
            $action = $this->request->input('action', null);
            ManageRoleMapAction::updateMap($ManageRole->id, $action);
            return redirect()->route('manage.role.resource.index')->with('success', '管理员角色创建成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员角色创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = ManageRole::where('id', $id)->first();
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '角色名称',
                'value' => $data->name,
            ],
            'action' => [
                'type' => 'checkbox-group',
                'col' => '12',
                'name' => '权限',
                'range' => ManageAction::groupAction(),
                'value' => ManageRoleMapAction::getMap($id),
            ],
            'describe' => [
                'type' => 'text',
                'col' => '12',
                'name' => '描述',
                'value' => $data->describe,
            ]
        ];

        $viewData = [];
        $viewData['title'] = '查看管理员角色';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.role.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.role.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改管理员信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->input('name')){
            return redirect()->back()->with('error', '请填写管理员名称!');
        }
        # 接受输入信息
        $action = $this->request->input('action', null);
        # 更新数据
        $manageRole = ManageRole::find($id);
        $manageRole->name = $this->request->input('name');
        $manageRole->describe = $this->request->input('describe', null);
        if ($manageRole->save()) {
            # 保存角色权限关联数据
            ManageRoleMapAction::updateMap($id, $action);
            # 角色缓存
            PRedis::HDel('manage:role', $id);
            return redirect()->route('manage.role.resource.index')->with('success', '管理员角色修改成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员角色修改失败!');
    }
}
