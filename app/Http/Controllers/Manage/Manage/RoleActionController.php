<?php
namespace App\Http\Controllers\Manage\Manage;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Manage\ManageAction;

class RoleActionController extends CommonController
{
    /**
     * 查看管理员角色
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'action_code' => '操作编码',
            'name' => '名称'
        ];
        $viewData = [];
        $viewData['title'] = '管理员权限';
        # 查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => ['title'=>'查看', 'route'=>'manage.role.action.resource.edit'],
            'destroy' => [
                'title' => '删除',
                'route' => 'manage.role.action.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'destroy' => [
                'method' => 'DELETE',
                'title' => '删除记录',
                'msg' => '确定要彻底删除该权限吗'
            ],
        ];
        # 添加记录的路由
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title'=>'添加管理员权限',
                'route' => route('manage.role.action.resource.create')
            ]
        ];
        # 查询记录
        $manageAction = new ManageAction;
        $viewData['list'] = $this->constraintSelect($manageAction, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除管理员角色
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        # 删除角色并且删除管理员与角色的关联关系
        $result = ManageAction::where('id', $id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '管理员权限删除成功!');
        }
        return redirect()->back()->with('error', '管理员权限删除失败!');
    }

    /**
     * 创建管理员页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'parent_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '权限分类',
                'range' => $this->getAction(),
                'default' => '0:主权限'
            ],
            'action_code' => [
                'type' => 'text',
                'col' => '4',
                'name' => '操作编码',
            ],
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '名称',
            ],
        ];
        $viewData = [];
        $viewData['title'] = '添加权限';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.role.action.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.role.action.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建管理员操作
     * @return mixed
     */
    public function store()
    {
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写权限名称');
        }
        if (!$this->request->has('action_code')) {
            return redirect()->back()->with('error', '请填写操作代码');
        }
        # 插入新数据
        $ManageAction = new ManageAction;
        $ManageAction->parent_id = $this->request->input('parent_id');
        $ManageAction->action_code = $this->request->input('action_code');
        $ManageAction->name =  $this->request->input('name');
        if ($ManageAction->save()) {
            return redirect()->route('manage.role.action.resource.index')->with('success', '管理员权限创建成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员权限创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = ManageAction::where('id', $id)->first();
        # 表单元素
        $param = [
            'parent_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '权限分类',
                'range' => $this->getAction(),
                'default' => '0:主权限',
                'value' => $data->parent_id,
            ],
            'action_code' => [
                'type' => 'text',
                'col' => '4',
                'name' => '操作编码',
                'value' => $data->action_code,
            ],
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '名称',
                'value' => $data->name,
            ]
        ];
        $viewData = [];
        $viewData['title'] = '查看管理员权限';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.role.action.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.role.action.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改管理员信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->has('name')) {
            return redirect()->back()->with('error', '请填写权限名称');
        }
        if (!$this->request->has('action_code')) {
            return redirect()->back()->with('error', '请填写操作代码');
        }
        # 插入新数据
        $ManageAction = ManageAction::find($id);
        $ManageAction->parent_id = $this->request->input('parent_id');
        $ManageAction->action_code = $this->request->input('action_code');
        $ManageAction->name =  $this->request->input('name');
        if ($ManageAction->save()) {
            return redirect()->route('manage.role.action.resource.index')->with('success', '管理员权限修改成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员权限修改失败!');
    }

    private function getAction()
    {
        $action = array();
        $result = ManageAction::where('parent_id', 0)->get();
        foreach ($result as $key => $val) {
            $action[$val->id] = $val->name;
        }
        return $action;
    }
}
