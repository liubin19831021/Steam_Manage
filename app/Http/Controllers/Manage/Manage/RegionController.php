<?php
namespace App\Http\Controllers\Manage\Manage;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Region;
use FormElement;
use PRedis;

class RegionController extends CommonController
{
    /**
     * 查看管理员角色
     * @return mixed
     */
    public function index()
    {
        $field = [
            'id' => 'ID',
            'name' => '地区名称',
            'code' => '邮政编码',
            'parent_id' => '父级编号',
            'level' => '层级',
            'status' => '状态'
        ];
        $viewData = [];
        $viewData['title'] = '地区管理';
        # 查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'status' => [
                1 => '启用',
                2 => '禁用'
            ]
        ];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => ['title'=>'查看', 'route'=>'manage.region.resource.edit'],
            'destroy' => [
                'title' => '禁用/启用',
                'route' => 'manage.region.resource.destroy'
            ]
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'destroy' => [
                'method' => 'DELETE',
                'title' => '启用状态',
                'msg' => '确定要修改禁用/启用状态'
            ],
        ];
        # 添加记录的路由
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title'=>'添加配送区域',
                'route' => route('manage.region.resource.create')
            ]
        ];
        # 查询记录
        $region = new Region;
        $viewData['list'] = $this->constraintSelect($region, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除地区
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $region = Region::find($id);
        $status = ($region->status == 1) ? 2 : 1;
        $statusStr = ($region->status == 1) ? '禁用' : '启用';
        $region->status = $status;
        if($region->save()) {
            return redirect()->route('manage.region.resource.index')->with('success', $statusStr . '成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', $statusStr . '失败!');
    }

    /**
     * 创建管理员页面
     * @return mixed
     */
    public function create()
    {
        $region = Region::keyName();
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '名称',
            ],
            'parent_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '父级',
                'range' => $region,
                'default' => '0:请选择……'
            ],
            'code' => [
                'type' => 'number',
                'col' => '4',
                'name' => '邮政编码',
            ]
        ];
        $viewData = [];
        $viewData['title'] = '添加角色';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.region.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.region.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建管理员操作
     * @return mixed
     */
    public function store()
    {
        # 判断名称
        if (!$this->request->has('name')){
            return redirect()->back()->with('error', '请填写地区名称!');
        }
        # 判断邮政编码
        $code = 0;
        if ($this->request->has('code')){
            $code = $this->request->input('code');
            # 判断邮政编码是否重复
            $hasCode = Region::where('code', $code)->count();
            if($hasCode > 0) {
                return redirect()->back()->with('error', '已存在该邮政编码！');
            }
        }
        # 父级编号
        $level = 0;
        $parentId = $this->request->input('parent_id');
        $parent = Region::find($parentId);
        if($parent) {
            $level = $parent->level + 1;
        }
        # 插入新数据
        $region = new Region();
        $region->name = $this->request->input('name');
        $region->code = $code;
        $region->parent_id = $parentId;
        $region->level = $level;
        $region->status = 1;
        if ($region->save()) {
            return redirect()->route('manage.region.resource.index')->with('success', '地区创建成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '地区创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $region = Region::keyName();
        $region = [0 => '最高级……'] + $region;
        # 查询记录数据
        $data = Region::find($id);
        $code = ($data->code > 0) ? $data->code : '';
        # 表单元素
        $param = [
            'name' => [
                'type' => 'text',
                'col' => '4',
                'name' => '名称',
                'value' => $data->name
            ],
            'parent_id' => [
                'type' => 'select',
                'col' => '4',
                'name' => '父级',
                'range' => $region,
                'value' => $data->parent_id
            ],
            'code' => [
                'type' => 'number',
                'col' => '4',
                'name' => '邮政编码',
                'value' => $code
            ]
        ];

        $viewData = [];
        $viewData['title'] = '查看地区信息';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.region.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.region.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改管理员信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->input('name')){
            return redirect()->back()->with('error', '请填写地区名称!');
        }
        # 更新数据
        $region = Region::find($id);
        $region->name = $this->request->input('name');
        $code = ($this->request->has('code')) ? $this->request->input('code') : 0;
        if($code > 0 && $code != $region->code) {
            # 判断邮政编码是否重复
            $hasCode = Region::where('code', $code)->count();
            if ($hasCode > 0) {
                return redirect()->back()->with('error', '已存在该邮政编码！');
            }
        }
        # 父级编号
        $level = 0;
        $parentId = $this->request->input('parent_id');
        if($parentId == $id){
            return redirect()->back()->with('error', '父级地区不能是自己！');
        }
        $parent = Region::find($parentId);
        if($parent) {
            $level = $parent->level + 1;
        }
        $region->parent_id = $parentId;
        $region->level = $level;
        $region->code = $code;
        if ($region->save()) {
            return redirect()->route('manage.region.resource.index')->with('success', '地区名称修改成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '地区名称修改失败!');
    }
}
