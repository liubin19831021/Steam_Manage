<?php
namespace App\Http\Controllers\Manage\Manage;

use App\Http\Controllers\Manage\CommonController;
use App\Model\Manage\Manage;
use Hash;

class ManageController extends CommonController
{
    /**
     * 修改密码
     * @return mixed
     */
    public function password()
    {
        $original = $this->request->input('original', null);
        $password = $this->request->input('password', null);
        $confirm = $this->request->input('confirm', null);
        if (!empty($original) && !empty($password) && $password == $confirm) {
            $manage = Manage::find($this->UID);
            if (Hash::check($original, $manage->password)) {
                $manage->password = Hash::make($password);
                if ($manage->save()) {
                    return redirect()->back()->with('success', '密码修改成功!');
                }
            }
        }
        return redirect()->back()->with('error', '密码修改失败!');
    }

    /**
     * 查看管理员
     * @return mixed
     */
    public function index()
    {
        /*
        $rule = ManageRule::check($this->user['role'], 'admin_manage');
        if (!$rule) {
            return redirect()->back()->with('error', '您没有访问系统用户管理功能的权限!');
        }
        */
        $field = [
            'id' => 'ID',
            'username' => '账号',
            'email' => '邮箱',
            'status' => '状态'
        ];

        $viewData = [];
        $viewData['title'] = '系统管理员';
        # 列表查询字段
        $viewData['field'] = $field;
        # 列表取值范围替换
        $viewData['range'] = [
            'status' => ['0'=>'禁用', '1'=>'正常']
        ];
        # 列表操作菜单
        $viewData['option_title'] = [
            'plus' => [
                'title'=>'添加管理员',
                'route' => route('manage.user.resource.create')
            ]
        ];
        # 行表操作菜单
        $viewData['option_row'] = [
            'edit' => [
                'title'=>'修改',
                'route'=>'manage.user.resource.edit'
            ],
            'rule' => [
                'title'=>'权限',
                'load'=>'manage.user.rule.resource.index'
            ],
            'destroy' => [
                'title'=>'删除',
                'route'=>'manage.user.resource.destroy',
            ],
        ];
        # 页面模态对话框
        $viewData['option_modal'] = [
            'destroy' => [
                'method' => 'DELETE',
                'title' => '删除记录',
                'msg' => '确定要彻底删除该管理员吗'
            ],
        ];
        # 列表数据
        $manage = new Manage;
        $viewData['list'] = $this->constraintSelect($manage, $field);
        # 渲染视图
        return view('manage.table', $viewData);
    }

    /**
     * 删除管理员
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $result = Manage::where('id', $id)->delete();
        if ($result) {
            return redirect()->back()->with('success', '管理员删除成功!');
        }
        return redirect()->back()->with('error', '管理员删除失败!');
    }

    /**
     * 创建管理员页面
     * @return mixed
     */
    public function create()
    {
        # 表单元素
        $param = [
            'username' => [
                'type' => 'text',
                'col' => '4',
                'name' => '账号',
            ],
            'password' => [
                'type' => 'password',
                'col' => '4',
                'name' => '密码',
            ],
            'email' => [
                'type' => 'text',
                'col' => '4',
                'name' => '邮箱',
            ],
            'phone' => [
                'type' => 'text',
                'col' => '4',
                'name' => '手机号',
            ],
            'status' => [
                'type' => 'select',
                'col' => '4',
                'name' => '状态',
                'range' => ['1'=>'启用', '0'=>'禁用']
            ],
        ];
        $viewData = [];
        $viewData['title'] = '添加管理员';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.user.resource.store');
        $viewData['form_method'] = 'POST';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.user.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        return view('manage.form', $viewData);
    }

    /**
     * 创建管理员操作
     * @return mixed
     */
    public function store()
    {
        if (!$this->request->has('username')){
            return redirect()->back()->with('error', '请填写管理员账号!');
        }
        if (!$this->request->has('password')){
            return redirect()->back()->with('error', '请填写密码!');
        }
        # 插入新数据
        $manage = new Manage;
        $manage->username = $this->request->input('username');
        $password = $this->request->input('password', null);
        $manage->password = Hash::make($password);
        $manage->email =  $this->request->input('email', null);
        $manage->phone = $this->request->input('phone', null);
        $manage->status = $this->request->input('status', 0);
        if ($manage->save()) {
            return redirect()->route('manage.user.resource.index')->with('success', '管理员创建成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员创建失败!');
    }

    /**
     * 修改管理员信息页面
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        # 查询记录数据
        $data = Manage::where('id', $id)->first();
        # 表单元素
        $param = [
            'username' => [
                'type' => 'text',
                'col' => '4',
                'name' => '账号',
                'value' => $data->username,
            ],
            'password' => [
                'type' => 'password',
                'col' => '4',
                'name' => '密码',
                'value' => null,
            ],
            'email' => [
                'type' => 'text',
                'col' => '4',
                'name' => '邮箱',
                'value' => $data->email,
            ],
            'phone' => [
                'type' => 'text',
                'col' => '4',
                'name' => '手机号',
                'value' => $data->phone,
            ],
            'status' => [
                'type' => 'select',
                'col' => '4',
                'name' => '状态',
                'value' => $data->status,
                'range' => ['1'=>'启用', '0'=>'禁用']
            ],
        ];

        $viewData = [];
        $viewData['title'] = '查看管理员';
        $viewData['form'] = $this->form($param);
        $viewData['form_url'] = route('manage.user.resource.update', ['id' => $id]);
        $viewData['form_method'] = 'PUT';
        $viewData['form_button'] = [
            'return' => [
                'name' => '取消',
                'type' => 'button',
                'route' => route('manage.user.resource.index')
            ],
            'submit' => [
                'name' => '提交',
                'type' => 'submit',
            ]
        ];
        # 渲染视图
        return view('manage.form', $viewData);
    }

    /**
     * 修改管理员信息操作
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        if (!$this->request->has('username')){
            return redirect()->back()->with('error', '请填写管理员账号!');
        }
        # 更新数据
        $manage = Manage::find($id);
        $manage->username =  $this->request->input('username');
        if ($this->request->has('password')){
            $password = $this->request->input('password', null);
            $manage->password = Hash::make($password);
        }
        $manage->email = $this->request->input('email');
        $manage->phone = $this->request->input('phone');
        $manage->status = $this->request->input('status');
        if ($manage->save()) {
            return redirect()->route('manage.user.resource.index')->with('success', '管理员信息修改成功!'); # 更新成功后跳转到列表页
        }
        return redirect()->back()->with('error', '管理员信息修改失败!');
    }


}
