<?php

namespace App\Http\Middleware;

use Closure;
use PRedis;

class AuthManage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        # 检测访问用户是否有认证信息
        if ($request->session()->has('manage')) {
            $uid = $request->session()->get('manage');
            # 根据Session从Redis中获取用户信息
            if (PRedis::exists('manage:' . $uid)) return $next($request);
        }
        # 没有认证信息重定向到登录页
        return redirect()->route('manage.login');
    }
}
