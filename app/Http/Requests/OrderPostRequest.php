<?php
namespace App\Http\Requests;

class OrderPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rebate_money' => ['regex:' . pattern('price')]
        ];
    }

    public function messages()
    {
        return [
            'rebate_money.regex' => '分成金额格式不正确'
        ];
    }
}
