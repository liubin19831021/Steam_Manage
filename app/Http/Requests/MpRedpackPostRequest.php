<?php
namespace App\Http\Requests;

class MpRedpackPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => ['regex:' . pattern('price')],
            'max_price' => ['regex:' . pattern('price')],
            'min_price' => ['regex:' . pattern('price')]
        ];
    }

    public function messages()
    {
        return [
            'price.regex' => '红包金额格式不正确',
            'max_price.regex' => '最大金额格式不正确',
            'min_price.regex' => '最小金额格式不正确'
        ];
    }
}
