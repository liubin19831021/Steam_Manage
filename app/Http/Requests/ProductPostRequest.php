<?php
namespace App\Http\Requests;

class ProductPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => ['regex:' . pattern('price')],
            'sale_price' => ['regex:' . pattern('price')]
        ];
    }

    public function messages()
    {
        return [
            'price.regex' => '商品价格金额格式不正确',
            'sale_price.regex' => '促销价格金额格式不正确'
        ];
    }
}
